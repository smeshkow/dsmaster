/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.dsmaster.model.dto.configuration;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration implements org.apache.thrift.TBase<Configuration, Configuration._Fields>, java.io.Serializable, Cloneable, Comparable<Configuration> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("Configuration");

  private static final org.apache.thrift.protocol.TField KEY_FIELD_DESC = new org.apache.thrift.protocol.TField("key", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField STR_VAL_FIELD_DESC = new org.apache.thrift.protocol.TField("strVal", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField INT_VAL_FIELD_DESC = new org.apache.thrift.protocol.TField("intVal", org.apache.thrift.protocol.TType.I32, (short)3);
  private static final org.apache.thrift.protocol.TField DOUBLE_VAL_FIELD_DESC = new org.apache.thrift.protocol.TField("doubleVal", org.apache.thrift.protocol.TType.DOUBLE, (short)4);
  private static final org.apache.thrift.protocol.TField DESCRIPTION_FIELD_DESC = new org.apache.thrift.protocol.TField("description", org.apache.thrift.protocol.TType.STRING, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ConfigurationStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ConfigurationTupleSchemeFactory());
  }

  private String key; // required
  private String strVal; // optional
  private int intVal; // optional
  private double doubleVal; // optional
  private String description; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    KEY((short)1, "key"),
    STR_VAL((short)2, "strVal"),
    INT_VAL((short)3, "intVal"),
    DOUBLE_VAL((short)4, "doubleVal"),
    DESCRIPTION((short)5, "description");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // KEY
          return KEY;
        case 2: // STR_VAL
          return STR_VAL;
        case 3: // INT_VAL
          return INT_VAL;
        case 4: // DOUBLE_VAL
          return DOUBLE_VAL;
        case 5: // DESCRIPTION
          return DESCRIPTION;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __INTVAL_ISSET_ID = 0;
  private static final int __DOUBLEVAL_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.STR_VAL,_Fields.INT_VAL,_Fields.DOUBLE_VAL,_Fields.DESCRIPTION};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.KEY, new org.apache.thrift.meta_data.FieldMetaData("key", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.STR_VAL, new org.apache.thrift.meta_data.FieldMetaData("strVal", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.INT_VAL, new org.apache.thrift.meta_data.FieldMetaData("intVal", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.DOUBLE_VAL, new org.apache.thrift.meta_data.FieldMetaData("doubleVal", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.DESCRIPTION, new org.apache.thrift.meta_data.FieldMetaData("description", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(Configuration.class, metaDataMap);
  }

  public Configuration() {
  }

  public Configuration(
    String key)
  {
    this();
    this.key = key;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public Configuration(Configuration other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetKey()) {
      this.key = other.key;
    }
    if (other.isSetStrVal()) {
      this.strVal = other.strVal;
    }
    this.intVal = other.intVal;
    this.doubleVal = other.doubleVal;
    if (other.isSetDescription()) {
      this.description = other.description;
    }
  }

  public Configuration deepCopy() {
    return new Configuration(this);
  }

  @Override
  public void clear() {
    this.key = null;
    this.strVal = null;
    setIntValIsSet(false);
    this.intVal = 0;
    setDoubleValIsSet(false);
    this.doubleVal = 0.0;
    this.description = null;
  }

  public String getKey() {
    return this.key;
  }

  public Configuration setKey(String key) {
    this.key = key;
    return this;
  }

  public void unsetKey() {
    this.key = null;
  }

  /** Returns true if field key is set (has been assigned a value) and false otherwise */
  public boolean isSetKey() {
    return this.key != null;
  }

  public void setKeyIsSet(boolean value) {
    if (!value) {
      this.key = null;
    }
  }

  public String getStrVal() {
    return this.strVal;
  }

  public Configuration setStrVal(String strVal) {
    this.strVal = strVal;
    return this;
  }

  public void unsetStrVal() {
    this.strVal = null;
  }

  /** Returns true if field strVal is set (has been assigned a value) and false otherwise */
  public boolean isSetStrVal() {
    return this.strVal != null;
  }

  public void setStrValIsSet(boolean value) {
    if (!value) {
      this.strVal = null;
    }
  }

  public int getIntVal() {
    return this.intVal;
  }

  public Configuration setIntVal(int intVal) {
    this.intVal = intVal;
    setIntValIsSet(true);
    return this;
  }

  public void unsetIntVal() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __INTVAL_ISSET_ID);
  }

  /** Returns true if field intVal is set (has been assigned a value) and false otherwise */
  public boolean isSetIntVal() {
    return EncodingUtils.testBit(__isset_bitfield, __INTVAL_ISSET_ID);
  }

  public void setIntValIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __INTVAL_ISSET_ID, value);
  }

  public double getDoubleVal() {
    return this.doubleVal;
  }

  public Configuration setDoubleVal(double doubleVal) {
    this.doubleVal = doubleVal;
    setDoubleValIsSet(true);
    return this;
  }

  public void unsetDoubleVal() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __DOUBLEVAL_ISSET_ID);
  }

  /** Returns true if field doubleVal is set (has been assigned a value) and false otherwise */
  public boolean isSetDoubleVal() {
    return EncodingUtils.testBit(__isset_bitfield, __DOUBLEVAL_ISSET_ID);
  }

  public void setDoubleValIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __DOUBLEVAL_ISSET_ID, value);
  }

  public String getDescription() {
    return this.description;
  }

  public Configuration setDescription(String description) {
    this.description = description;
    return this;
  }

  public void unsetDescription() {
    this.description = null;
  }

  /** Returns true if field description is set (has been assigned a value) and false otherwise */
  public boolean isSetDescription() {
    return this.description != null;
  }

  public void setDescriptionIsSet(boolean value) {
    if (!value) {
      this.description = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case KEY:
      if (value == null) {
        unsetKey();
      } else {
        setKey((String)value);
      }
      break;

    case STR_VAL:
      if (value == null) {
        unsetStrVal();
      } else {
        setStrVal((String)value);
      }
      break;

    case INT_VAL:
      if (value == null) {
        unsetIntVal();
      } else {
        setIntVal((Integer)value);
      }
      break;

    case DOUBLE_VAL:
      if (value == null) {
        unsetDoubleVal();
      } else {
        setDoubleVal((Double)value);
      }
      break;

    case DESCRIPTION:
      if (value == null) {
        unsetDescription();
      } else {
        setDescription((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case KEY:
      return getKey();

    case STR_VAL:
      return getStrVal();

    case INT_VAL:
      return Integer.valueOf(getIntVal());

    case DOUBLE_VAL:
      return Double.valueOf(getDoubleVal());

    case DESCRIPTION:
      return getDescription();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case KEY:
      return isSetKey();
    case STR_VAL:
      return isSetStrVal();
    case INT_VAL:
      return isSetIntVal();
    case DOUBLE_VAL:
      return isSetDoubleVal();
    case DESCRIPTION:
      return isSetDescription();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof Configuration)
      return this.equals((Configuration)that);
    return false;
  }

  public boolean equals(Configuration that) {
    if (that == null)
      return false;

    boolean this_present_key = true && this.isSetKey();
    boolean that_present_key = true && that.isSetKey();
    if (this_present_key || that_present_key) {
      if (!(this_present_key && that_present_key))
        return false;
      if (!this.key.equals(that.key))
        return false;
    }

    boolean this_present_strVal = true && this.isSetStrVal();
    boolean that_present_strVal = true && that.isSetStrVal();
    if (this_present_strVal || that_present_strVal) {
      if (!(this_present_strVal && that_present_strVal))
        return false;
      if (!this.strVal.equals(that.strVal))
        return false;
    }

    boolean this_present_intVal = true && this.isSetIntVal();
    boolean that_present_intVal = true && that.isSetIntVal();
    if (this_present_intVal || that_present_intVal) {
      if (!(this_present_intVal && that_present_intVal))
        return false;
      if (this.intVal != that.intVal)
        return false;
    }

    boolean this_present_doubleVal = true && this.isSetDoubleVal();
    boolean that_present_doubleVal = true && that.isSetDoubleVal();
    if (this_present_doubleVal || that_present_doubleVal) {
      if (!(this_present_doubleVal && that_present_doubleVal))
        return false;
      if (this.doubleVal != that.doubleVal)
        return false;
    }

    boolean this_present_description = true && this.isSetDescription();
    boolean that_present_description = true && that.isSetDescription();
    if (this_present_description || that_present_description) {
      if (!(this_present_description && that_present_description))
        return false;
      if (!this.description.equals(that.description))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    HashCodeBuilder builder = new HashCodeBuilder();

    boolean present_key = true && (isSetKey());
    builder.append(present_key);
    if (present_key)
      builder.append(key);

    boolean present_strVal = true && (isSetStrVal());
    builder.append(present_strVal);
    if (present_strVal)
      builder.append(strVal);

    boolean present_intVal = true && (isSetIntVal());
    builder.append(present_intVal);
    if (present_intVal)
      builder.append(intVal);

    boolean present_doubleVal = true && (isSetDoubleVal());
    builder.append(present_doubleVal);
    if (present_doubleVal)
      builder.append(doubleVal);

    boolean present_description = true && (isSetDescription());
    builder.append(present_description);
    if (present_description)
      builder.append(description);

    return builder.toHashCode();
  }

  @Override
  public int compareTo(Configuration other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetKey()).compareTo(other.isSetKey());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetKey()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.key, other.key);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetStrVal()).compareTo(other.isSetStrVal());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetStrVal()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.strVal, other.strVal);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetIntVal()).compareTo(other.isSetIntVal());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetIntVal()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.intVal, other.intVal);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetDoubleVal()).compareTo(other.isSetDoubleVal());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetDoubleVal()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.doubleVal, other.doubleVal);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetDescription()).compareTo(other.isSetDescription());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetDescription()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.description, other.description);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Configuration(");
    boolean first = true;

    sb.append("key:");
    if (this.key == null) {
      sb.append("null");
    } else {
      sb.append(this.key);
    }
    first = false;
    if (isSetStrVal()) {
      if (!first) sb.append(", ");
      sb.append("strVal:");
      if (this.strVal == null) {
        sb.append("null");
      } else {
        sb.append(this.strVal);
      }
      first = false;
    }
    if (isSetIntVal()) {
      if (!first) sb.append(", ");
      sb.append("intVal:");
      sb.append(this.intVal);
      first = false;
    }
    if (isSetDoubleVal()) {
      if (!first) sb.append(", ");
      sb.append("doubleVal:");
      sb.append(this.doubleVal);
      first = false;
    }
    if (isSetDescription()) {
      if (!first) sb.append(", ");
      sb.append("description:");
      if (this.description == null) {
        sb.append("null");
      } else {
        sb.append(this.description);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ConfigurationStandardSchemeFactory implements SchemeFactory {
    public ConfigurationStandardScheme getScheme() {
      return new ConfigurationStandardScheme();
    }
  }

  private static class ConfigurationStandardScheme extends StandardScheme<Configuration> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, Configuration struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // KEY
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.key = iprot.readString();
              struct.setKeyIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // STR_VAL
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.strVal = iprot.readString();
              struct.setStrValIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // INT_VAL
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.intVal = iprot.readI32();
              struct.setIntValIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // DOUBLE_VAL
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.doubleVal = iprot.readDouble();
              struct.setDoubleValIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // DESCRIPTION
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.description = iprot.readString();
              struct.setDescriptionIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, Configuration struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.key != null) {
        oprot.writeFieldBegin(KEY_FIELD_DESC);
        oprot.writeString(struct.key);
        oprot.writeFieldEnd();
      }
      if (struct.strVal != null) {
        if (struct.isSetStrVal()) {
          oprot.writeFieldBegin(STR_VAL_FIELD_DESC);
          oprot.writeString(struct.strVal);
          oprot.writeFieldEnd();
        }
      }
      if (struct.isSetIntVal()) {
        oprot.writeFieldBegin(INT_VAL_FIELD_DESC);
        oprot.writeI32(struct.intVal);
        oprot.writeFieldEnd();
      }
      if (struct.isSetDoubleVal()) {
        oprot.writeFieldBegin(DOUBLE_VAL_FIELD_DESC);
        oprot.writeDouble(struct.doubleVal);
        oprot.writeFieldEnd();
      }
      if (struct.description != null) {
        if (struct.isSetDescription()) {
          oprot.writeFieldBegin(DESCRIPTION_FIELD_DESC);
          oprot.writeString(struct.description);
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ConfigurationTupleSchemeFactory implements SchemeFactory {
    public ConfigurationTupleScheme getScheme() {
      return new ConfigurationTupleScheme();
    }
  }

  private static class ConfigurationTupleScheme extends TupleScheme<Configuration> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, Configuration struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetKey()) {
        optionals.set(0);
      }
      if (struct.isSetStrVal()) {
        optionals.set(1);
      }
      if (struct.isSetIntVal()) {
        optionals.set(2);
      }
      if (struct.isSetDoubleVal()) {
        optionals.set(3);
      }
      if (struct.isSetDescription()) {
        optionals.set(4);
      }
      oprot.writeBitSet(optionals, 5);
      if (struct.isSetKey()) {
        oprot.writeString(struct.key);
      }
      if (struct.isSetStrVal()) {
        oprot.writeString(struct.strVal);
      }
      if (struct.isSetIntVal()) {
        oprot.writeI32(struct.intVal);
      }
      if (struct.isSetDoubleVal()) {
        oprot.writeDouble(struct.doubleVal);
      }
      if (struct.isSetDescription()) {
        oprot.writeString(struct.description);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, Configuration struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(5);
      if (incoming.get(0)) {
        struct.key = iprot.readString();
        struct.setKeyIsSet(true);
      }
      if (incoming.get(1)) {
        struct.strVal = iprot.readString();
        struct.setStrValIsSet(true);
      }
      if (incoming.get(2)) {
        struct.intVal = iprot.readI32();
        struct.setIntValIsSet(true);
      }
      if (incoming.get(3)) {
        struct.doubleVal = iprot.readDouble();
        struct.setDoubleValIsSet(true);
      }
      if (incoming.get(4)) {
        struct.description = iprot.readString();
        struct.setDescriptionIsSet(true);
      }
    }
  }

}

