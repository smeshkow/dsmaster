/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.dsmaster.model.dto.security;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum AccessLevel implements org.apache.thrift.TEnum {
  A(1),
  B(28),
  C(56),
  D(84),
  E(112),
  F(140),
  G(168),
  H(196),
  I(224),
  J(252),
  K(280),
  L(308),
  M(336),
  N(364),
  O(392),
  P(420),
  Q(448),
  R(476),
  S(504),
  T(532),
  U(560),
  V(588),
  W(616),
  X(644),
  Y(672),
  Z(700);

  private final int value;

  private AccessLevel(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static AccessLevel findByValue(int value) { 
    switch (value) {
      case 1:
        return A;
      case 28:
        return B;
      case 56:
        return C;
      case 84:
        return D;
      case 112:
        return E;
      case 140:
        return F;
      case 168:
        return G;
      case 196:
        return H;
      case 224:
        return I;
      case 252:
        return J;
      case 280:
        return K;
      case 308:
        return L;
      case 336:
        return M;
      case 364:
        return N;
      case 392:
        return O;
      case 420:
        return P;
      case 448:
        return Q;
      case 476:
        return R;
      case 504:
        return S;
      case 532:
        return T;
      case 560:
        return U;
      case 588:
        return V;
      case 616:
        return W;
      case 644:
        return X;
      case 672:
        return Y;
      case 700:
        return Z;
      default:
        return null;
    }
  }
}
