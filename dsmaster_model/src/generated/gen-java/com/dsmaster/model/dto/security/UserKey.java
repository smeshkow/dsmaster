/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.dsmaster.model.dto.security;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserKey implements org.apache.thrift.TBase<UserKey, UserKey._Fields>, java.io.Serializable, Cloneable, Comparable<UserKey> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("UserKey");

  private static final org.apache.thrift.protocol.TField UUID_FIELD_DESC = new org.apache.thrift.protocol.TField("uuid", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField ACCESS_LEVEL_FIELD_DESC = new org.apache.thrift.protocol.TField("accessLevel", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField KEY_STORE_FIELD_DESC = new org.apache.thrift.protocol.TField("keyStore", org.apache.thrift.protocol.TType.STRING, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new UserKeyStandardSchemeFactory());
    schemes.put(TupleScheme.class, new UserKeyTupleSchemeFactory());
  }

  private String uuid; // required
  private com.dsmaster.model.dto.security.AccessLevel accessLevel; // required
  private String keyStore; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    UUID((short)1, "uuid"),
    /**
     * 
     * @see com.dsmaster.model.dto.security.AccessLevel
     */
    ACCESS_LEVEL((short)2, "accessLevel"),
    KEY_STORE((short)3, "keyStore");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // UUID
          return UUID;
        case 2: // ACCESS_LEVEL
          return ACCESS_LEVEL;
        case 3: // KEY_STORE
          return KEY_STORE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.UUID, new org.apache.thrift.meta_data.FieldMetaData("uuid", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.ACCESS_LEVEL, new org.apache.thrift.meta_data.FieldMetaData("accessLevel", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, com.dsmaster.model.dto.security.AccessLevel.class)));
    tmpMap.put(_Fields.KEY_STORE, new org.apache.thrift.meta_data.FieldMetaData("keyStore", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(UserKey.class, metaDataMap);
  }

  public UserKey() {
  }

  public UserKey(
    String uuid,
    com.dsmaster.model.dto.security.AccessLevel accessLevel,
    String keyStore)
  {
    this();
    this.uuid = uuid;
    this.accessLevel = accessLevel;
    this.keyStore = keyStore;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public UserKey(UserKey other) {
    if (other.isSetUuid()) {
      this.uuid = other.uuid;
    }
    if (other.isSetAccessLevel()) {
      this.accessLevel = other.accessLevel;
    }
    if (other.isSetKeyStore()) {
      this.keyStore = other.keyStore;
    }
  }

  public UserKey deepCopy() {
    return new UserKey(this);
  }

  @Override
  public void clear() {
    this.uuid = null;
    this.accessLevel = null;
    this.keyStore = null;
  }

  public String getUuid() {
    return this.uuid;
  }

  public UserKey setUuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

  public void unsetUuid() {
    this.uuid = null;
  }

  /** Returns true if field uuid is set (has been assigned a value) and false otherwise */
  public boolean isSetUuid() {
    return this.uuid != null;
  }

  public void setUuidIsSet(boolean value) {
    if (!value) {
      this.uuid = null;
    }
  }

  /**
   * 
   * @see com.dsmaster.model.dto.security.AccessLevel
   */
  public com.dsmaster.model.dto.security.AccessLevel getAccessLevel() {
    return this.accessLevel;
  }

  /**
   * 
   * @see com.dsmaster.model.dto.security.AccessLevel
   */
  public UserKey setAccessLevel(com.dsmaster.model.dto.security.AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
    return this;
  }

  public void unsetAccessLevel() {
    this.accessLevel = null;
  }

  /** Returns true if field accessLevel is set (has been assigned a value) and false otherwise */
  public boolean isSetAccessLevel() {
    return this.accessLevel != null;
  }

  public void setAccessLevelIsSet(boolean value) {
    if (!value) {
      this.accessLevel = null;
    }
  }

  public String getKeyStore() {
    return this.keyStore;
  }

  public UserKey setKeyStore(String keyStore) {
    this.keyStore = keyStore;
    return this;
  }

  public void unsetKeyStore() {
    this.keyStore = null;
  }

  /** Returns true if field keyStore is set (has been assigned a value) and false otherwise */
  public boolean isSetKeyStore() {
    return this.keyStore != null;
  }

  public void setKeyStoreIsSet(boolean value) {
    if (!value) {
      this.keyStore = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case UUID:
      if (value == null) {
        unsetUuid();
      } else {
        setUuid((String)value);
      }
      break;

    case ACCESS_LEVEL:
      if (value == null) {
        unsetAccessLevel();
      } else {
        setAccessLevel((com.dsmaster.model.dto.security.AccessLevel)value);
      }
      break;

    case KEY_STORE:
      if (value == null) {
        unsetKeyStore();
      } else {
        setKeyStore((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case UUID:
      return getUuid();

    case ACCESS_LEVEL:
      return getAccessLevel();

    case KEY_STORE:
      return getKeyStore();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case UUID:
      return isSetUuid();
    case ACCESS_LEVEL:
      return isSetAccessLevel();
    case KEY_STORE:
      return isSetKeyStore();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof UserKey)
      return this.equals((UserKey)that);
    return false;
  }

  public boolean equals(UserKey that) {
    if (that == null)
      return false;

    boolean this_present_uuid = true && this.isSetUuid();
    boolean that_present_uuid = true && that.isSetUuid();
    if (this_present_uuid || that_present_uuid) {
      if (!(this_present_uuid && that_present_uuid))
        return false;
      if (!this.uuid.equals(that.uuid))
        return false;
    }

    boolean this_present_accessLevel = true && this.isSetAccessLevel();
    boolean that_present_accessLevel = true && that.isSetAccessLevel();
    if (this_present_accessLevel || that_present_accessLevel) {
      if (!(this_present_accessLevel && that_present_accessLevel))
        return false;
      if (!this.accessLevel.equals(that.accessLevel))
        return false;
    }

    boolean this_present_keyStore = true && this.isSetKeyStore();
    boolean that_present_keyStore = true && that.isSetKeyStore();
    if (this_present_keyStore || that_present_keyStore) {
      if (!(this_present_keyStore && that_present_keyStore))
        return false;
      if (!this.keyStore.equals(that.keyStore))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    HashCodeBuilder builder = new HashCodeBuilder();

    boolean present_uuid = true && (isSetUuid());
    builder.append(present_uuid);
    if (present_uuid)
      builder.append(uuid);

    boolean present_accessLevel = true && (isSetAccessLevel());
    builder.append(present_accessLevel);
    if (present_accessLevel)
      builder.append(accessLevel.getValue());

    boolean present_keyStore = true && (isSetKeyStore());
    builder.append(present_keyStore);
    if (present_keyStore)
      builder.append(keyStore);

    return builder.toHashCode();
  }

  @Override
  public int compareTo(UserKey other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetUuid()).compareTo(other.isSetUuid());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUuid()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.uuid, other.uuid);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetAccessLevel()).compareTo(other.isSetAccessLevel());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAccessLevel()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.accessLevel, other.accessLevel);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetKeyStore()).compareTo(other.isSetKeyStore());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetKeyStore()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.keyStore, other.keyStore);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("UserKey(");
    boolean first = true;

    sb.append("uuid:");
    if (this.uuid == null) {
      sb.append("null");
    } else {
      sb.append(this.uuid);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("accessLevel:");
    if (this.accessLevel == null) {
      sb.append("null");
    } else {
      sb.append(this.accessLevel);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("keyStore:");
    if (this.keyStore == null) {
      sb.append("null");
    } else {
      sb.append(this.keyStore);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class UserKeyStandardSchemeFactory implements SchemeFactory {
    public UserKeyStandardScheme getScheme() {
      return new UserKeyStandardScheme();
    }
  }

  private static class UserKeyStandardScheme extends StandardScheme<UserKey> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, UserKey struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // UUID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.uuid = iprot.readString();
              struct.setUuidIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // ACCESS_LEVEL
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.accessLevel = com.dsmaster.model.dto.security.AccessLevel.findByValue(iprot.readI32());
              struct.setAccessLevelIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // KEY_STORE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.keyStore = iprot.readString();
              struct.setKeyStoreIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, UserKey struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.uuid != null) {
        oprot.writeFieldBegin(UUID_FIELD_DESC);
        oprot.writeString(struct.uuid);
        oprot.writeFieldEnd();
      }
      if (struct.accessLevel != null) {
        oprot.writeFieldBegin(ACCESS_LEVEL_FIELD_DESC);
        oprot.writeI32(struct.accessLevel.getValue());
        oprot.writeFieldEnd();
      }
      if (struct.keyStore != null) {
        oprot.writeFieldBegin(KEY_STORE_FIELD_DESC);
        oprot.writeString(struct.keyStore);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class UserKeyTupleSchemeFactory implements SchemeFactory {
    public UserKeyTupleScheme getScheme() {
      return new UserKeyTupleScheme();
    }
  }

  private static class UserKeyTupleScheme extends TupleScheme<UserKey> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, UserKey struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetUuid()) {
        optionals.set(0);
      }
      if (struct.isSetAccessLevel()) {
        optionals.set(1);
      }
      if (struct.isSetKeyStore()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetUuid()) {
        oprot.writeString(struct.uuid);
      }
      if (struct.isSetAccessLevel()) {
        oprot.writeI32(struct.accessLevel.getValue());
      }
      if (struct.isSetKeyStore()) {
        oprot.writeString(struct.keyStore);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, UserKey struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.uuid = iprot.readString();
        struct.setUuidIsSet(true);
      }
      if (incoming.get(1)) {
        struct.accessLevel = com.dsmaster.model.dto.security.AccessLevel.findByValue(iprot.readI32());
        struct.setAccessLevelIsSet(true);
      }
      if (incoming.get(2)) {
        struct.keyStore = iprot.readString();
        struct.setKeyStoreIsSet(true);
      }
    }
  }

}

