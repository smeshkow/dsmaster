/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.dsmaster.model.dto.stuff;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum ArmorType implements org.apache.thrift.TEnum {
  HEAD(1),
  CHEST(2),
  LEGS(3),
  HANDS(4);

  private final int value;

  private ArmorType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static ArmorType findByValue(int value) { 
    switch (value) {
      case 1:
        return HEAD;
      case 2:
        return CHEST;
      case 3:
        return LEGS;
      case 4:
        return HANDS;
      default:
        return null;
    }
  }
}
