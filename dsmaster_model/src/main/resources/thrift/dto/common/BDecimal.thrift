include 'BInteger.thrift'

namespace java com.dsmaster.model.dto.common

struct BDecimal {
    1: required i32 scale = 1,
    2: required BInteger.BInteger intVal;
}