namespace java com.dsmaster.model.dto.common

struct BInteger {
    1: required list<byte> bytes = 1
}