namespace java com.dsmaster.model.dto.configuration

struct Configuration {
    1: string key,
    2: optional string strVal,
    3: optional i32 intVal,
    4: optional double doubleVal,
    5: optional string description
}