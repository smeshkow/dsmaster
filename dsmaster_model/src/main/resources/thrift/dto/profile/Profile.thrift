include 'ProfileStatus.thrift'

namespace java com.dsmaster.model.dto.profile

struct Profile {
    1: optional i32 id,
    2: string uuid,
    3: string email,
    4: string passwordHash,
    5: ProfileStatus.ProfileStatus status,
    6: optional i64 timestampCreated,
    7: optional i64 timestampUpdated
}