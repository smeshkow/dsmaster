include "BuildBonuses.thrift"

namespace java com.dsmaster.model.dto.profile

struct Build {
    1: optional i32 id,

    2: optional i16 lvl,
    3: optional i64 sm,

    4: i16 vgr,
    5: i16 endurance,
    6: i16 vit,
    7: i16 atn,
    8: i16 str,
    9: i16 dex,
    10: i16 adp,
    11: i16 intelligent,
    12: i16 fth,

    13: optional BuildBonuses.BuildBonuses buildBonuses
}