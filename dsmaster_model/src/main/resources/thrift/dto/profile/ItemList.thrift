include 'Build.thrift'
include '../stuff/attributes/AttackAttribute.thrift'

namespace java com.dsmaster.model.dto.profile

struct ItemList {
    1:  i32 id,
    2:  Build.Build build,
    3:  optional i32 left1Id,
    4:  optional i32 left2Id,
    5:  optional i32 left3Id,
    6:  optional i32 right1Id,
    7:  optional i32 right2Id,
    8:  optional i32 right3Id,

    9:  optional i32 headId,
    10: optional i32 handId,
    11: optional i32 chestId,
    12: optional i32 legsId,

    13: optional AttackAttribute.AttackAttribute attackBonusLeft1,
    14: optional AttackAttribute.AttackAttribute attackBonusLeft2,
    15: optional AttackAttribute.AttackAttribute attackBonusLeft3,

    16: optional AttackAttribute.AttackAttribute attackBonusRight1,
    17: optional AttackAttribute.AttackAttribute attackBonusRight2,
    18: optional AttackAttribute.AttackAttribute attackBonusRight3


    /*9:  optional i16 left1Bonus,
    10: optional i16 left2Bonus,
    11: optional i16 left3Bonus,
    12: optional i16 right1Bonus,
    13: optional i16 right2Bonus,
    14: optional i16 right3Bonus,*/


    //TODO consumable ?
    //TODO spells ?
    //TODO buffs as result

}