namespace java com.dsmaster.model.dto.profile

enum ProfileStatus {
    ON_APPROVE = 1,
    ACTIVE = 2,
    INACTIVE = 3,
    BLOCKED = 4,
    BANNED = 5,
    DELETED = 6
}