namespace java com.dsmaster.model.dto.profile

struct BuildBonuses {
    1: i32 hp,
    2: i16 stamina,
    3: double equipLoad,
    4: byte slot,
    5: i16 castSpeed,
    6: i16 atkStr,
    7: i16 atkDex,
    8: i16 atkMagic,
    9: i16 atkFire,
    10: i16 atkLighting,
    11: i16 atkDark,
    12: i16 atkPoison,
    13: i16 atkBleed,

    14: i16 physDef,
    15: i16 magicDef,
    16: i16 fireDef,
    17: i16 lightingDef,
    18: i16 darkDef,
    19: i16 poisonRes,
    20: i16 bleedRes,
    21: i16 petrifyRes,
    22: i16 curseRes,
    23: i16 agility,
    24: double poise
}