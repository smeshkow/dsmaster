namespace java com.dsmaster.model.dto.article

enum ArticleType {
    ALL = 1,
    MENU = 2,
    WEAPON_SHIELD = 3,
    ARMOR = 4,
    ARROW = 5,
    RING = 6,
    SPELL = 7,
    ITEM = 8
}