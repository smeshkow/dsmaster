include '../common/Addon.thrift'
include 'ArticleType.thrift'

namespace java com.dsmaster.model.dto.article

struct Article {
    1: optional i32 id,
    2: Addon.Addon addon,
    3: string name,
    4: optional string description,
    5: optional string acquiredFrom,
    6: optional string hintsAndTips,
    7: ArticleType.ArticleType type,
    8: optional string link
}