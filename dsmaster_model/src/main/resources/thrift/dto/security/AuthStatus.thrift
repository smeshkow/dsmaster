namespace java com.dsmaster.model.dto.security

enum AuthStatus {
    OK = 1,
	USER_INACTIVE = 2,
	USER_NOT_EXISTS = 3,
	WRONG_PASSWORD = 4,
	SQL_ERROR = 5,
	USER_BLOCKED = 6,
	USER_BANNED = 7,
	USER_DELETED = 8,
	USER_ONAPPROVE = 9,
	USER_NO_AGENCY = 10,
    WRONG_MAIL = 11

}