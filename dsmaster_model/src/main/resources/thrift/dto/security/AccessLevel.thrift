namespace java com.dsmaster.model.dto.security

enum AccessLevel {
    // Each Access level has 26 sublevels (26 digits of space after each level), so you can add them if you need.
    A = 1, // highest access level - SUPER ADMIN - ALL ACCESS ALLOWED
    B = 28,
    C = 56,
    D = 84, // All DB write access
    E = 112,
    F = 140,
    G = 168,
    H = 196,
    I = 224,
    J = 252,
    K = 280,
    L = 308,
    M = 336,
    N = 364,
    O = 392,
    P = 420,
    Q = 448,
    R = 476,
    S = 504,
    T = 532,
    U = 560,
    V = 588,
    W = 616,
    X = 644,
    Y = 672, // registered user
    Z = 700  // lowest access level - unauthorized user - ALL ACCESS RESTRICTED
}