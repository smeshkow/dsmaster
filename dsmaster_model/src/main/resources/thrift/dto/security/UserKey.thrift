include 'AccessLevel.thrift'

namespace java com.dsmaster.model.dto.security

struct UserKey {
    1: string uuid,
    2: AccessLevel.AccessLevel accessLevel,
    3: string keyStore
}