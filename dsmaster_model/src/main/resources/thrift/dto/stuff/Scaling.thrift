namespace java com.dsmaster.model.dto.stuff

enum Scaling {
    S = 1,
    A = 2,
    B = 3,
    C = 4,
    D = 5,
    E = 6
}