namespace java com.dsmaster.model.dto.stuff

enum ArmorType {
    HEAD = 1,
    CHEST = 2,
    LEGS = 3,
    HANDS = 4
}