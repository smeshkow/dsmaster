namespace java com.dsmaster.model.dto.stuff

enum AttackType {
    PARRY = 1,
    PROJECTILE = 2,
    SLASH = 3,
    SPELL = 4,
    SPELL_PARRY = 5,
    STRIKE = 6,
    THRUST = 7
}