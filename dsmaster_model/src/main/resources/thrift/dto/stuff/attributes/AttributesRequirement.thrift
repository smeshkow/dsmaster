namespace java com.dsmaster.model.dto.stuff.attributes

struct AttributesRequirement {
    1: optional i32 id,
    2: optional i16 requiredStr,
    3: optional i16 requiredDex,
    4: optional i16 requiredInt,
    5: optional i16 requiredFaith
}