namespace java com.dsmaster.model.dto.stuff.attributes

struct AttackAttribute {
    1: optional i16 physicalAtk,
    2: optional i16 magicAtk,
    3: optional i16 fireAtk,
    4: optional i16 lightningAtk,
    5: optional i16 darkAtk,
    6: optional i16 poisonAtk,
    7: optional i16 bleedAtk,
}