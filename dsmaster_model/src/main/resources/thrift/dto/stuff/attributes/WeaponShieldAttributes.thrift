include 'AttackAttribute.thrift'

namespace java com.dsmaster.model.dto.stuff.attributes

struct WeaponShieldAttributes {
    1: optional i32 id,
    2: optional i16 durability,
    3: optional double weight,
    //atc
    4: optional AttackAttribute.AttackAttribute attackAttribute,
    //reduction
    5: optional i16 physicalReduction,
    6: optional i16 magicReduction,
    7: optional i16 lightningReduction,
    8: optional i16 fireReduction,
    9: optional i16 darkReduction,
    10: optional i16 poisonReduction,
    11: optional i16 bleedReduction,
    12: optional i16 curseReduction,
    13: optional i16 petrifyReduction,
    //other
    14: optional i16 counterStrength,
    15: optional i16 stability,
    16: optional i16 poiseDamage,
    17: optional i16 shortRange,
    18: optional i16 castSpeed
}