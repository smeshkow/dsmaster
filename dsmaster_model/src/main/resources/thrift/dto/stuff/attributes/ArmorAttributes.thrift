namespace java com.dsmaster.model.dto.stuff.attributes

struct ArmorAttributes {
    1:  optional i32 id,
    2:  optional i16 durability,
    3:  optional double weight,

    4:  optional i16 physicalDef,
    5:  optional i16 magicDef,
    6:  optional i16 fireDef,
    7:  optional i16 lightningDef,
    8:  optional i16 darkDef,

    9:  optional i16 poisonResist,
    10: optional i16 bleedResist,
    11: optional i16 petrifyResist,
    12: optional i16 curseResist,

    13: optional i16 physicalDefStrike,
    14: optional i16 physicalDefSlash,
    15: optional i16 physicalDefThrust,

    16: optional i16 poise
}