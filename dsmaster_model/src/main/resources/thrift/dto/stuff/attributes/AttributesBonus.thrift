namespace java com.dsmaster.model.dto.stuff.attributes

struct AttributesBonus {
    1: optional i32 id,
    2: optional i16 bonusStr,
    3: optional i16 bonusDex,
    4: optional i16 bonusInt,
    5: optional i16 bonusFaith
}