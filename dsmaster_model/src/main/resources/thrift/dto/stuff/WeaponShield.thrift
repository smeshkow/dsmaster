include '../common/Addon.thrift'
include '../article/Article.thrift'
include 'WeaponCategoty.thrift'
include 'AttackType.thrift'
include 'Scaling.thrift'
include 'attributes/WeaponShieldAttributes.thrift'
include 'attributes/AttributesRequirement.thrift'
include 'attributes/AttributesBonus.thrift'

namespace java com.dsmaster.model.dto.stuff

struct WeaponShield {
    1:  optional i32 id,
    2:  Addon.Addon addon,
    3:  string name,
    4:  optional string description,
    5:  optional WeaponCategoty.WeaponCategory weaponCategory,
    6:  optional AttackType.AttackType attackType1,
    7:  optional AttackType.AttackType attackType2,
    8:  optional Scaling.Scaling strScaling,
    9:  optional Scaling.Scaling dexScaling,
    10: optional Scaling.Scaling magicScaling,
    11: optional Scaling.Scaling fireScaling,
    12: optional Scaling.Scaling lightningScaling,
    13: optional Scaling.Scaling darkScaling,
    14: optional double strScalingPercent,
    15: optional double dexScalingPercent,
    16: optional double magicScalingPercent,
    17: optional double fireScalingPercent,
    18: optional double lightningScalingPercent,
    19: optional double darkScalingPercent,
    20: optional WeaponShieldAttributes.WeaponShieldAttributes weaponShieldAttributes,
    21: optional AttributesRequirement.AttributesRequirement attributesRequirement,
    22: optional AttributesBonus.AttributesBonus attributesBonus,
    23: optional Article.Article article
}