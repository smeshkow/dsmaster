include '../common/Addon.thrift'
include 'ArmorType.thrift'
include 'Scaling.thrift'
include 'attributes/AttributesRequirement.thrift'
include 'attributes/AttributesBonus.thrift'
include 'attributes/ArmorAttributes.thrift'

namespace java com.dsmaster.model.dto.stuff

struct Armor {
    1:  optional i32 id,
    2:  Addon.Addon addon,
    3:  string name,
    4:  optional string description,
    5:  optional Scaling.Scaling scaling,
    6:  optional ArmorType.ArmorType armorType
    7:  optional ArmorAttributes.ArmorAttributes armorAttributes,
    8:  optional AttributesRequirement.AttributesRequirement attributesRequirement,
    9:  optional AttributesBonus.AttributesBonus attributesBonus
}