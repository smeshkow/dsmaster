package com.dsmaster.model.exception;

/**
 * Created by smeshkov on 08.08.14.
 */
public class OperationNotSupported extends RuntimeException {
    public OperationNotSupported(String message) {
        super(message);
    }
}
