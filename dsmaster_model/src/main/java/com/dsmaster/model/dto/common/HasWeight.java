package com.dsmaster.model.dto.common;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:46
 */
@Deprecated
public interface HasWeight<T extends HasWeight> {

    Double getWeight();

}
