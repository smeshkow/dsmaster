package com.dsmaster.model.dto.common;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:59
 */
public interface HasItemType {

    ItemType getItemType();

}
