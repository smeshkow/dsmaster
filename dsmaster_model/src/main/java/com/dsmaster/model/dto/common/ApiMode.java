package com.dsmaster.model.dto.common;

/**
 * Created by smeshkov on 7/15/14.
 */
public enum ApiMode {

    SIMPLE("simple"),
    SECURE("secure")
    ;

    private String val;

    ApiMode(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public static ApiMode forVal(String val) {
        for (ApiMode v : values()) {
            if(v.getVal().equals(val)) return v;
        }
        return null;
    }

}
