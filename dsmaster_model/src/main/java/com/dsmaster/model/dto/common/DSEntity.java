package com.dsmaster.model.dto.common;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:52
 */
public interface DSEntity<T extends DSEntity> {

    Integer getId();

    String getName();

    String getDescription();


}
