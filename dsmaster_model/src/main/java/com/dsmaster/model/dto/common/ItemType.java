package com.dsmaster.model.dto.common;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:53
 */
public enum ItemType {

    WEAPON,
    SHIELD,
    ARMOR,
    RING,
    CONSUMABLE,
    SPELL,
    ;

}
