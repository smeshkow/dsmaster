package com.dsmaster.model.util;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.instrument.InstrumentationSavingAgent;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.util.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Created by smeshkov on 3/15/14.
 *
 * Helpful tool for determining java Objects size with help of spring-instrument.jar (Java Agent)
 * This Util is Based on Maxim Zakharenkov's sizeofag.jar
 * explained here http://jroller.com/maxim/entry/again_about_determining_size_of
 *
 */
public class MemoryUtils {

    private static final Logger log =
            LoggerFactory.getLogger(MemoryUtils.class);

    private static final boolean AGENT_CLASS_PRESENT = ClassUtils.isPresent(
            InstrumentationSavingAgent.class.getName(),
            InstrumentationLoadTimeWeaver.class.getClassLoader());

    public static void printFullObjectSize(Object obj, Unit unit) {
        log.info("size of {}", getFullObjectSize(obj, unit));
    }

    public static String getFullObjectSize(Object obj, Unit unit) {
        long size = 0L;
        String simpleName = "null";
        if(obj != null) {
            // Skip Spring Autowired
            List<Class> excludeAnnotations = new ArrayList<>();
            excludeAnnotations.add(Autowired.class);

            // Skip Spring Application Context
            List<Class> excludeInstOf = new ArrayList<>();
            excludeInstOf.add(ApplicationContext.class);

            size = fullSizeOf(obj, excludeAnnotations, excludeInstOf);
            simpleName = obj.getClass().getSimpleName();
        }
        return String.format("%s is %d %s", simpleName, size/unit.getDivisor(), unit.getName());
    }

    /**
     * Calculates only one object size without
     * counting its child variable sizes which
     * are derived from Object.
     * @param obj object to calculate size of
     * @return object size in bytes
     */
    public static long sizeOf(Object obj) {
        return getInstrumentation().getObjectSize(obj);
    }

    /**
     * Calculates full size of object iterating over
     * its hierarchy graph.
     * @param obj object to calculate size of
     * @return object size in bytes
     */
    public static long fullSizeOf(Object obj) {
        return fullSizeOf(obj, null, null);
    }

    /**
     * Calculates full size of object iterating over
     * its hierarchy graph.
     * @param obj object to calculate size of
     * @param excludeAnnotations exclude fields of obj which annotated with classes
     * @param excludeInstOf exclude fields of obj which are instances of classes
     * @return object size in bytes
     */
    public static long fullSizeOf(Object obj, List<Class> excludeAnnotations, List<Class> excludeInstOf) {
        Map<Object, Object> visited = new IdentityHashMap<Object, Object>();
        Stack<Object> stack = new Stack<Object>();

        long result = internalSizeOf(obj, stack, visited, excludeAnnotations, excludeInstOf);
        while (!stack.isEmpty()) {
            result += internalSizeOf(stack.pop(), stack, visited, excludeAnnotations, excludeInstOf);
        }
        visited.clear();
        return result;
    }

    // ----------------------------------------------- PRIVATES -----------------------------------------------

    /**
     * Obtain the Instrumentation instance for the current VM, if available.
     * @return the Instrumentation instance, or {@code null} if none found
     */
    private static Instrumentation getInstrumentation() {
        if (AGENT_CLASS_PRESENT) {
            return InstrumentationAccessor.getInstrumentation();
        } else {
            throw new IllegalStateException("Agent not initialized.");
        }
    }

    /**
     * Inner class to avoid InstrumentationSavingAgent dependency.
     */
    private static class InstrumentationAccessor {

        public static Instrumentation getInstrumentation() {
            return InstrumentationSavingAgent.getInstrumentation();
        }
    }

    private static boolean skipObject(Object obj, Map<Object, Object> visited) {
        if (obj instanceof String) {
            // skip interned string
            if (obj == ((String) obj).intern()) {
                return true;
            }
        }
        return (obj == null) // skip visited object
                || visited.containsKey(obj);
    }

    private static long internalSizeOf(Object obj, Stack<Object> stack, Map<Object, Object> visited) {
        return internalSizeOf(obj, stack, visited, null, null);
    }

    private static long internalSizeOf(Object obj, Stack<Object> stack, Map<Object, Object> visited,
                                       List<Class> excludeAnnotations, List<Class> excludeInstOf) {
        if (skipObject(obj, visited)){
            return 0;
        }
        visited.put(obj, null);

        long result = 0;
        // get size of object + primitive variables + member pointers
        result += MemoryUtils.sizeOf(obj);

        // process all array elements
        Class clazz = obj.getClass();
        if (clazz.isArray()) {
            if(clazz.getName().length() != 2) {// skip primitive type array
                int length =  Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    stack.add(Array.get(obj, i));
                }
            }
            return result;
        }

        // process all fields of the object
        while (clazz != null) {
            Field[] fields = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {

                // Skip by excludes
                if(isSkipByAnnotation(fields[i], excludeAnnotations)) continue;
                if(isSkipByInstOf(fields[i], excludeInstOf)) continue;

                if (!Modifier.isStatic(fields[i].getModifiers())) {
                    if (fields[i].getType().isPrimitive()) {
                        continue; // skip primitive fields
                    } else {
                        fields[i].setAccessible(true);
                        try {
                            // objects to be estimated are put to stack
                            Object objectToAdd = fields[i].get(obj);
                            if (objectToAdd != null) {
                                stack.add(objectToAdd);
                            }
                        } catch (IllegalAccessException ex) {
                            assert false;
                        }
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
        return result;
    }

    private static boolean isSkipByAnnotation(Field field, List<Class> excludeAnnotations) {
        if(CollectionUtils.isEmpty(excludeAnnotations)) return false;
        for (Annotation annotation : field.getDeclaredAnnotations()) {
            if(excludeAnnotations.contains(annotation.annotationType())) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSkipByInstOf(Field field, List<Class> excludeInstOf) {
        if(CollectionUtils.isEmpty(excludeInstOf) || field.getType() == null) return false;
        for (Class aClass : excludeInstOf) if(field.getType().isAssignableFrom(aClass)) return true;
        return false;
    }

    // ----------------------------------------------- INNER ENUM -----------------------------------------------

    public static enum Unit {
        BYTE(1, "bytes"), KILOBYTE(1024, "kilobytes"), MEGABYTE(1048576, "megabytes");

        private int divisor;
        private String name;

        Unit(int divisor, String name) {
            this.divisor = divisor;
            this.name = name;
        }

        public int getDivisor() {
            return divisor;
        }

        public String getName() {
            return name;
        }
    }
}
