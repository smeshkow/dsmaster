package com.dsmaster.model.util;

import com.dsmaster.model.dto.common.BDecimal;
import com.dsmaster.model.dto.common.BInteger;
import org.springframework.aop.framework.Advised;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by smeshkov on 7/21/14.
 */
public class DsmUtils {

    public static final String CHARSET_ENCODING = "UTF-8";
    public static final Charset CHARSET = Charset.forName( CHARSET_ENCODING );
    public static final int SYSTEM_USER_ID = -1;
    public static final BDecimal SYSTEM_USER_IP = bigDecimalToProtocol(BigDecimal.valueOf(-1L));

    @SuppressWarnings({"unchecked"})
    public static <T> T getTargetObject(Object proxy)  {
        if (isProxy(proxy)) {
            try {
                return (T) ((Advised)proxy).getTargetSource().getTarget();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
        }
    }

    public static boolean isProxy(Object proxy) {
        return proxy instanceof Advised;
    }

    public static String parseDomain(String urlStr) {
        if(urlStr==null) {
            return null;
        }
        String s = new String(urlStr);
        int index = urlStr.indexOf("://");
        if(index>-1) {
            s = s.substring(index+3, urlStr.length());
        }
        index = s.indexOf("/");
        if(index>-1) {
            s = s.substring(0, index);
        }
        return s;
    }

    public static BDecimal bigDecimalToProtocol(BigDecimal value) {
        int scale = value.scale();
        BigInteger bigInteger = value.unscaledValue();
        byte[] byteArray = bigInteger.toByteArray();
        List<Byte> bytes = new ArrayList<>();
        for (byte b : byteArray) {
            bytes.add(b);
        }
        return new BDecimal(scale, new BInteger(bytes));
    }

    public static BigDecimal protocolToBigDecimal(BDecimal value) {
        List<Byte> bytes = value.getIntVal().getBytes();
        byte[] byteArray = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            byteArray[i] = bytes.get(i);
        }
        return new BigDecimal(new BigInteger(byteArray), value.getScale());
    }

    public static BigDecimal ipToDecimal(String stringIP) {
        DecimalFormat df = new DecimalFormat();
        df.setParseBigDecimal(true);
        try {
            return (BigDecimal) df.parse(stringIP);
        } catch (ParseException e) {
            ExcHandler.ex(e);
            return null;
        }
    }

}
