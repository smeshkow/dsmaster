package com.dsmaster.model.util;

/**
 * Created by smeshkov on 8/5/14.
 */
@FunctionalInterface
public interface ExceptionalConsumer<T, E extends Exception> {

    void accept(T t) throws E;

}
