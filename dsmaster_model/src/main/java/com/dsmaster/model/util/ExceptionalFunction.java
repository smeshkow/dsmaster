package com.dsmaster.model.util;

/**
 * Created by smeshkov on 7/22/14.
 */
@FunctionalInterface
public interface ExceptionalFunction<R, T, E extends Exception> {
    public R apply(T t) throws E;
}
