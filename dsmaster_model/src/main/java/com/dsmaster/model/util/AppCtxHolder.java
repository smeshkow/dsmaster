package com.dsmaster.model.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;

/**
 * Created by smeshkov on 7/22/14.
 */
public class AppCtxHolder implements ApplicationContextAware, Ordered {

    private static ApplicationContext ctx;

    public static ApplicationContext get() {
        return ctx;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
