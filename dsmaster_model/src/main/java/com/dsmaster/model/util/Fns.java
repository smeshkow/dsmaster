package com.dsmaster.model.util;

import org.apache.thrift.TEnum;
import org.squarebrackets.function.*;
import org.squarebrackets.function.DoubleConsumer;
import org.squarebrackets.function.LongConsumer;

import java.util.function.*;

/**
 * User: artem_marinov
 * Date: 09.07.14
 * Time: 17:06
 */
public class Fns {

    public static <F,T> T mutate(Supplier<F> supplierFrom, Supplier<T> supplierTo, BiFunction<F, T, T> biFunction) {
        T result = biFunction.apply(supplierFrom.get(), supplierTo.get());
        return result;
    }

    public static final Function<TEnum, Byte> tEnumToByte = e -> Integer.valueOf(e.getValue()).byteValue();



    public static <T> void doIfSupplierOk(Consumer<T> consumer, Supplier<T> supplier, Supplier<Boolean> testSupplier) {
        doIfSupplierOk(consumer, supplier, t -> t, testSupplier);
    }

    public static <F,T> void doIfSupplierOk(Consumer<T> consumer, Supplier<F> supplier, Function<F, T> converter, Supplier<Boolean> testSupplier) {
        if(testSupplier.get()) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

    public static <T> void doIfNotNull(Consumer<T> consumer, Supplier<T> supplier) {
        doIfNotNull(consumer, supplier, t -> t);
    }

    public static <F,T> void doIfNotNull(Consumer<T> consumer, Supplier<F> supplier, Function<F, T> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }


    public static void setBooleanIfNotNull(BooleanConsumer consumer, Supplier<Boolean> supplier) {
        setBooleanIfNotNull(consumer, supplier, t -> t);
    }

    public static <F> void setBooleanIfNotNull(BooleanConsumer consumer, Supplier<F> supplier, Function<F, Boolean> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

    public static void setByteIfNotNull(ByteConsumer consumer, Supplier<Byte> supplier) {
        setByteIfNotNull(consumer, supplier, t -> t);
    }

    public static <F> void setByteIfNotNull(ByteConsumer consumer, Supplier<F> supplier, Function<F, Byte> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

    public static void setShortIfNotNull(ShortConsumer consumer, Supplier<Short> supplier) {
        setShortIfNotNull(consumer, supplier, t ->  t);
    }

    public static <F> void setShortIfNotNull(ShortConsumer consumer, Supplier<F> supplier, Function<F, Short> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

    public static void setLongIfNotNull(LongConsumer consumer, Supplier<Long> supplier) {
        setLongIfNotNull(consumer, supplier, t -> t);
    }

    public static <F> void setLongIfNotNull(LongConsumer consumer, Supplier<F> supplier, Function<F, Long> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

   public static void setDoubleIfNotNull(DoubleConsumer consumer, Supplier<Double> supplier) {
        setDoubleIfNotNull(consumer, supplier, t -> t);
    }

    public static <F> void setDoubleIfNotNull(DoubleConsumer consumer, Supplier<F> supplier, Function<F, Double> converter) {
        if(supplier.get() != null) {
            consumer.accept(converter.apply(supplier.get()));
        }
    }

}
