package com.dsmaster.gateway.service.security;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.security.SecurityService;
import com.dsmaster.api.gateway.service.security.UserKeyListResponse;
import com.dsmaster.api.util.UserKeyCache;
import com.dsmaster.api.util.DsmGen;
import com.dsmaster.model.dto.security.AccessLevel;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.ExcHandler;
import com.dsmaster.model.util.MemoryUtils;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by smeshkov on 02.09.14.
 */
public class UserKeyCacheWrapper {

    private static final Logger log = LoggerFactory.getLogger(UserKeyCacheWrapper.class);

    private final UserKeyCache state;

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();
    private final Condition isReady = w.newCondition();

    private volatile boolean cacheValid = false;

    private final SecurityService.Iface securityService;

    // Singleton
    private static class UserKeyCacheWrapperHolder {
        private static final UserKeyCacheWrapper HOLDER = new UserKeyCacheWrapper();
    }
    public static UserKeyCacheWrapper get() { return UserKeyCacheWrapperHolder.HOLDER; }

    private UserKeyCacheWrapper() {
        state = new UserKeyCache();
        securityService = AppCtxHolder.get().getBean(SecurityService.Iface.class);
    }

    // -------------------------------------------------- WRITE ------------------------------------------------------

    public void write(final UserKey store) {
        w.lock();
        try {
            state.putToKeyMap(store.getUuid(), store);
        } finally {
            w.unlock();
        }
    }

    public void writeAccess(final String uuid, final AccessLevel accessLevel) {
        w.lock();
        try {
            UserKey store = state.getKeyMap().get(uuid);
            if(store == null) {
                log.error("Can't find UserKeyCache by UUID[{}]", uuid);
                return;
            }
            store.setAccessLevel(accessLevel);
        } finally {
            w.unlock();
        }
    }

    // -------------------------------------------------- READ ------------------------------------------------------

    public UserKey read(String uuid) {
        r.lock();
        cacheCheck();
        try {
            return state.getKeyMap().get(uuid);
        } finally {
            r.unlock();
        }
    }

    public AccessLevel readAccess(String uuid) {
        r.lock();
        cacheCheck();
        try {
            UserKey store = state.getKeyMap().get(uuid);
            if(store != null) { return store.getAccessLevel();
            } else { return null; }
        } finally {
            r.unlock();
        }
    }

    public String readServerUuid() {
        r.lock();
        cacheCheck();
        try {
            return state.getServerUuid();
        } finally {
            r.unlock();
        }
    }

    // -------------------------------------------------- PACKAGE-PRIVATE ------------------------------------------------------

    void refresh() {
        w.lock();
        try {
            Stopwatch sw = Stopwatch.createStarted();
            log.info("Refreshing...");

            state.setServerUuid(DsmGen.get().genUUID());
            log.debug("New server UUID[{}]", state.getServerUuid());

            UserKeyListResponse response = securityService.getAllKeysForCache(new Request().setUuid(state.getServerUuid()));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                Map<String, UserKey> userKeyMap = response.getDtoList().stream()
                        .collect(Collectors.toMap(UserKey::getUuid, Function.<UserKey>identity()));
                state.setKeyMap(userKeyMap);
                log.debug("user keys: {}", userKeyMap.size());
            }

            sw.stop();
            TimeUnit unit = TimeUnit.MILLISECONDS;
            log.info("Refreshed...[{}]. Elapsed: {} {}",
                    MemoryUtils.getFullObjectSize(state, MemoryUtils.Unit.BYTE),
                    sw.elapsed(unit), unit);

            if(!cacheValid) {
                cacheValid = true;
                isReady.signalAll();
            }
        } catch (Throwable t) { ExcHandler.ex(t);
        } finally { w.unlock(); }
    }

    // -------------------------------------------------- PRIVATE ------------------------------------------------------

    private void cacheCheck() {
        if(!cacheValid) {
            r.unlock();
            w.lock();
            try {
                // Recheck state because another thread might have
                // acquired write lock and changed state before we did.
                while (!cacheValid) isReady.await();
                // Downgrade by acquiring read lock before releasing write lock
                r.lock();
            } catch (InterruptedException e) {
                ExcHandler.ex(e);
            } finally {
                w.unlock(); // Unlock write, still hold read
            }
        }
    }

}
