package com.dsmaster.gateway.service.security;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.security.*;
import com.dsmaster.gateway.service.EntityTransformers;
import com.dsmaster.gateway.service.security.UserKeyCacheWrapper;
import com.dsmaster.gateway.dao.SecurityDao;
import com.dsmaster.gateway.entity.security.UserKeyEntity;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DtoTransformers;
import com.dsmaster.gateway.util.Validation;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by smeshkov on 8/1/14.
 */

@Service
public class SecurityServiceImpl implements DsmApiService, SecurityService.Iface {

    @Autowired
    private SecurityDao dao;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new SecurityService.Processor<>(AppCtxHolder.get().getBean(SecurityService.Iface.class));
    }

    @Override
    @Validation
    @Transactional(readOnly = true)
    public UserKeyListResponse getAllKeys(GetUserKeyListRqstLvlA request) throws TException {
        return getUserKeyList();
    }

    @Override
    @Transactional(readOnly = true)
    public UserKeyListResponse getAllKeysForCache(Request request) throws TException {
        return getUserKeyList();
    }

    @Override
    @Validation
    @Transactional
    public Response saveKey(UserKeyRqstLvlA request) throws TException {
        UserKey dto = request.getDto();

        // Important check
        if(StringUtils.isEmpty(dto.getUuid())) {
            return new Response(StatusCode.ILLEGAL_ARGUMENTS, "UUID is empty!", "");
        }

        UserKeyEntity entity = dao.find(dto.getUuid());
        if(entity == null) {
            UserKeyEntity newEntity = Fns.mutate(() -> dto, UserKeyEntity::new, EntityTransformers.toUserKeyEntity);
            newEntity = dao.persist(newEntity);
            if(newEntity == null) {
                return new Response(StatusCode.UNKNOWN_ERROR, "", "");
            }
        } else {
            Fns.mutate(() -> dto, () -> entity, EntityTransformers.toUserKeyEntity);
        }
        UserKeyCacheWrapper.get().write(dto);
        return new Response();
    }

    @Override
    @Validation
    @Transactional
    public void saveKeyOneway(UserKeyRqstLvlA request) throws TException {
        saveKey(request);
    }

    @Override
    @Validation
    @Transactional(readOnly = true)
    public UserKeyListResponse findByParams(UserKeyParamsRqstA request) throws TException {
        List<UserKeyEntity> entities = dao.findByParams(request.getUuid(), request.getUuids(), request.getAccessLevel(), request.getAccessLevels());
        if(CollectionUtils.isEmpty(entities)) {
            return new UserKeyListResponse()
                    .setResponse(new Response(StatusCode.OBJECT_NOT_FOUND, "Can't find entities with request: " + request, ""));
        }
        return new UserKeyListResponse()
                .setResponse(new Response())
                .setDtoList(entities.stream()
                        .map(entity -> Fns.mutate(() -> entity, UserKey::new, DtoTransformers.toUserKeyFn))
                        .collect(Collectors.toList()));
    }

    // -------------------------------------------------- PRIVATE ------------------------------------------------------

    private UserKeyListResponse getUserKeyList() {
        List<UserKeyEntity> entities = dao.findByParams(null, null, null, null);
        if(CollectionUtils.isEmpty(entities)) {
            return new UserKeyListResponse(new Response(StatusCode.OBJECT_NOT_FOUND, "", ""));
        }
        return new UserKeyListResponse().setDtoList(entities.stream()
                .map(entity -> Fns.mutate(() -> entity, UserKey::new, DtoTransformers.toUserKeyFn))
                .collect(Collectors.toList()));
    }
}
