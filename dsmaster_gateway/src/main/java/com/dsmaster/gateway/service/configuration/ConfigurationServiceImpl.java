package com.dsmaster.gateway.service.configuration;

import com.dsmaster.api.gateway.request.StringRequest;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.configuration.ConfigurationListResponse;
import com.dsmaster.api.gateway.service.configuration.ConfigurationResponse;
import com.dsmaster.api.gateway.service.configuration.ConfigurationService;
import com.dsmaster.gateway.dao.ConfigurationDao;
import com.dsmaster.gateway.entity.configuration.ConfigurationEntity;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DtoTransformers;
import com.dsmaster.gateway.util.Validation;
import com.dsmaster.model.dto.configuration.Configuration;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.commons.collections.CollectionUtils;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by smeshkov on 7/16/14.
 */
@Service
public class ConfigurationServiceImpl implements DsmApiService, ConfigurationService.Iface {

    @Autowired
    private ConfigurationDao configurationDao;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new ConfigurationService.Processor(AppCtxHolder.get().getBean(ConfigurationService.Iface.class));
    }

    @Override
    @Validation
    @Transactional(readOnly = true)
    public ConfigurationListResponse findByKey(StringRequest request) throws TException {
        List<ConfigurationEntity> entities = configurationDao.findByKey(request.getValue());
        if(CollectionUtils.isEmpty(entities)) {
            return new ConfigurationListResponse().setResponse(
                    new Response(StatusCode.OBJECT_NOT_FOUND, "Can't find entities. Request: " + request, "" ));
        }
        return new ConfigurationListResponse(new Response())
                .setConfigurationList(entities.stream()
                                .map(entity -> Fns.mutate(() -> entity, Configuration::new, DtoTransformers.toConfigurationFn))
                                .collect(Collectors.toList())
                );
    }

    @Override
    @Validation
    @Transactional(readOnly = true)
    public ConfigurationResponse findByExactKey(StringRequest request) throws TException {
        ConfigurationEntity entity = configurationDao.find(request.getValue());
        if(entity == null) {
            return new ConfigurationResponse().setResponse(
                    new Response(StatusCode.OBJECT_NOT_FOUND, "Can't find entity. Request: " + request, "" ));
        }
        return new ConfigurationResponse(new Response())
                .setConfiguration(Fns.mutate(() -> entity, Configuration::new, DtoTransformers.toConfigurationFn));
    }

}
