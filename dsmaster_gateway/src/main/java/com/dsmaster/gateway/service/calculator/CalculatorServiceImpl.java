package com.dsmaster.gateway.service.calculator;

import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.service.calculator.*;
import com.dsmaster.gateway.dao.CalculatorDao;
import com.dsmaster.gateway.entity.calculator.AttributePerformanceEntity;
import com.dsmaster.gateway.model.AttributePerformance;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.EntityTransformers;
import com.dsmaster.model.dto.profile.Build;
import com.dsmaster.model.dto.profile.BuildBonuses;
import com.dsmaster.model.dto.profile.ItemList;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * User: artem_marinov
 * Date: 31.07.14
 * Time: 16:42
 */
@Service
public class CalculatorServiceImpl implements DsmApiService, CalculatorService.Iface, CalculatorServicePrivate  {

    @Autowired
    private CalculatorDao calculatorDao;

    @Autowired
    private BuildCalculator buildCalculator;

    @Autowired
    private WeaponCalculator weaponCalculator;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new CalculatorService.Processor(AppCtxHolder.get().getBean(CalculatorService.Iface.class));
    }

    @Override
    public BuildResponse calculateBuildBonus(BuildRequest request) throws TException {
        Build build = request.getBuild();

        BuildBonuses buildBonuses = buildCalculator.calculateBonuses(build);
        build.setBuildBonuses(buildBonuses);

        return new BuildResponse(new Response(), build);
    }

    @Override
    public ItemListResponse calculateItemListBonuses(ItemListRequest request) throws TException {
        ItemList itemList = request.getItemList();
        weaponCalculator.calculateNSetBonuses(itemList);
        return new ItemListResponse(new Response(), itemList);
    }


    // P R I V A T E    U S A G E

    @Override
    @Transactional(readOnly = true)
    public List<AttributePerformance> getAttributePerformances() {
        List<AttributePerformanceEntity> attrPerformanceEntities = calculatorDao.getAttributePerformances();

        List<AttributePerformance> attributePerformances = attrPerformanceEntities.stream()
                .map(i -> Fns.mutate(() -> i, AttributePerformance::new, EntityTransformers.toAttributePerformance))
                .collect(Collectors.toList());
        return attributePerformances;
    }

}
