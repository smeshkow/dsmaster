package com.dsmaster.gateway.service.calculator;

import com.dsmaster.api.gateway.service.calculator.CalculatorService;
import com.dsmaster.gateway.service.DsmApiServer;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DsmServer;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * User: artem_marinov
 * Date: 31.07.14
 * Time: 17:23
 */
public class CalculatorServer extends DsmApiServer {

    @Autowired
    private CalculatorService.Iface calculatorService;

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) calculatorService).getProcessor();
    }

}
