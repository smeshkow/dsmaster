package com.dsmaster.gateway.service.article;

import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.article.*;
import com.dsmaster.gateway.dao.ArticleDao;
import com.dsmaster.gateway.entity.article.ArticleEntity;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DtoTransformers;
import com.dsmaster.gateway.service.EntityTransformers;
import com.dsmaster.gateway.util.Validation;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by smeshkov on 8/1/14.
 */

@Service
public class ArticleServiceImpl implements DsmApiService, ArticleService.Iface {

    @Autowired
    private ArticleDao dao;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new ArticleService.Processor<>(AppCtxHolder.get().getBean(ArticleService.Iface.class));
    }

    @Override
    @Transactional
    @Validation
    public ArticleResponse create(ArticleRequest request) throws TException {
        ArticleResponse response = new ArticleResponse();
        ArticleEntity entity = Fns.mutate(() -> request.getArticle(), ArticleEntity::new, EntityTransformers.toArticleEntity);
        final ArticleEntity finalEntity = dao.persist(entity);
        if(entity == null || entity.getId() == null) {
            response.setResponse(new Response(StatusCode.UNKNOWN_ERROR, "", ""));
            return response;
        }
        Article article = Fns.mutate(() -> finalEntity, Article::new, DtoTransformers.toArticleFn);
        response.setResponse(new Response());
        response.setArticle(article);
        return response;
    }

    @Override
    @Transactional
    @Validation
    public ArticleResponse update(ArticleRequest request) throws TException {
        if(request.getArticle().getId() <= 0) {
            return new ArticleResponse(
                    new Response(StatusCode.OBJECT_NOT_FOUND, "Can't find entity with ID=" + request.getArticle().getId(), ""));
        }
        ArticleEntity entity = dao.find(request.getArticle().getId());
        if(entity == null || entity.getId() == null) {
            return new ArticleResponse(
                    new Response(StatusCode.UNKNOWN_ERROR, "Can't find entity with ID=" + request.getArticle().getId(), ""));
        }
        Fns.mutate(() -> request.getArticle(), () -> entity, EntityTransformers.toArticleEntity);
        Article article = Fns.mutate(() -> entity, Article::new, DtoTransformers.toArticleFn);
        return new ArticleResponse().setResponse(new Response()).setArticle(article);
    }

    @Override
    @Transactional
    @Validation
    public ArticleResponse save(ArticleRequest request) throws TException {
        Article article = request.getArticle();

        // Important check
        if(StringUtils.isEmpty(article.getName())) {
            return new ArticleResponse(new Response(StatusCode.ILLEGAL_ARGUMENTS, "Article name is empty!", ""));
        }

        if(article.getId() > 0) {
            return update(request);
        } else {
            ArticleListResponse response = findByParams(new ArticleParamsRequest(request.getRequest()).setName(article.getName()));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                int size = response.getArticleList().size();
                if(size == 1) {
                    article.setId(response.getArticleList().get(0).getId());
                    return update(request.setArticle(article));
                } else if (size == 0) {
                    return create(request);
                } else {
                    String errMsg = String.format("Found [%d] entities with name [%s]", size, article.getName());
                    return new ArticleResponse(new Response(StatusCode.ILLEGAL_STATUS, errMsg, ""));
                }
            } else {
                return create(request);
            }
        }
    }

    @Override
    @Transactional
    @Validation
    public void saveOneway(ArticleRequest request) throws TException {
        save(request);
    }

    @Override
    @Transactional(readOnly = true)
    @Validation
    public ArticleListResponse findByParams(ArticleParamsRequest request) throws TException {
        Set<Byte> typeIds = CollectionUtils.isNotEmpty(request.getTypes()) ? request.getTypes()
                .stream().map(Fns.tEnumToByte).collect(Collectors.toSet()) : null;
        List<ArticleEntity> entities = dao.findByParams(request.getName(), request.getNames(), typeIds);
        if(CollectionUtils.isEmpty(entities)) {
            return new ArticleListResponse()
                    .setResponse(new Response(StatusCode.OBJECT_NOT_FOUND, "Can't find entities with request: " + request, ""));
        }
        return new ArticleListResponse()
                .setResponse(new Response())
                .setArticleList(entities.stream()
                        .map(entity -> Fns.mutate(() -> entity, Article::new, DtoTransformers.toArticleFn))
                        .collect(Collectors.toList()));
    }
}
