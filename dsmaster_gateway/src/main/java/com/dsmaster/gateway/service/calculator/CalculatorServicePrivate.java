package com.dsmaster.gateway.service.calculator;

import com.dsmaster.gateway.model.AttributePerformance;

import java.util.List;

/**
 * User: artem_marinov
 * Date: 22.08.14
 * Time: 16:45
 */
public interface CalculatorServicePrivate {

    List<AttributePerformance> getAttributePerformances();

}
