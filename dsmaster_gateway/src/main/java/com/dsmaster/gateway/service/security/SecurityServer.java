package com.dsmaster.gateway.service.security;

import com.dsmaster.api.gateway.service.security.SecurityService;
import com.dsmaster.gateway.service.DsmApiServer;
import com.dsmaster.gateway.service.DsmApiService;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by smeshkov on 8/1/14.
 */
public class SecurityServer extends DsmApiServer {

    private static final long REFRESH_DELAY = 1_800_000L;

    @Autowired
    private SecurityService.Iface securityService;

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) securityService).getProcessor();
    }

    @Override
    protected void beforeStart() {
        super.beforeStart();
        UserKeyCacheWrapper.get().refresh();
    }

    @Scheduled(fixedDelay=REFRESH_DELAY, initialDelay = REFRESH_DELAY)
    public void rebuild() {
        UserKeyCacheWrapper.get().refresh();
    }
}
