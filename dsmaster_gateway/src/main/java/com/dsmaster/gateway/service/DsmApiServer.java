package com.dsmaster.gateway.service;

import com.dsmaster.model.dto.common.ApiMode;
import com.google.common.base.Objects;

/**
 * Created by smeshkov on 6/27/14.
 */

public abstract class DsmApiServer extends DsmServer {

    private ApiMode mode;
    private String host;
    private Integer port;

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public ApiMode getMode() {
        return mode;
    }

    @Override
    public void setMode(ApiMode mode) {
        this.mode = mode;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("mode", mode)
                .add("host", host)
                .add("port", port)
                .omitNullValues()
                .toString();
    }
}
