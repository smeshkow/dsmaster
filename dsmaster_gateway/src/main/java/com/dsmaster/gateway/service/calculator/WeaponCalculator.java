package com.dsmaster.gateway.service.calculator;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.service.stuff.StuffService;
import com.dsmaster.api.gateway.service.stuff.WeaponShieldListResponse;
import com.dsmaster.model.dto.profile.Build;
import com.dsmaster.model.dto.profile.BuildBonuses;
import com.dsmaster.model.dto.profile.ItemList;
import com.dsmaster.model.dto.stuff.WeaponShield;
import com.dsmaster.model.dto.stuff.attributes.AttackAttribute;
import com.google.common.base.Preconditions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * User: artem_marinov
 * Date: 25.08.14
 * Time: 16:51
 */
@Service
public class WeaponCalculator {

    @Autowired
    private CalculatorServicePrivate calculatorServicePrivate;

    @Autowired
    private StuffService.Iface stuffService;

    private Map<Integer, WeaponShield> weaponShieldCache = new HashMap<>();

    @PostConstruct
    private void postConstruct() throws TException {
        populateWeaponShieldCache();
    }

    private void populateWeaponShieldCache() throws TException {
        WeaponShieldListResponse weaponShieldsResponse = stuffService.findWeaponShields(new Request());
        List<WeaponShield> weaponShields = weaponShieldsResponse.getWeaponShields();
        weaponShields.stream().forEach(i -> {
            weaponShieldCache.put(i.getId(), i);
        });
    }

    public void calculateNSetBonuses(ItemList itemList) {
        Build build = itemList.getBuild();

        if(! build.isSetBuildBonuses()) {
            throw new IllegalArgumentException("Build bonuses not calculated yet");
        }

        BuildBonuses buildBonuses = build.getBuildBonuses();

        calcNSetWeaponAttackBonuses(itemList::setAttackBonusLeft1, itemList.getLeft1Id(), buildBonuses, itemList::isSetLeft1Id);
        calcNSetWeaponAttackBonuses(itemList::setAttackBonusLeft2, itemList.getLeft2Id(), buildBonuses, itemList::isSetLeft2Id);
        calcNSetWeaponAttackBonuses(itemList::setAttackBonusLeft3, itemList.getLeft3Id(), buildBonuses, itemList::isSetLeft3Id);

        calcNSetWeaponAttackBonuses(itemList::setAttackBonusRight1, itemList.getRight1Id(), buildBonuses, itemList::isSetRight1Id);
        calcNSetWeaponAttackBonuses(itemList::setAttackBonusRight2, itemList.getRight2Id(), buildBonuses, itemList::isSetRight2Id);
        calcNSetWeaponAttackBonuses(itemList::setAttackBonusRight3, itemList.getRight3Id(), buildBonuses, itemList::isSetRight3Id);
    }

    public AttackAttribute calculateWeaponAttackBonuses(WeaponShield weaponShield, BuildBonuses buildBonuses) {
        Preconditions.checkState(weaponShield != null, "weaponShield is null");

        AttackAttribute attackAttribute = new AttackAttribute();

        Short phyFromStr = calcAdditionalDamage(buildBonuses.getAtkStr(), weaponShield.getStrScalingPercent());
        Short phyFromDex = calcAdditionalDamage(buildBonuses.getAtkDex(), weaponShield.getDexScalingPercent());
        attackAttribute.setPhysicalAtk(Integer.valueOf(phyFromDex + phyFromStr).shortValue());

        Short mag = calcAdditionalDamage(buildBonuses.getAtkMagic(), weaponShield.getMagicScalingPercent());
        attackAttribute.setMagicAtk(mag);

        Short fire = calcAdditionalDamage(buildBonuses.getAtkFire(), weaponShield.getFireScalingPercent());
        attackAttribute.setFireAtk(fire);

        Short lighting = calcAdditionalDamage(buildBonuses.getAtkLighting(), weaponShield.getLightningScalingPercent());
        attackAttribute.setLightningAtk(lighting);

        Short dark = calcAdditionalDamage(buildBonuses.getAtkDark(), weaponShield.getDarkScalingPercent());
        attackAttribute.setDarkAtk(dark);

        //TODO I need more data from the game
        //TODO Bleed
        //TODO Poison

        return attackAttribute;
    }

    private void calcNSetWeaponAttackBonuses(Consumer<AttackAttribute> consumer, int weaponShieldId,
                                             BuildBonuses buildBonuses, Supplier<Boolean> evalCondition) {
        if(evalCondition.get()) {
            WeaponShield weaponShield = weaponShieldCache.get(weaponShieldId);
            AttackAttribute attackAttribute = calculateWeaponAttackBonuses(weaponShield, buildBonuses);
            consumer.accept(attackAttribute);
        }
    }


    private Short calcAdditionalDamage(Short baseBonusDamage, Double percentage) {
        return Double.valueOf(baseBonusDamage * percentage / 100).shortValue();
    }

}
