package com.dsmaster.gateway.service.article;

import com.dsmaster.api.gateway.service.article.ArticleService;
import com.dsmaster.gateway.service.DsmApiServer;
import com.dsmaster.gateway.service.DsmApiService;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by smeshkov on 8/1/14.
 */
public class ArticleServer extends DsmApiServer {

    @Autowired
    private ArticleService.Iface articleService;

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) articleService).getProcessor();
    }
}
