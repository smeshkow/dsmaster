package com.dsmaster.gateway.service;

import com.dsmaster.api.DsmApi;
import com.dsmaster.api.util.PropertyPlaceholderCfg;
import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.ExcHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TProcessor;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Created by smeshkov on 6/27/14.
 */

public abstract class DsmServer implements DsmApi {

    protected final Logger log =
            LoggerFactory.getLogger(getClass());

    private static final TimeUnit TIME_UNIT = TimeUnit.MILLISECONDS;
    private static final long STOP_TIMEOUT = 60000L;
    private final ExecutorService serverExecutor = Executors.newSingleThreadExecutor();
    private Optional<ServerRunner> serverRunner;

    @Override
    public void start() {
        if(StringUtils.isNotEmpty(getHost()) && getPort() != null) {
            beforeStart();
            serverRunner = Optional.of(new ServerRunner(getMode(), getHost(), getPort()));
            serverExecutor.execute(serverRunner.get());
        } else {
            serverRunner = Optional.empty();
            log.error(String.format("Can't start, host=[%s], port=[%d], mode=[%s]", getHost(), getPort(), getMode()));
        }
    }

    @PreDestroy
    public void stop() {
        serverRunner.ifPresent(r -> r.stop());
        serverExecutor.shutdown();
        try { serverExecutor.awaitTermination(STOP_TIMEOUT, TIME_UNIT);
        } catch (InterruptedException e) { ExcHandler.interruption(e); }
        log.info(String.format("Stopped -> [%s:%d] in %s mode", getHost(), getPort(), getMode()));
    }

    protected void beforeStart() {}

    protected abstract TProcessor getProcessor();

    private class ServerRunner implements Runnable {

        private TServer server;
        private final ApiMode mode;
        private final String host;
        private final Integer port;

        private ServerRunner(ApiMode mode, String host, Integer port) {
            this.mode = mode;
            this.host = host;
            this.port = port;
        }

        @Override
        public void run() {
            int minThreads = Runtime.getRuntime().availableProcessors() + 1;
            int maxThreads = minThreads + 4;// +4 because I/O can wait
            try {
                TServerTransport transport;
                InetAddress inetAddress = InetAddress.getByName(host);
                if(ApiMode.SECURE.equals(mode)) {
                    TSSLTransportFactory.TSSLTransportParameters params = new TSSLTransportFactory.TSSLTransportParameters();
                    params.setKeyStore("../../lib/java/dsm/.keystore", "thrift", null, null);
                    transport = TSSLTransportFactory.getServerSocket(port, 0, inetAddress, params);
                } else {
                    transport = new TServerSocket(new ServerSocket(port, maxThreads, inetAddress));
                }
                TThreadPoolServer.Args args = new TThreadPoolServer.Args(transport);
                args.processor(getProcessor());
                args.minWorkerThreads(minThreads);
                args.maxWorkerThreads(maxThreads);
                server = new TThreadPoolServer(args);
                log.info(String.format("Starting -> [%s:%d] in %s mode", host, port, mode));
                server.serve();
            } catch (Exception e) {
                stop();
                ExcHandler.ex(e);
            }
        }

        private boolean isServing() {
            return server != null && server.isServing();
        }

        public void stop() {
            if(isServing()) {
                server.stop();
                log.info("Stopped");
            } else {
                log.info("Wasn't running");
            }
        }
    }

}
