package com.dsmaster.gateway.service.stuff;

import com.dsmaster.api.gateway.service.stuff.StuffService;
import com.dsmaster.gateway.service.DsmApiServer;
import com.dsmaster.gateway.service.DsmApiService;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by smeshkov on 6/27/14.
 */

public class StuffServer extends DsmApiServer {

    @Autowired
    private StuffService.Iface stuffService;

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) stuffService).getProcessor();
    }
}
