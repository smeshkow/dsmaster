package com.dsmaster.gateway.service.profile;

import com.dsmaster.api.gateway.service.profile.ProfileService;
import com.dsmaster.gateway.service.DsmApiServer;
import com.dsmaster.gateway.service.DsmApiService;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by smeshkov on 6/27/14.
 */

public class ProfileServer extends DsmApiServer {

    @Autowired
    private ProfileService.Iface profileService;

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) profileService).getProcessor();
    }
}
