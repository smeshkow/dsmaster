package com.dsmaster.gateway.service.profile;

import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.profile.AuthRequest;
import com.dsmaster.api.gateway.service.profile.ProfileService;
import com.dsmaster.api.gateway.service.profile.RegisterRqst;
import com.dsmaster.api.gateway.service.security.AuthResponse;
import com.dsmaster.api.gateway.service.security.SecurityService;
import com.dsmaster.api.gateway.service.security.UserKeyRqstLvlA;
import com.dsmaster.api.util.DsmGen;
import com.dsmaster.gateway.dao.ProfileDao;
import com.dsmaster.gateway.entity.profile.ProfileEntity;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DtoTransformers;
import com.dsmaster.gateway.util.RequestFactory;
import com.dsmaster.gateway.util.Validation;
import com.dsmaster.model.dto.profile.Profile;
import com.dsmaster.model.dto.profile.ProfileStatus;
import com.dsmaster.model.dto.security.AccessLevel;
import com.dsmaster.model.dto.security.AuthStatus;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.function.Supplier;

/**
 * User: artem_marinov
 * Date: 27.06.14
 * Time: 16:21
 */
@Service
public class ProfileServiceImpl implements DsmApiService, ProfileService.Iface {

    private static final AccessLevel UNAUTHORIZED = AccessLevel.Z;
    private static final AccessLevel AUTHORIZED = AccessLevel.Y;

    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private SecurityService.Iface securityService;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new ProfileService.Processor(AppCtxHolder.get().getBean(ProfileService.Iface.class));
    }

    @Override
    @Validation
    @Transactional
    public Response registerUser(RegisterRqst request) throws TException {
        ProfileEntity entity = profileDao.findByExactEmail(request.getEmail());
        if(entity != null) return new Response(StatusCode.ALREADY_EXISTS, "", "");

        Date now = new Date();
        DsmGen gen = DsmGen.get();
        String uuid = gen.genUUID();

        entity = new ProfileEntity();
        entity.setUuid(uuid);
        entity.setEmail(request.getEmail());
        entity.setPasswordHash(gen.hashPwd(request.getPassword()));
        entity.setTimestampCreated(now.getTime());
        entity.setTimestampUpdated(now.getTime());
        entity = profileDao.persist(entity);

        if(entity == null) return new Response(StatusCode.UNKNOWN_ERROR, "", "");

        // TODO create KeyStore for Thrift API secure mode

        return securityService.saveKey(RequestFactory.system(UserKeyRqstLvlA.class).setDto(new UserKey(uuid, AUTHORIZED, "")));
    }

    @Override
    @Validation
    @Transactional(readOnly = true)
    public AuthResponse authUser(AuthRequest request) throws TException {
        ProfileEntity entity = profileDao.findByExactEmail(request.getEmail());

        if(entity == null) return new AuthResponse(
                new Response(StatusCode.OBJECT_NOT_FOUND, "", ""), AuthStatus.USER_NOT_EXISTS, UNAUTHORIZED);

        String passwordHash = DigestUtils.md5Hex(request.getPassword());
        Profile profile = Fns.mutate(() -> entity, Profile::new, DtoTransformers.toProfileFn);

        if ( !profile.getPasswordHash().equals( passwordHash ) ) {
            return new AuthResponse(
                    new Response(StatusCode.ILLEGAL_ARGUMENTS, "", ""), AuthStatus.WRONG_PASSWORD, UNAUTHORIZED);
        }

        if ( profile.getStatus() == ProfileStatus.BANNED ) {
            return new AuthResponse(
                    new Response(StatusCode.NO_PRIVILEGES, "", ""), AuthStatus.USER_BANNED, UNAUTHORIZED);
        }

        if ( profile.getStatus() == ProfileStatus.ON_APPROVE ) {
            return new AuthResponse(
                    new Response(StatusCode.NO_PRIVILEGES, "", ""), AuthStatus.USER_ONAPPROVE, UNAUTHORIZED);
        }

        if ( profile.getStatus() == ProfileStatus.BLOCKED ) {
            return new AuthResponse(
                    new Response(StatusCode.NO_PRIVILEGES, "", ""), AuthStatus.USER_BLOCKED, UNAUTHORIZED);
        }

        if ( profile.getStatus() == ProfileStatus.DELETED ) {
            return new AuthResponse(
                    new Response(StatusCode.NO_PRIVILEGES, "", ""), AuthStatus.USER_DELETED, UNAUTHORIZED);
        }

        if ( profile.getStatus() == ProfileStatus.INACTIVE ) {
            return new AuthResponse(
                    new Response(StatusCode.NO_PRIVILEGES, "", ""), AuthStatus.USER_INACTIVE, UNAUTHORIZED);
        }

        return new AuthResponse(new Response(), AuthStatus.OK, AUTHORIZED);
    }

}
