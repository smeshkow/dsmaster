package com.dsmaster.gateway.service;

import com.dsmaster.gateway.entity.article.ArticleEntity;
import com.dsmaster.gateway.entity.attributes.ArmorAttributesEntity;
import com.dsmaster.gateway.entity.attributes.AttributesBonusEntity;
import com.dsmaster.gateway.entity.attributes.AttributesRequirementEntity;
import com.dsmaster.gateway.entity.attributes.WeaponShieldAttributesEntity;
import com.dsmaster.gateway.entity.calculator.AttributePerformanceEntity;
import com.dsmaster.gateway.entity.security.UserKeyEntity;
import com.dsmaster.gateway.entity.stuff.ArmorEntity;
import com.dsmaster.gateway.entity.stuff.WeaponShieldEntity;
import com.dsmaster.gateway.model.AttributePerformance;
import com.dsmaster.gateway.model.StatAttribute;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.common.Addon;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.dto.stuff.*;
import com.dsmaster.model.dto.stuff.attributes.*;
import com.dsmaster.model.util.Fns;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * User: artem_marinov
 * Date: 14.07.14
 * Time: 16:58
 */
public class EntityTransformers {

    public static final Function<Byte, Scaling> byteToScaling = i -> Scaling.findByValue(i);

    public static final Function<Byte, AttackType> byteToAttackType = i -> AttackType.findByValue(i);

    public static final BiFunction<ArmorAttributes, ArmorAttributesEntity, ArmorAttributesEntity> toArmorAttributeEntityFn = (f, t) -> {
        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);

        //TODO copypaste
        Fns.doIfSupplierOk(t::setDurability, f::getDurability, f::isSetDurability);
        Fns.doIfSupplierOk(t::setWeight, f::getWeight, f::isSetWeight);

        Fns.doIfSupplierOk(t::setPhysicalDef, f::getPhysicalDef, f::isSetPhysicalDef);
        Fns.doIfSupplierOk(t::setMagicDef, f::getMagicDef, f::isSetMagicDef);
        Fns.doIfSupplierOk(t::setFireDef, f::getFireDef, f::isSetFireDef);
        Fns.doIfSupplierOk(t::setLightningDef, f::getLightningDef, f::isSetLightningDef);
        Fns.doIfSupplierOk(t::setDarkDef, f::getDarkDef, f::isSetDarkDef);

        Fns.doIfSupplierOk(t::setPoisonResist, f::getPoisonResist, f::isSetPoisonResist);
        Fns.doIfSupplierOk(t::setBleedResist, f::getBleedResist, f::isSetBleedResist);
        Fns.doIfSupplierOk(t::setPetrifyResist, f::getPetrifyResist, f::isSetPetrifyResist);
        Fns.doIfSupplierOk(t::setCurseResist, f::getCurseResist, f::isSetCurseResist);

        Fns.doIfSupplierOk(t::setPhysicalDefStrike, f::getPhysicalDefStrike, f::isSetPhysicalDefStrike);
        Fns.doIfSupplierOk(t::setPhysicalDefSlash, f::getPhysicalDefSlash, f::isSetPhysicalDefSlash);
        Fns.doIfSupplierOk(t::setPhysicalDefThrust, f::getPhysicalDefThrust, f::isSetPhysicalDefThrust);

        Fns.doIfSupplierOk(t::setPoise, f::getPoise, f::isSetPoise);

        return t;
    };

    public static final BiFunction<AttackAttribute, WeaponShieldAttributesEntity, WeaponShieldAttributesEntity> toAttackAttributeEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        Fns.doIfSupplierOk(t::setPhysicalAtk, f::getPhysicalAtk, f::isSetPhysicalAtk);
        Fns.doIfSupplierOk(t::setMagicAtk, f::getMagicAtk, f::isSetMagicAtk);
        Fns.doIfSupplierOk(t::setFireAtk, f::getFireAtk, f::isSetFireAtk);
        Fns.doIfSupplierOk(t::setLightningAtk, f::getLightningAtk, f::isSetLightningAtk);
        Fns.doIfSupplierOk(t::setDarkAtk, f::getDarkAtk, f::isSetDarkAtk);
        Fns.doIfSupplierOk(t::setPoisonAtk, f::getPoisonAtk, f::isSetPoisonAtk);
        Fns.doIfSupplierOk(t::setBleedAtk, f::getBleedAtk, f::isSetBleedAtk);

        return t;
    };

    public static final BiFunction<WeaponShieldAttributes, WeaponShieldAttributesEntity, WeaponShieldAttributesEntity> toWeaponShieldAttributeEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);

        //TODO copypaste
        Fns.doIfSupplierOk(t::setDurability, f::getDurability, f::isSetDurability);
        Fns.doIfSupplierOk(t::setWeight, f::getWeight, f::isSetWeight);

        if(f.isSetAttackAttribute()) {
            Fns.mutate(f::getAttackAttribute, () -> t, toAttackAttributeEntityFn);
        }

        Fns.doIfSupplierOk(t::setPhysicalReduction, f::getPhysicalReduction, f::isSetPhysicalReduction);
        Fns.doIfSupplierOk(t::setMagicReduction, f::getMagicReduction, f::isSetMagicReduction);
        Fns.doIfSupplierOk(t::setFireReduction, f::getFireReduction, f::isSetFireReduction);
        Fns.doIfSupplierOk(t::setLightningReduction, f::getLightningReduction, f::isSetLightningReduction);
        Fns.doIfSupplierOk(t::setDarkReduction, f::getDarkReduction, f::isSetDarkReduction);
        Fns.doIfSupplierOk(t::setPoisonReduction, f::getPoisonReduction, f::isSetPoisonReduction);
        Fns.doIfSupplierOk(t::setBleedReduction, f::getBleedReduction, f::isSetBleedReduction);
        Fns.doIfSupplierOk(t::setCurseReduction, f::getCurseReduction, f::isSetCurseReduction);
        Fns.doIfSupplierOk(t::setPetrifyReduction, f::getPetrifyReduction, f::isSetPetrifyReduction);

        Fns.doIfSupplierOk(t::setCounterStrength, f::getCounterStrength, f::isSetCounterStrength);
        Fns.doIfSupplierOk(t::setStability, f::getStability, f::isSetStability);
        Fns.doIfSupplierOk(t::setPoiseDamage, f::getPoiseDamage, f::isSetPoiseDamage);
        Fns.doIfSupplierOk(t::setShortRange, f::getShortRange, f::isSetShortRange);
        Fns.doIfSupplierOk(t::setCastSpeed, f::getCastSpeed, f::isSetCastSpeed);

        return t;
    };

    public static final BiFunction<AttributesBonus, AttributesBonusEntity, AttributesBonusEntity> toAttributesBonusEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);

        Fns.doIfSupplierOk(t::setBonusStr, f::getBonusStr, f::isSetBonusStr);
        Fns.doIfSupplierOk(t::setBonusDex, f::getBonusDex, f::isSetBonusDex);
        Fns.doIfSupplierOk(t::setBonusInt, f::getBonusInt, f::isSetBonusInt);
        Fns.doIfSupplierOk(t::setBonusFaith, f::getBonusFaith, f::isSetBonusFaith);
//        Fns.doIfSupplierOk(t::setBonusPhysicalDef, f::getBonusPhysicalDef, f::isSetBonusPhysicalDef);
//        Fns.doIfSupplierOk(t::setBonusResistance, f::getBonusResistance, f::isSetBonusResistance);

        return t;
    };

    public static final BiFunction<AttributesRequirement, AttributesRequirementEntity, AttributesRequirementEntity> toAttributesRequirementEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);

        Fns.doIfSupplierOk(t::setRequiredStr, f::getRequiredStr, f::isSetRequiredStr);
        Fns.doIfSupplierOk(t::setRequiredDex, f::getRequiredDex, f::isSetRequiredDex);
        Fns.doIfSupplierOk(t::setRequiredInt, f::getRequiredInt, f::isSetRequiredInt);
        Fns.doIfSupplierOk(t::setRequiredFaith, f::getRequiredFaith, f::isSetRequiredFaith);

        return t;
    };


    public static final BiFunction<WeaponShield, WeaponShieldEntity, WeaponShieldEntity> toWeaponShieldEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);
        Fns.doIfSupplierOk(t::setAddonId, f::getAddon, Fns.tEnumToByte, f::isSetAddon);
        Fns.doIfSupplierOk(t::setName, f::getName, f::isSetName);
        Fns.doIfSupplierOk(t::setDescription, f::getDescription, f::isSetDescription);

        Fns.doIfSupplierOk(t::setWeaponCategoryId, f::getWeaponCategory, Fns.tEnumToByte, f::isSetWeaponCategory);
        Fns.doIfSupplierOk(t::setAttackTypeId1, f::getAttackType1, Fns.tEnumToByte, f::isSetAttackType1);
        Fns.doIfSupplierOk(t::setAttackTypeId2, f::getAttackType2, Fns.tEnumToByte, f::isSetAttackType2);

        Fns.doIfSupplierOk(t::setStrScalingId, f::getStrScaling, Fns.tEnumToByte, f::isSetStrScaling);
        Fns.doIfSupplierOk(t::setDexScalingId, f::getDexScaling, Fns.tEnumToByte, f::isSetDexScaling);
        Fns.doIfSupplierOk(t::setMagicScalingId, f::getMagicScaling, Fns.tEnumToByte, f::isSetMagicScaling);
        Fns.doIfSupplierOk(t::setFireScalingId, f::getFireScaling, Fns.tEnumToByte, f::isSetFireScaling);
        Fns.doIfSupplierOk(t::setLightningScalingId, f::getLightningScaling, Fns.tEnumToByte, f::isSetLightningScaling);
        Fns.doIfSupplierOk(t::setDarkScalingId, f::getDarkScaling, Fns.tEnumToByte, f::isSetDarkScaling);

        Fns.doIfSupplierOk(t::setStrScalingPercent, f::getStrScalingPercent, f::isSetStrScalingPercent);
        Fns.doIfSupplierOk(t::setDexScalingPercent, f::getDexScalingPercent, f::isSetDexScalingPercent);
        Fns.doIfSupplierOk(t::setMagicScalingPercent, f::getMagicScalingPercent, f::isSetMagicScalingPercent);
        Fns.doIfSupplierOk(t::setFireScalingPercent, f::getFireScalingPercent, f::isSetFireScalingPercent);
        Fns.doIfSupplierOk(t::setLightningScalingPercent, f::getLightningScalingPercent, f::isSetLightningScalingPercent);
        Fns.doIfSupplierOk(t::setDarkScalingPercent, f::getDarkScalingPercent, f::isSetDarkScalingPercent);

        //TODO Poison, Bleed percentage?

        if(f.isSetWeaponShieldAttributes()) {
            final WeaponShieldAttributesEntity wsAttributesEntity = t.getWeaponShieldAttributesEntity() != null ?
                    t.getWeaponShieldAttributesEntity() :
                    new WeaponShieldAttributesEntity();

            Fns.mutate(f::getWeaponShieldAttributes, () -> wsAttributesEntity, toWeaponShieldAttributeEntityFn);
            t.setWeaponShieldAttributesEntity(wsAttributesEntity);
        }

        //TODO copypaste
        if(f.isSetAttributesBonus()) {
            final AttributesBonusEntity attributeBonusEntity = t.getAttributesBonusEntity() != null ?
                    t.getAttributesBonusEntity() :
                    new AttributesBonusEntity();

            Fns.mutate(f::getAttributesBonus, () -> attributeBonusEntity, toAttributesBonusEntityFn);
            t.setAttributesBonusEntity(attributeBonusEntity);
        }

        //TODO copypaste
        if(f.isSetAttributesRequirement()) {
            final AttributesRequirementEntity attributeReqEntity = t.getAttributesRequirementEntity() != null ?
                    t.getAttributesRequirementEntity() :
                    new AttributesRequirementEntity();

            Fns.mutate(f::getAttributesRequirement, () -> attributeReqEntity, toAttributesRequirementEntityFn);
            t.setAttributesRequirementEntity(attributeReqEntity);
        }

        return t;
    };

    public static final BiFunction<Armor, ArmorEntity, ArmorEntity> toArmorEntityFn = (f, t) -> {
        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);
        Fns.doIfSupplierOk(t::setAddonId, f::getAddon, Fns.tEnumToByte, f::isSetAddon);
        Fns.doIfSupplierOk(t::setName, f::getName, f::isSetName);
        Fns.doIfSupplierOk(t::setDescription, f::getDescription, f::isSetDescription);

        Fns.doIfSupplierOk(t::setArmorTypeId, f::getArmorType, Fns.tEnumToByte, f::isSetArmorType);
        Fns.doIfSupplierOk(t::setScalingId, f::getScaling, Fns.tEnumToByte, f::isSetScaling);

        if(f.isSetArmorAttributes()) {
            final ArmorAttributesEntity armorAttributesEntity = t.getArmorAttributesEntity() != null ?
                    t.getArmorAttributesEntity() :
                    new ArmorAttributesEntity();

            Fns.mutate(f::getArmorAttributes, () -> armorAttributesEntity, toArmorAttributeEntityFn);
            t.setArmorAttributesEntity(armorAttributesEntity);
        }

        //TODO copypaste
        if(f.isSetAttributesBonus()) {
            final AttributesBonusEntity attributeBonusEntity = t.getAttributesBonusEntity() != null ?
                    t.getAttributesBonusEntity() :
                    new AttributesBonusEntity();

            Fns.mutate(f::getAttributesBonus, () -> attributeBonusEntity, toAttributesBonusEntityFn);
            t.setAttributesBonusEntity(attributeBonusEntity);
        }

        //TODO copypaste
        if(f.isSetAttributesRequirement()) {
            final AttributesRequirementEntity attributeReqEntity = t.getAttributesRequirementEntity() != null ?
                    t.getAttributesRequirementEntity() :
                    new AttributesRequirementEntity();

            Fns.mutate(f::getAttributesRequirement, () -> attributeReqEntity, toAttributesRequirementEntityFn);
            t.setAttributesRequirementEntity(attributeReqEntity);
        }

        return t;
    };

    public static final BiFunction<AttributePerformanceEntity, AttributePerformance, AttributePerformance> toAttributePerformance = (f, t) -> {
        t.setAttribute(StatAttribute.findByValue(f.getAttributeId()))
                .setAttributeValue(f.getAttributeValue())
                .setPerformance(StatAttribute.findByValue(f.getPerformanceId()))
                .setPerformanceValue(f.getPerformanceValue());
        return t;
    };

    public static final BiFunction<Article, ArticleEntity, ArticleEntity> toArticleEntity = (f, t) -> {
//        Fns.doIfSupplierOk(t::setId, f::getId, f::isSetId);
        Fns.doIfSupplierOk(t::setAddonId, f::getAddon, Fns.tEnumToByte, f::isSetAddon);
        Fns.doIfSupplierOk(t::setName, f::getName, f::isSetName);
        Fns.doIfSupplierOk(t::setDescription, f::getDescription, f::isSetDescription);

        Fns.doIfSupplierOk(t::setAcquiredFrom, f::getAcquiredFrom, f::isSetAcquiredFrom);
        Fns.doIfSupplierOk(t::setHintsAndTips, f::getHintsAndTips, f::isSetHintsAndTips);
        Fns.doIfSupplierOk(t::setTypeId, f::getType, Fns.tEnumToByte, f::isSetType);
        Fns.doIfSupplierOk(t::setLink, f::getLink, f::isSetLink);
        return t;
    };

    public static final BiFunction<UserKey, UserKeyEntity, UserKeyEntity> toUserKeyEntity = (f, t) -> {
        Fns.doIfSupplierOk(t::setUuid, f::getUuid, f::isSetUuid);
        Fns.doIfSupplierOk(t::setAccessLevelId, f::getAccessLevel, e -> e.getValue(), f::isSetAccessLevel);
        Fns.doIfSupplierOk(t::setKeyStore, f::getKeyStore, f::isSetKeyStore);
        return t;
    };




    // F R O M

    public static final BiFunction<WeaponShieldAttributesEntity, WeaponShieldAttributes, WeaponShieldAttributes> fromWeaponShieldAttributeEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        //TODO copypaste
        t.setId(f.getId());
        t.setDurability(f.getDurability());
        t.setWeight(f.getWeight());

        AttackAttribute attackAttribute = new AttackAttribute();

        attackAttribute.setPhysicalAtk(f.getPhysicalAtk());
        attackAttribute.setMagicAtk(f.getMagicAtk());
        attackAttribute.setFireAtk(f.getFireAtk());
        attackAttribute.setLightningAtk(f.getLightningAtk());
        attackAttribute.setDarkAtk(f.getDarkAtk());
        attackAttribute.setPoisonAtk(f.getPoisonAtk());
        attackAttribute.setBleedAtk(f.getBleedAtk());

        t.setAttackAttribute(attackAttribute);

        t.setPhysicalReduction(f.getPhysicalReduction());
        t.setMagicReduction(f.getMagicReduction());
        t.setFireReduction(f.getFireReduction());
        t.setLightningReduction(f.getLightningReduction());
        t.setDarkReduction(f.getDarkReduction());
        t.setPoisonReduction(f.getPoisonReduction());
        t.setBleedReduction(f.getBleedReduction());
        t.setCurseReduction(f.getCurseReduction());
        t.setPetrifyReduction(f.getPetrifyReduction());

        t.setCounterStrength(f.getCounterStrength());
        t.setStability(f.getStability());
        t.setPoiseDamage(f.getPoiseDamage());
        t.setShortRange(f.getShortRange());
        t.setCastSpeed(f.getCastSpeed());

        return t;
    };

    public static final BiFunction<AttributesBonusEntity, AttributesBonus, AttributesBonus> fromAttributesBonusEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        t.setId(f.getId());

        Fns.doIfNotNull(t::setBonusStr, f::getBonusStr);
        Fns.doIfNotNull(t::setBonusDex, f::getBonusDex);
        Fns.doIfNotNull(t::setBonusInt, f::getBonusInt);
        Fns.doIfNotNull(t::setBonusFaith, f::getBonusFaith);

//        t.setBonusPhysicalDef(f.getBonusPhysicalDef());
//        t.setBonusResistance(f.getBonusResistance());

        return t;
    };

    public static final BiFunction<AttributesRequirementEntity, AttributesRequirement, AttributesRequirement> fromAttributesRequirementEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        t.setId(f.getId());

        Fns.doIfNotNull(t::setRequiredStr, f::getRequiredStr);
        Fns.doIfNotNull(t::setRequiredDex, f::getRequiredDex);
        Fns.doIfNotNull(t::setRequiredInt, f::getRequiredInt);
        Fns.doIfNotNull(t::setRequiredFaith, f::getRequiredFaith);

        return t;
    };


    public static final BiFunction<WeaponShieldEntity, WeaponShield, WeaponShield> fromWeaponShieldEntityFn = (f, t) -> {
        if(f == null) {
            return null;
        }

        t.setId(f.getId());
        t.setAddon(Addon.findByValue(f.getAddonId()));
        t.setName(f.getName());
        t.setDescription(f.getDescription());
        t.setWeaponCategory(WeaponCategory.findByValue(f.getWeaponCategoryId()));


        Fns.doIfNotNull(t::setAttackType1, f::getAttackTypeId1, byteToAttackType);
        Fns.doIfNotNull(t::setAttackType2, f::getAttackTypeId2, byteToAttackType);

        Fns.doIfNotNull(t::setStrScaling, f::getStrScalingId, byteToScaling);
        Fns.doIfNotNull(t::setDexScaling, f::getDexScalingId, byteToScaling);
        Fns.doIfNotNull(t::setMagicScaling, f::getMagicScalingId, byteToScaling);
        Fns.doIfNotNull(t::setFireScaling, f::getFireScalingId, byteToScaling);
        Fns.doIfNotNull(t::setLightningScaling, f::getLightningScalingId, byteToScaling);
        Fns.doIfNotNull(t::setDarkScaling, f::getDarkScalingId, byteToScaling);

        t.setStrScalingPercent(f.getStrScalingPercent());
        t.setDexScalingPercent(f.getDexScalingPercent());
        t.setMagicScalingPercent(f.getMagicScalingPercent());
        t.setFireScalingPercent(f.getFireScalingPercent());
        t.setLightningScalingPercent(f.getLightningScalingPercent());
        t.setDarkScalingPercent(f.getDarkScalingPercent());

        //TODO Poison, Bleed percentage?


        if(f.getWeaponShieldAttributesEntity() != null) {

            /*final WeaponShieldAttributesEntity wsAttributesEntity = t.getWeaponShieldAttributesEntity() != null ?
                    t.getWeaponShieldAttributesEntity() :
                    new WeaponShieldAttributesEntity();*/
            WeaponShieldAttributes wsAttributes = Fns.mutate(f::getWeaponShieldAttributesEntity,
                    WeaponShieldAttributes::new,
                    fromWeaponShieldAttributeEntityFn);
            t.setWeaponShieldAttributes(wsAttributes);
        }

        //TODO copypaste
        if(f.getAttributesBonusEntity() != null) {
//            final AttributesBonusEntity attributeBonusEntity = t.getAttributesBonusEntity() != null ?
//                    t.getAttributesBonusEntity() :
//                    new AttributesBonusEntity();

            AttributesBonus attributesBonus = Fns.mutate(f::getAttributesBonusEntity,
                    AttributesBonus::new,
                    fromAttributesBonusEntityFn);
            t.setAttributesBonus(attributesBonus);
        }

        //TODO copypaste
        if(f.getAttributesRequirementEntity() != null) {
//            final AttributesRequirementEntity attributeReqEntity = t.getAttributesRequirementEntity() != null ?
//                    t.getAttributesRequirementEntity() :
//                    new AttributesRequirementEntity();

            AttributesRequirement attributesRequirement = Fns.mutate(f::getAttributesRequirementEntity,
                    AttributesRequirement::new,
                    fromAttributesRequirementEntityFn);
            t.setAttributesRequirement(attributesRequirement);
        }

        return t;
    };

}
