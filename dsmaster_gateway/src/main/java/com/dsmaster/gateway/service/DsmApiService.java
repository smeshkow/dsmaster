package com.dsmaster.gateway.service;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.util.CasCacheWrapper;
import com.dsmaster.gateway.service.security.UserKeyCacheWrapper;
import com.dsmaster.model.dto.security.AccessLevel;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TProcessor;

import java.util.List;
import java.util.function.Supplier;

/**
 * Created by smeshkov on 6/30/14.
 */
public interface DsmApiService {

    default TProcessor getProcessor() {
        return getProcessorSupplier().get();
    }

    Supplier<? extends TProcessor> getProcessorSupplier();
}
