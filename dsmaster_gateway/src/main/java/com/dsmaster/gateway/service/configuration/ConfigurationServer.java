package com.dsmaster.gateway.service.configuration;

import com.dsmaster.api.DsmApiCfg;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.request.StringRequest;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.configuration.ConfigurationListResponse;
import com.dsmaster.api.gateway.service.configuration.ConfigurationService;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.DsmServer;
import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.dto.configuration.Configuration;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.ExcHandler;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by smeshkov on 7/16/14.
 */

@Service
public class ConfigurationServer extends DsmServer {

    private static final String POSTFIX = "Server";

    // Configuration
    @Value("${api.cfg}")
    protected String apiCfg;
    @Value("${api.cfg.name}")
    protected String apiCfgName;
    @Value("${api.mode}")
    private String modeStr;
    @Value("${api.host.address}")
    private String host;
    @Value("${api.host.port}")
    private Integer port;

    @Autowired
    private ConfigurationService.Iface configurationService;

    private ApiMode mode;

    @PostConstruct
    public void init() {
        mode = ApiMode.forVal(modeStr) != null ? ApiMode.forVal(modeStr) : ApiMode.SIMPLE;
        start();
        try {
            ConfigurationListResponse response = configurationService.findByKey(new StringRequest(new Request(), apiCfg));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) AppCtxHolder.get().getAutowireCapableBeanFactory();
                String packageStr = this.getClass().getPackage().getName();
                for (Configuration cfg : response.getConfigurationList()) {
                    String cfgName = cfg.getKey().replace(apiCfg + ".", "");
                    DsmApiCfg dsmApiCfg = new DsmApiCfg(cfgName, getMode(), cfg.getStrVal(), cfg.getIntVal());
                    dsmApiCfg.initApi(beanFactory, POSTFIX, packageStr.replace("." + apiCfgName, ""));
                }
            }
        } catch (TException e) {
            ExcHandler.ex(e);
        }
    }

    @Override
    protected TProcessor getProcessor() {
        return ((DsmApiService) configurationService).getProcessor();
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public ApiMode getMode() {
        return mode;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public void setMode(ApiMode mode) {
        this.mode = mode;
    }

}
