package com.dsmaster.gateway.service;

import com.dsmaster.gateway.entity.article.ArticleEntity;
import com.dsmaster.gateway.entity.configuration.ConfigurationEntity;
import com.dsmaster.gateway.entity.profile.ProfileEntity;
import com.dsmaster.gateway.entity.security.UserKeyEntity;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.model.dto.common.Addon;
import com.dsmaster.model.dto.configuration.Configuration;
import com.dsmaster.model.dto.profile.Profile;
import com.dsmaster.model.dto.profile.ProfileStatus;
import com.dsmaster.model.dto.security.AccessLevel;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.util.Fns;

import java.util.function.BiFunction;

/**
 * Created by smeshkov on 7/16/14.
 */
public class DtoTransformers {

    public static final BiFunction<ConfigurationEntity, Configuration, Configuration> toConfigurationFn = (f, t) -> {
        if(f == null) {
            return null;
        }
        Fns.doIfNotNull(t::setKey, f::getKey);
        Fns.doIfNotNull(t::setStrVal, f::getStrVal);
        Fns.doIfNotNull(t::setIntVal, f::getIntVal);
        Fns.doIfNotNull(t::setDoubleVal, f::getDoubleVal);
        Fns.doIfNotNull(t::setDescription, f::getDescription);

        return t;
    };

    public static final BiFunction<ArticleEntity, Article, Article> toArticleFn = (f, t) -> {
        if(f == null) {
            return null;
        }
        Fns.doIfNotNull(t::setId, f::getId);
        t.setAddon(f.getAddonId() != null ? Addon.findByValue((int) f.getAddonId()) : null);
        Fns.doIfNotNull(t::setName, f::getName);
        Fns.doIfNotNull(t::setDescription, f::getDescription);
        Fns.doIfNotNull(t::setAcquiredFrom, f::getAcquiredFrom);
        Fns.doIfNotNull(t::setHintsAndTips, f::getHintsAndTips);
        t.setType(f.getTypeId() != null ? ArticleType.findByValue((int) f.getTypeId()) : null);
        Fns.doIfNotNull(t::setLink, f::getLink);
        return t;
    };

    public static final BiFunction<UserKeyEntity, UserKey, UserKey> toUserKeyFn = (f, t) -> {
        if(f == null) {
            return null;
        }
        Fns.doIfNotNull(t::setUuid, f::getUuid);
        t.setAccessLevel(f.getAccessLevelId() != null ? AccessLevel.findByValue(f.getAccessLevelId()) : null);
        Fns.doIfNotNull(t::setKeyStore, f::getKeyStore);
        return t;
    };

    public static final BiFunction<ProfileEntity, Profile, Profile> toProfileFn = (f, t) -> {
        if(f == null) {
            return null;
        }
        Fns.doIfNotNull(t::setId, f::getId);
        Fns.doIfNotNull(t::setUuid, f::getUuid);
        Fns.doIfNotNull(t::setEmail, f::getEmail);
        Fns.doIfNotNull(t::setPasswordHash, f::getPasswordHash);
        t.setStatus(f.getStatusId() != null ? ProfileStatus.findByValue((int) f.getStatusId()) : null);
        Fns.doIfNotNull(t::setTimestampCreated, f::getTimestampCreated);
        Fns.doIfNotNull(t::setTimestampUpdated, f::getTimestampUpdated);
        return t;
    };

}
