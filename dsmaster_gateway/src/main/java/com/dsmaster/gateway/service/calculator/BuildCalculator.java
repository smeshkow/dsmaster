package com.dsmaster.gateway.service.calculator;

import com.dsmaster.gateway.model.AttributePerformance;
import com.dsmaster.gateway.model.StatAttribute;
import com.dsmaster.model.dto.profile.Build;
import com.dsmaster.model.dto.profile.BuildBonuses;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: artem_marinov
 * Date: 22.08.14
 * Time: 16:42
 */
@Service
public class BuildCalculator {

    @Autowired
    private CalculatorServicePrivate calculatorServicePrivate;

    private Table<StatAttribute, Short, Map<StatAttribute, Short>> attributeBonuses = HashBasedTable.create();


    @PostConstruct
    private void postConstruct() {
        buildAttributeBonuses();
    }

    private void buildAttributeBonuses() {
        List<AttributePerformance> attributePerformances = calculatorServicePrivate.getAttributePerformances();

        attributePerformances.forEach(t -> {
            StatAttribute attr = t.getAttribute();
            Short attrValue = t.getAttributeValue();

            if (!attributeBonuses.contains(attr, attrValue)) {
                attributeBonuses.put(attr, attrValue, new HashMap());
            }

            Map<StatAttribute, Short> performanceMap = attributeBonuses.get(attr, attrValue);
            performanceMap.put(t.getPerformance(), t.getPerformanceValue());
        });
    }


    public BuildBonuses calculateBonuses(Build build) {
        BuildBonuses result = new BuildBonuses();

        result.setAtkStr(getPerformanceValue(StatAttribute.STR, build.getStr(), StatAttribute.STR_ATK_BONUS));
        result.setAtkDex(getPerformanceValue(StatAttribute.DEX, build.getDex(), StatAttribute.DEX_ATK_BONUS));


        Short lightFromFth = getPerformanceValue(StatAttribute.FTH, build.getFth(), StatAttribute.LIGHT_ATK_BONUS);
        Short fireFromFth = getPerformanceValue(StatAttribute.FTH, build.getFth(), StatAttribute.FIRE_ATK_BONUS);
        Short darkFromFth = getPerformanceValue(StatAttribute.FTH, build.getFth(), StatAttribute.DARK_ATK_BONUS);

        Short magicFromInt = getPerformanceValue(StatAttribute.INT, build.getIntelligent(), StatAttribute.MAGIC_ATK_BONUS);
        Short fireFromInt = getPerformanceValue(StatAttribute.INT, build.getIntelligent(), StatAttribute.FIRE_ATK_BONUS);
        Short darkFromInt = getPerformanceValue(StatAttribute.INT, build.getIntelligent(), StatAttribute.DARK_ATK_BONUS);

        result.setAtkLighting(lightFromFth);
        result.setAtkMagic(magicFromInt);
        result.setAtkFire(roundStat((fireFromFth + fireFromInt) / 2.0));
        result.setAtkDark(darkFromInt < darkFromFth ? darkFromInt : darkFromFth);

        return result;
    }

    private Short getPerformanceValue(StatAttribute statAttribute, short statAttributeValue, StatAttribute performance) {
        return attributeBonuses.get(statAttribute, statAttributeValue).get(performance);
    }

    private Short roundStat(Double value) {
        //TODO round, need tests
        return value.shortValue();
    }
}
