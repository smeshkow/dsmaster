package com.dsmaster.gateway.service.stuff;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.service.stuff.SaveWeaponShieldRequest;
import com.dsmaster.api.gateway.service.stuff.StuffService;
import com.dsmaster.api.gateway.service.stuff.WeaponShieldListResponse;
import com.dsmaster.gateway.dao.WeaponShieldDao;
import com.dsmaster.gateway.entity.stuff.WeaponShieldEntity;
import com.dsmaster.gateway.service.DsmApiService;
import com.dsmaster.gateway.service.EntityTransformers;
import com.dsmaster.model.dto.stuff.WeaponShield;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.Fns;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 17:49
 */
@Service
public class StuffServiceImpl implements DsmApiService, StuffService.Iface {

    @Autowired
    private WeaponShieldDao weaponShieldDao;

    @Override
    public Supplier<? extends TProcessor> getProcessorSupplier() {
        return () -> new StuffService.Processor(AppCtxHolder.get().getBean(StuffService.Iface.class));
    }

    @Override
    @Transactional(readOnly = true)
    public WeaponShieldListResponse findWeaponShields(Request request) throws TException {
        List<WeaponShieldEntity> weaponShieldEntities = weaponShieldDao.find();
        List<WeaponShield> weaponShields = weaponShieldEntities.parallelStream()
                .map(i -> Fns.mutate(() -> i, WeaponShield::new, EntityTransformers.fromWeaponShieldEntityFn))
                .collect(Collectors.toList());

        return new WeaponShieldListResponse(new Response(), weaponShields);
    }

    @Override
    @Transactional
    public Response saveWeaponShield(SaveWeaponShieldRequest request) throws TException {
        WeaponShield weaponShield = request.getWeaponShield();

        if(weaponShield.isSetId()) {
            WeaponShieldEntity entity = weaponShieldDao.find(weaponShield.getId());
            Fns.mutate(() -> weaponShield, () -> entity, EntityTransformers.toWeaponShieldEntityFn);
        } else {
            WeaponShieldEntity entity = Fns.mutate(() -> weaponShield, WeaponShieldEntity::new, EntityTransformers.toWeaponShieldEntityFn);
            weaponShieldDao.persist(entity);
        }

        return new Response();
    }

}
