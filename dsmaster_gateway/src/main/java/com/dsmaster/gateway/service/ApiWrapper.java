package com.dsmaster.gateway.service;

import com.dsmaster.api.DsmApi;
import com.dsmaster.api.DsmApiCfg;
import com.dsmaster.gateway.service.configuration.ConfigurationServer;
import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.DsmUtils;
import com.google.common.collect.Lists;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import javax.annotation.PostConstruct;
import javax.jws.WebService;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by smeshkov on 7/17/14.
 */

@Deprecated
public class ApiWrapper {

    private static final Logger log =
            LoggerFactory.getLogger(ApiWrapper.class);

    private static final String SERVICE_POSTFIX = "ServiceImpl";
    private static final String SERVER_POSTFIX = "Server";
    private static final String SERVICE_REPLACE_REGEXP = "\\$Iface|\\$AsyncIface";

    // Configuration
    @Value("${api.cfg}")
    protected String apiCfg;
    @Value("${api.cfg.name}")
    protected String apiCfgName;
    @Value("${api.mode}")
    private String mode;
    @Value("${api.host.address}")
    private String host;
    @Value("${api.host.port}")
    private Integer port;

    @PostConstruct
    public void postConstruct() {
        try {

            DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) AppCtxHolder
                    .get().getAutowireCapableBeanFactory();
            Set<String> beanNames = new LinkedHashSet<String>(beanFactory.getBeanDefinitionCount());
            beanNames.addAll(Arrays.asList(beanFactory.getBeanDefinitionNames()));
            beanNames.addAll(Arrays.asList(beanFactory.getSingletonNames()));

            boolean startCfg = false;
            String cfgServiceBeanName = apiCfgName + SERVICE_POSTFIX;

            for (String beanName : beanNames) {
                Class<?> type = beanFactory.getType(beanName);
                if (type != null && !type.isInterface()) {
                    Object bean = beanFactory.getBean(beanName);
                    if(bean instanceof DsmApiService) {
                        Class<?> classToWrap;
                        if(DsmUtils.isProxy(bean)) {
                            classToWrap = DsmUtils.getTargetObject(bean).getClass();
                        } else {
                            classToWrap = type;
                        }
                        Object wrapperBean = makeWrapperDynamically(bean, classToWrap);
                        if(wrapperBean != null) {
                            // Replace bean with wrapper
                            beanFactory.removeBeanDefinition(beanName);
                            beanFactory.registerSingleton(beanName, wrapperBean);
                            if(beanName.equals(cfgServiceBeanName)) startCfg = true;
                            log.info("[{}] wrapped", beanName);
                        }
                    }
                }
            }

            if(startCfg) {
                ApiMode apiMode = ApiMode.forVal(mode);
                DsmApiCfg dsmApiCfg = new DsmApiCfg(apiCfgName, apiMode != null ? apiMode : ApiMode.SIMPLE, host, port);
                DsmApi api = dsmApiCfg.initApi(beanFactory, SERVER_POSTFIX, this.getClass().getPackage().getName() + "." + apiCfgName);
                ((ConfigurationServer) api).init();
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private Object makeWrapperDynamically(Object beanToWrap, Class beanToWrapClass) {
        if(beanToWrap != null && beanToWrapClass != null) {
            try {
                String wrapperPostfix = "Wrapper";
                String wrappedObjectName = "wrapped";
                String wrappedObjectSetter = "setWrapped";

                ClassPool classPool = ClassPool.getDefault();

                // Get Interface of JaxB service to wrapper class
                Class wrapperIface = fetchServiceIface(beanToWrapClass);
                if(wrapperIface == null) {
                    log.error("Error during [{}] bean wrapping -> Can't get Interface of Service!", beanToWrapClass.getSimpleName());
                    return null;
                }
                CtClass serviceIface = classPool.get(wrapperIface.getName());
                CtClass apiIface = classPool.get(DsmApiService.class.getName());

                // ----------------------- Service wrapper
                CtClass ccWrapper = classPool.makeClass(beanToWrapClass.getName() + wrapperPostfix);
                ClassFile cf = ccWrapper.getClassFile();
                ConstPool cp = cf.getConstPool();
//                addWSAnnotation(annotation, cf, cp);
                ccWrapper.addInterface(serviceIface);
                ccWrapper.addInterface(apiIface);
//                CtField wrpObjField = createWrappedObjField(wrappedObjectName, ccWrapper, beanToWrapClass);
                CtField wrpObjField = createWrappedObjField(wrappedObjectName, ccWrapper, wrapperIface);
                ccWrapper.addField(wrpObjField);
//                CtMethod setter = createWrappedObjSetter(wrappedObjectName, wrappedObjectSetter, ccWrapper, beanToWrapClass);
                CtMethod setter = createWrappedObjSetter(wrappedObjectName, wrappedObjectSetter, ccWrapper, wrapperIface);
                ccWrapper.addMethod(setter);

                wrapMethods(ccWrapper, cp, Lists.newArrayList(wrapperIface, DsmApiService.class), wrappedObjectName, Collections.emptyList());

                Class dynamicBeanClass = ccWrapper.toClass();
                Object dynamicBean = dynamicBeanClass.newInstance();
//                Method setMethod = dynamicBeanClass.getDeclaredMethod(wrappedObjectSetter, new Class[]{beanToWrapClass});
                Method setMethod = dynamicBeanClass.getDeclaredMethod(wrappedObjectSetter, new Class[]{wrapperIface});
                setMethod.setAccessible(true);
                setMethod.invoke(dynamicBean, new Object[]{beanToWrap});

                return dynamicBean;

            } catch (Throwable t) {
                t.printStackTrace();
                log.error("Error during [{}] bean wrapping -> {}: {}", beanToWrapClass.getSimpleName(), t.getClass().getName(), t.getMessage());
            }
        } else {
            log.error("Nothing to wrap: beanToWrap={}, beanToWrapClass={}", beanToWrap, beanToWrapClass);
        }
        return null;
    }

    private CtMethod createWrappedObjSetter(String wrappedObjectName, String wrappedObjectSetter, CtClass wrapperClass,
                                            Class wrappedObjClass) throws CannotCompileException {
        String pName = "proxy";
        // Signature
        StringBuilder sbPrxM = new StringBuilder("private void ");
        sbPrxM.append(wrappedObjectSetter);
        sbPrxM.append("(");
        sbPrxM.append(wrappedObjClass.getName());
        sbPrxM.append(" ");
        sbPrxM.append(pName);
        sbPrxM.append(")");
        // Body
        sbPrxM.append("{ this.");
        sbPrxM.append(wrappedObjectName);
        sbPrxM.append(" = ");
        sbPrxM.append(pName);
        sbPrxM.append(";");
        sbPrxM.append(" }");
        return CtNewMethod.make(sbPrxM.toString(), wrapperClass);
    }

    private CtField createWrappedObjField(String wrappedObjectName, CtClass wrapperClass, Class wrappedObjClass) throws CannotCompileException {
        StringBuilder sbPrx = new StringBuilder("private ");
        sbPrx.append(wrappedObjClass.getName());
        sbPrx.append(" ");
        sbPrx.append(wrappedObjectName);
        sbPrx.append(";");
        return CtField.make(sbPrx.toString(), wrapperClass);
    }

    /**
     * Add @WebService annotation to class.
     * @param annotation - annotation with params.
     * @param cf - class file of changing class.
     * @param cp - class pool of changing class.
     */
    private void addWSAnnotation(WebService annotation, ClassFile cf, ConstPool cp) {
        AnnotationsAttribute attr = new AnnotationsAttribute(cp, AnnotationsAttribute.visibleTag);
        Annotation a = new Annotation(WebService.class.getName(), cp);
        a.addMemberValue("serviceName", new StringMemberValue(annotation.serviceName(), cp));
        a.addMemberValue("portName", new StringMemberValue(annotation.portName(), cp));
        attr.setAnnotation(a);
        cf.addAttribute(attr);
        cf.setVersionToJava5();
    }

    /**
     * Add annotation to method.
     * @param method - method to change.
     * @param annotation - annotation to add
     * @param cp - method constant pool.
     * @return annotated method.
     */
    private CtMethod addMethodAnnotation(CtMethod method, Class<?> annotation, ConstPool cp) {
        AnnotationsAttribute anAttr = new AnnotationsAttribute(cp, AnnotationsAttribute.visibleTag);
        Annotation annot = new Annotation(annotation.getName(), cp);
        anAttr.addAnnotation(annot);
        // add the annotation to the method descriptor
        method.getMethodInfo().addAttribute(anAttr);
        return method;
    }

    /**
     * Get WebService Interface from target class implementation.
     * @param targetClass - class that implements this interface.
     * @return web service interface.
     */
    private Class fetchServiceIface(Class targetClass) {
        for (Class aInterface : targetClass.getInterfaces()) {
            String iface = aInterface.getName();
            String s = iface.replaceAll(SERVICE_REPLACE_REGEXP, "");
            s = s.substring(iface.lastIndexOf(".") + 1, s.length());
            if(targetClass.getSimpleName().contains(s)) {
                return aInterface;
            }
        }
        return null;
    }

    /**
     * Wrap inner object methods according to Interface.
     * @param wrapperClass - wrapper class.
     * @param cp - wrapper class constant pool,
     * @param ifaceList - inner object interfaces.
     * @param wrappedObjectName - inner object variable name.
     * @param annotations - annotations to add to methods
     * @return wrapper class.
     * @throws CannotCompileException
     */
    private CtClass wrapMethods(CtClass wrapperClass, ConstPool cp, List<Class> ifaceList, String wrappedObjectName,
                                List<Class<?>> annotations) throws CannotCompileException {
        for (Class iface : ifaceList) {
            for (Method method : iface.getMethods()) {
                CtMethod ctMethod = createMethod(wrapperClass, wrappedObjectName, method);
                if(CollectionUtils.isNotEmpty(annotations)) {
                    for (Class<?> annotation : annotations) {
                        addMethodAnnotation(ctMethod, annotation, cp);
                    }
                }
                wrapperClass.addMethod( ctMethod );
            }
        }
        return wrapperClass;
    }

    /**
     * Create new method for wrapper class.
     * @param wrapperClass - wrapper class.
     * @param wrappedObjectName - inner object variable name.
     * @param method - method of inner object interface to create.
     * @return created method.
     * @throws CannotCompileException
     */
    private CtMethod createMethod(CtClass wrapperClass, String wrappedObjectName, Method method) throws CannotCompileException {
        String paramName = "request";

        // Signature
        StringBuilder sb = new StringBuilder("public ");
        sb.append(method.getReturnType().getName());
        sb.append(" ");
        sb.append(method.getName());

        // Parameter
        boolean withParam = false;
        sb.append("(");
        for (Class<?> aClass : method.getParameterTypes()) {
            if(aClass.getSimpleName().contains("Request")) {
                sb.append(aClass.getName());sb.append(" ");sb.append(paramName);
                withParam = true;
            }
        }
        sb.append(")");

        // Throws
        Class<?>[] exceptionTypes = method.getExceptionTypes();
        if(ArrayUtils.isNotEmpty(exceptionTypes)) {
            sb.append(" throws");
            String comma = "";
            for (Class<?> exceptionType : exceptionTypes) {
                sb.append(comma);sb.append(" ");sb.append(exceptionType.getName());
                comma = ",";
            }
        }

        // Body
        String parenthesis;
        if(method.getDeclaringClass().equals(DsmApiService.class)) {
            sb.append("{ return ((");sb.append(DsmApiService.class.getName());sb.append(") ");
            parenthesis = ")";
        } else {
            sb.append("{ return ");
            parenthesis = "";
        }
        sb.append(wrappedObjectName);sb.append(parenthesis);
        sb.append(".");
        sb.append(method.getName());
        sb.append("(");
        if(withParam) sb.append(paramName);
        sb.append("); }");

        return CtNewMethod.make(sb.toString(), wrapperClass);
    }

}
