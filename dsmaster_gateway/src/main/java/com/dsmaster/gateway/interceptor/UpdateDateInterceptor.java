package com.dsmaster.gateway.interceptor;

import com.dsmaster.gateway.entity.TimestampEntity;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by smeshkov on 03.09.14.
 */
public class UpdateDateInterceptor extends EmptyInterceptor {
    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        if(entity instanceof TimestampEntity) {
            int i = Arrays.asList(propertyNames).indexOf("timestampUpdated");
            if(i >= 0) {
                currentState[i] = new Date();
                return true;
            }
        }
        return false;
    }
}
