package com.dsmaster.gateway.util;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.gateway.service.security.UserKeyCacheWrapper;
import com.dsmaster.model.dto.security.AccessLevel;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by smeshkov on 8/4/14.
 */

@Aspect
@Component
public class ValidateAspect {

    private final Logger log =
            LoggerFactory.getLogger(getClass());

    public static final List<String> REQUEST_POSTFIXES = Lists.newArrayList("Request", "Rqst");
    public static final String LEVEL_POSTFIX = "Lvl";
    public static final String REQUEST_GETTER = "getRequest";
    public static final String RESPONSE_SETTER = "setResponse";

//    private static Validator validator = javax.validation.Validation.buildDefaultValidatorFactory().getValidator();

    @Around(value = "@annotation(annotation)")
    public Object aroundTrace(ProceedingJoinPoint pjp, final Validation annotation) throws Throwable {
        final MethodSignature signature = (MethodSignature) pjp.getSignature();
        Object retValue;

        Object ARG = null;
        try {
            ARG = pjp.getArgs()[ 0 ];

            // Access Validation
            Class<?> requestClass = ARG.getClass();
            Method requestGetter = requestClass.getDeclaredMethod(REQUEST_GETTER, new Class[0]);
            requestGetter.setAccessible(true);
            Request request = (Request) requestGetter.invoke(ARG, new Object[0]);
            if(!apiAccessCheck(request, requestClass)) {
                log.error("API Request[{}]: Restricted Access for UUID[{}] with IP[{}] ---> [{}]",
                        requestClass.getSimpleName(), request.getUuid(), request.getFromUserIP(), pjp.toShortString());
                Class responseClass = signature.getReturnType();
                Object response = responseClass.newInstance();
                Method responseSetter = responseClass.getDeclaredMethod(RESPONSE_SETTER, new Class[]{Response.class});
                responseSetter.setAccessible(true);
                responseSetter.invoke(response, new Object[]{new Response(StatusCode.NO_PRIVILEGES, "", "")});
                return response;
            }


            // TODO may be some day for request validation - START
//            Set<ConstraintViolation<Object>> constraintViolation = validator.validate( ARG );

//            if ( constraintViolation.size() > 0 ) {
//                ConstraintViolation next = constraintViolation.iterator().next();
//                log.error("Validation failed: {} --> [{}]", Arrays.toString(constraintViolation.toArray()), pjp.toShortString());
//
//                Class aClass = signature.getReturnType();
//                Response<?> response = (Response<?>) aClass.getConstructor().newInstance();
//
//                return response.setErrorMessage( next.getMessage() )
//                        .setStatusCode(StatusCodes.ILLEGAL_ARGUMENTS)
//                        .setStatusDetail(next.getPropertyPath().toString());
//            }
            // TODO may be some day for request validation - END


            retValue = pjp.proceed( pjp.getArgs() );
        } catch( Throwable tt ) {
            log.error("<-" + pjp.toShortString() + "\nRequest: " + ARG + "\n\nThrows:", tt);
            throw tt;
        }

        return retValue;
    }

    private boolean apiAccessCheck(Request request, Class requestClass) {
        if( request == null || requestClass == null ) return false;
        if(requestClass.equals(Request.class)) {
            return true;
        } else {
            AccessLevel wantedLevel = parseLevel(requestClass);
            if(wantedLevel == null || wantedLevel == AccessLevel.Z) {
                return true;
            } else {
                UserKeyCacheWrapper cacheWrapper = UserKeyCacheWrapper.get();
                if(cacheWrapper == null) return false;
                String serverUuid = cacheWrapper.readServerUuid();
                if(StringUtils.isNotEmpty(serverUuid) && serverUuid.equals(request.getUuid())) return true;
                AccessLevel allowedLevel = cacheWrapper.readAccess(request.getUuid());
                if(allowedLevel == null) {
                    return false;
                } else {
                    return allowedLevel.getValue() <= wantedLevel.getValue();
                }
            }
        }
    }

    private AccessLevel parseLevel(Class requestClass) {
        String className = requestClass.getSimpleName();
        for (String s : REQUEST_POSTFIXES) {
            if (className.contains(s)) {
                String lvlStr = className.substring(className.lastIndexOf(s) + s.length(), className.length());
                if (StringUtils.isNotEmpty(lvlStr)) {
                    String lvlVal = lvlStr.substring(lvlStr.lastIndexOf(LEVEL_POSTFIX) + LEVEL_POSTFIX.length(), lvlStr.length());
                    if (StringUtils.isNotEmpty(lvlVal)) {
                        return AccessLevel.valueOf(lvlVal);
                    }
                }
            }
        }
        return null;
    }

}
