package com.dsmaster.gateway.util;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.gateway.service.security.UserKeyCacheWrapper;
import com.dsmaster.model.util.DsmUtils;
import com.dsmaster.model.util.ExcHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by smeshkov on 03.09.14.
 */
public class RequestFactory {

    public static final String REQUEST_SETTER = "setRequest";

    public static <T> T system(Class<? extends T> aClass) {

        try {
            T request;
            Request innerRequest = new Request();
            if(aClass.isAssignableFrom(Request.class)) {
                request = (T) innerRequest;
            } else {
                request = aClass.newInstance();
                Method requestSetter = aClass.getDeclaredMethod(REQUEST_SETTER, new Class[]{Request.class});
                requestSetter.setAccessible(true);
                requestSetter.invoke(request, new Object[]{innerRequest});
            }
            innerRequest.setUuid(UserKeyCacheWrapper.get().readServerUuid());
            innerRequest.setFromUserId(DsmUtils.SYSTEM_USER_ID);
            innerRequest.setFromUserIP(DsmUtils.SYSTEM_USER_IP);
            return request;
        } catch( InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e ) {
            ExcHandler.ex(e);
            throw new RuntimeException(e);
        }
    }

}
