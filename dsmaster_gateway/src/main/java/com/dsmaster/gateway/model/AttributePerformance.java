package com.dsmaster.gateway.model;

/**
 * User: artem_marinov
 * Date: 22.08.14
 * Time: 12:55
 */
public class AttributePerformance {

    private StatAttribute attribute;

    private Short attributeValue;

    private StatAttribute performance;

    private Short performanceValue;

    public StatAttribute getAttribute() {
        return attribute;
    }

    public AttributePerformance setAttribute(StatAttribute attribute) {
        this.attribute = attribute;
        return this;
    }

    public Short getAttributeValue() {
        return attributeValue;
    }

    public AttributePerformance setAttributeValue(Short attributeValue) {
        this.attributeValue = attributeValue;
        return this;
    }

    public StatAttribute getPerformance() {
        return performance;
    }

    public AttributePerformance setPerformance(StatAttribute performance) {
        this.performance = performance;
        return this;
    }

    public Short getPerformanceValue() {
        return performanceValue;
    }

    public AttributePerformance setPerformanceValue(Short performanceValue) {
        this.performanceValue = performanceValue;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttributePerformance that = (AttributePerformance) o;

        if (attribute != that.attribute) return false;
        if (attributeValue != null ? !attributeValue.equals(that.attributeValue) : that.attributeValue != null)
            return false;
        if (performance != that.performance) return false;
        if (performanceValue != null ? !performanceValue.equals(that.performanceValue) : that.performanceValue != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = attribute != null ? attribute.hashCode() : 0;
        result = 31 * result + (attributeValue != null ? attributeValue.hashCode() : 0);
        result = 31 * result + (performance != null ? performance.hashCode() : 0);
        result = 31 * result + (performanceValue != null ? performanceValue.hashCode() : 0);
        return result;
    }

}
