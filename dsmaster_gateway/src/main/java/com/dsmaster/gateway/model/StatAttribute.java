package com.dsmaster.gateway.model;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 17:36
 */
public enum StatAttribute {

    STR( (short)1),
    DEX( (short)2),
    INT( (short)3),
    FTH( (short)4),

    STR_ATK_BONUS( (short)20),
    DEX_ATK_BONUS( (short)21),
    MAGIC_ATK_BONUS( (short)22),
    LIGHT_ATK_BONUS( (short)23),
    FIRE_ATK_BONUS( (short)24),
    DARK_ATK_BONUS( (short)25),
    POISON_ATK_BONUS( (short)26),
    BLEED_ATK_BONUS( (short)27),

    ;

    private short id;

    StatAttribute( short id) {
        this.id = id;
    }


    public static StatAttribute findByValue(short value) {
        for(StatAttribute item : StatAttribute.values()) {
            if(item.getId() == value) {
                return item;
            }
        }
        return null;
    }

    public short getId() {
        return id;
    }

}
