package com.dsmaster.gateway.entity.calculator;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 13:21
 */
@Entity
@Table(name = "attributes_performance", schema = "ds_core")
public class AttributePerformanceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "attribute_id")
    private Short attributeId;

    @Column(name = "attribute_value")
    private Short attributeValue;

    @Column(name = "performance_id")
    private Short performanceId;

    @Column(name = "performance_value")
    private Short performanceValue;


    public Integer getId() {
        return id;
    }

    public AttributePerformanceEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Short getAttributeId() {
        return attributeId;
    }

    public AttributePerformanceEntity setAttributeId(Short attributeId) {
        this.attributeId = attributeId;
        return this;
    }

    public Short getAttributeValue() {
        return attributeValue;
    }

    public AttributePerformanceEntity setAttributeValue(Short attributeValue) {
        this.attributeValue = attributeValue;
        return this;
    }

    public Short getPerformanceId() {
        return performanceId;
    }

    public AttributePerformanceEntity setPerformanceId(Short performanceId) {
        this.performanceId = performanceId;
        return this;
    }

    public Short getPerformanceValue() {
        return performanceValue;
    }

    public AttributePerformanceEntity setPerformanceValue(Short performanceValue) {
        this.performanceValue = performanceValue;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttributePerformanceEntity that = (AttributePerformanceEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("attributeId", attributeId)
                .add("attributeValue", attributeValue)
                .add("performanceId", performanceId)
                .add("performanceValue", performanceValue)
                .omitNullValues()
                .toString();
    }

}
