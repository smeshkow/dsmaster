package com.dsmaster.gateway.entity.article;

import com.dsmaster.gateway.entity.BaseEntity;
import com.google.common.base.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by smeshkov on 7/31/14.
 */

@Entity
@Table(name = "articles", schema = "ds_core")
public class ArticleEntity extends BaseEntity<ArticleEntity> {

    @Column(name = "acquired_from", columnDefinition = "TEXT")
    private String acquiredFrom;

    @Column(name = "hints_and_tips", columnDefinition = "TEXT")
    private String hintsAndTips;

    @Column(name = "type_id")
    private Byte typeId;

    @Column(name = "link")
    private String link;

    public String getAcquiredFrom() {
        return acquiredFrom;
    }

    public ArticleEntity setAcquiredFrom(String acquiredFrom) {
        this.acquiredFrom = acquiredFrom;
        return this;
    }

    public String getHintsAndTips() {
        return hintsAndTips;
    }

    public ArticleEntity setHintsAndTips(String hintsAndTips) {
        this.hintsAndTips = hintsAndTips;
        return this;
    }

    public Byte getTypeId() {
        return typeId;
    }

    public ArticleEntity setTypeId(Byte typeId) {
        this.typeId = typeId;
        return this;
    }

    public String getLink() {
        return link;
    }

    public ArticleEntity setLink(String link) {
        this.link = link;
        return this;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("acquiredFrom", acquiredFrom)
                .add("hintsAndTips", hintsAndTips)
                .add("typeId", typeId)
                .add("link", link)
                .omitNullValues()
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ArticleEntity that = (ArticleEntity) o;

        if (acquiredFrom != null ? !acquiredFrom.equals(that.acquiredFrom) : that.acquiredFrom != null) return false;
        if (hintsAndTips != null ? !hintsAndTips.equals(that.hintsAndTips) : that.hintsAndTips != null) return false;
        if (link != null ? !link.equals(that.link) : that.link != null) return false;
        if (typeId != null ? !typeId.equals(that.typeId) : that.typeId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (acquiredFrom != null ? acquiredFrom.hashCode() : 0);
        result = 31 * result + (hintsAndTips != null ? hintsAndTips.hashCode() : 0);
        result = 31 * result + (typeId != null ? typeId.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }

}
