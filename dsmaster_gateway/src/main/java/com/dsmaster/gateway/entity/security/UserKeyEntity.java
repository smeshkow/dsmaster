package com.dsmaster.gateway.entity.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by smeshkov on 02.09.14.
 */

@Entity
@Table(name = "user_keys", schema = "ds_core")
public class UserKeyEntity {

    @Id
    @Column(name = "uuid")
    private String uuid;

    @Column(name = "access_level_id")
    private Integer accessLevelId;

    @Column(name = "key_store" )
    private String keyStore;

    public String getUuid() {
        return uuid;
    }

    public UserKeyEntity setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public Integer getAccessLevelId() {
        return accessLevelId;
    }

    public UserKeyEntity setAccessLevelId(Integer accessLevelId) {
        this.accessLevelId = accessLevelId;
        return this;
    }

    public String getKeyStore() {
        return keyStore;
    }

    public UserKeyEntity setKeyStore(String keyStore) {
        this.keyStore = keyStore;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserKeyEntity that = (UserKeyEntity) o;

        if (accessLevelId != null ? !accessLevelId.equals(that.accessLevelId) : that.accessLevelId != null) return false;
        if (keyStore != null ? !keyStore.equals(that.keyStore) : that.keyStore != null) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (accessLevelId != null ? accessLevelId.hashCode() : 0);
        result = 31 * result + (keyStore != null ? keyStore.hashCode() : 0);
        return result;
    }
}
