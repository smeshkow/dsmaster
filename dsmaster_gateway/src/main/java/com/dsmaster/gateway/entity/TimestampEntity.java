package com.dsmaster.gateway.entity;

/**
 * Created by smeshkov on 03.09.14.
 */
public interface TimestampEntity<E> {

    Long getTimestampCreated();

    E setTimestampCreated(Long timestampCreated);

    Long getTimestampUpdated();

    E setTimestampUpdated(Long timestampUpdated);

}
