package com.dsmaster.gateway.entity.attributes;

import com.dsmaster.model.dto.common.HasWeight;
import com.google.common.base.Objects;

import javax.persistence.MappedSuperclass;

/**
 * Created by smeshkov on 6/25/14.
 */

//@Entity
//@Table(name = "", schema = "")
@MappedSuperclass
public class BaseAttributesEntity implements HasWeight<BaseAttributesEntity> {

    private Short durability;
    private Double weight;

    //"LeftHandATK":???,
    //"RightHandATK":???,


    public Short getDurability() {
        return durability;
    }

    public BaseAttributesEntity setDurability(Short durability) {
        this.durability = durability;
        return this;
    }

    @Override
    public Double getWeight() {
        return weight;
    }

    public BaseAttributesEntity setWeight(Double weight) {
        this.weight = weight;
        return this;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("durability", durability)
                .add("weight", weight)
                .omitNullValues()
                .toString();
    }

}
