package com.dsmaster.gateway.entity.attributes;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 30.06.14
 * Time: 11:33
 */
@Entity
@Table(name = "armor_attributes", schema = "ds_core")
public class ArmorAttributesEntity extends BaseAttributesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    private Short physicalDef;
    private Short magicDef;
    private Short fireDef;
    private Short lightningDef;
    private Short darkDef;
    private Short poisonResist;
    private Short bleedResist;
//    private Short breakResist;
    private Short petrifyResist;
    private Short curseResist;
    private Short physicalDefStrike;
    private Short physicalDefSlash;
    private Short physicalDefThrust;
    private Short poise;

    public Integer getId() {
        return id;
    }

    public ArmorAttributesEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Short getPhysicalDef() {
        return physicalDef;
    }

    public ArmorAttributesEntity setPhysicalDef(Short physicalDef) {
        this.physicalDef = physicalDef;
        return this;
    }

    public Short getMagicDef() {
        return magicDef;
    }

    public ArmorAttributesEntity setMagicDef(Short magicDef) {
        this.magicDef = magicDef;
        return this;
    }

    public Short getFireDef() {
        return fireDef;
    }

    public ArmorAttributesEntity setFireDef(Short fireDef) {
        this.fireDef = fireDef;
        return this;
    }

    public Short getLightningDef() {
        return lightningDef;
    }

    public ArmorAttributesEntity setLightningDef(Short lightningDef) {
        this.lightningDef = lightningDef;
        return this;
    }

    public Short getDarkDef() {
        return darkDef;
    }

    public ArmorAttributesEntity setDarkDef(Short darkDef) {
        this.darkDef = darkDef;
        return this;
    }

    public Short getPoisonResist() {
        return poisonResist;
    }

    public ArmorAttributesEntity setPoisonResist(Short poisonResist) {
        this.poisonResist = poisonResist;
        return this;
    }

    public Short getBleedResist() {
        return bleedResist;
    }

    public ArmorAttributesEntity setBleedResist(Short bleedResist) {
        this.bleedResist = bleedResist;
        return this;
    }

    public Short getPetrifyResist() {
        return petrifyResist;
    }

    public ArmorAttributesEntity setPetrifyResist(Short petrifyResist) {
        this.petrifyResist = petrifyResist;
        return this;
    }

    public Short getCurseResist() {
        return curseResist;
    }

    public ArmorAttributesEntity setCurseResist(Short curseResist) {
        this.curseResist = curseResist;
        return this;
    }

    public Short getPhysicalDefStrike() {
        return physicalDefStrike;
    }

    public ArmorAttributesEntity setPhysicalDefStrike(Short physicalDefStrike) {
        this.physicalDefStrike = physicalDefStrike;
        return this;
    }

    public Short getPhysicalDefSlash() {
        return physicalDefSlash;
    }

    public ArmorAttributesEntity setPhysicalDefSlash(Short physicalDefSlash) {
        this.physicalDefSlash = physicalDefSlash;
        return this;
    }

    public Short getPhysicalDefThrust() {
        return physicalDefThrust;
    }

    public ArmorAttributesEntity setPhysicalDefThrust(Short physicalDefThrust) {
        this.physicalDefThrust = physicalDefThrust;
        return this;
    }

    public Short getPoise() {
        return poise;
    }

    public ArmorAttributesEntity setPoise(Short poise) {
        this.poise = poise;
        return this;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("physicalDef", physicalDef)
                .add("magicDef", magicDef)
                .add("fireDef", fireDef)
                .add("lightningDef", lightningDef)
                .add("darkDef", darkDef)
                .add("poisonResist", poisonResist)
                .add("bleedResist", bleedResist)
                .add("petrifyResist", petrifyResist)
                .add("curseResist", curseResist)
                .add("physicalDefStrike", physicalDefStrike)
                .add("physicalDefSlash", physicalDefSlash)
                .add("physicalDefThrust", physicalDefThrust)
                .add("poise", poise)
                .omitNullValues()
                .toString();
    }
}
