package com.dsmaster.gateway.entity.attributes;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 30.06.14
 * Time: 11:34
 */
@Entity
@Table(name = "attributes_requirement", schema = "ds_core")
public class AttributesRequirementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "required_str")
    private Short requiredStr;

    @Column(name = "required_dex")
    private Short requiredDex;

    @Column(name = "required_int")
    private Short requiredInt;

    @Column(name = "required_faith")
    private Short requiredFaith;

    public Integer getId() {
        return id;
    }

    public AttributesRequirementEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Short getRequiredStr() {
        return requiredStr;
    }

    public AttributesRequirementEntity setRequiredStr(Short requiredStr) {
        this.requiredStr = requiredStr;
        return this;
    }

    public Short getRequiredDex() {
        return requiredDex;
    }

    public AttributesRequirementEntity setRequiredDex(Short requiredDex) {
        this.requiredDex = requiredDex;
        return this;
    }

    public Short getRequiredInt() {
        return requiredInt;
    }

    public AttributesRequirementEntity setRequiredInt(Short requiredInt) {
        this.requiredInt = requiredInt;
        return this;
    }

    public Short getRequiredFaith() {
        return requiredFaith;
    }

    public AttributesRequirementEntity setRequiredFaith(Short requiredFaith) {
        this.requiredFaith = requiredFaith;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttributesRequirementEntity that = (AttributesRequirementEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("requiredStr", requiredStr)
                .add("requiredDex", requiredDex)
                .add("requiredInt", requiredInt)
                .add("requiredFaith", requiredFaith)
                .omitNullValues()
                .toString();
    }

}
