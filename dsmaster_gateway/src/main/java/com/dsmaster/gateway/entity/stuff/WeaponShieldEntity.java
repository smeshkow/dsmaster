package com.dsmaster.gateway.entity.stuff;

import com.dsmaster.gateway.entity.BaseEntity;
import com.dsmaster.gateway.entity.article.ArticleEntity;
import com.dsmaster.gateway.entity.attributes.AttributesBonusEntity;
import com.dsmaster.gateway.entity.attributes.AttributesRequirementEntity;
import com.dsmaster.gateway.entity.attributes.WeaponShieldAttributesEntity;
import com.google.common.base.Objects;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:42
 */
@Entity
@Table(name = "weapons_shields", schema = "ds_core")
public class WeaponShieldEntity extends BaseEntity<WeaponShieldEntity> {

    @Column(name = "weapon_category_id")
    private Byte weaponCategoryId; //staff, chime

    @Column(name = "attack_type_id_1")
    private Byte attackTypeId1; //Parry/Strike |  Slash

    @Column(name = "attack_type_id_2")
    private Byte attackTypeId2; //Parry/Strike |  Slash



    //SCALING  S-E

    @Column(name = "str_scaling_id")
    private Byte strScalingId;

    @Column(name = "dex_scaling_id")
    private Byte dexScalingId;

    @Column(name = "magic_scaling_id")
    private Byte magicScalingId;

    @Column(name = "fire_scaling_id")
    private Byte fireScalingId;

    @Column(name = "lightning_scaling_id")
    private Byte lightningScalingId;

    @Column(name = "dark_scaling_id")
    private Byte darkScalingId;



    @Column(name = "str_scaling_percent")
    private Double strScalingPercent;

    @Column(name = "dex_scaling_percent")
    private Double dexScalingPercent;

    @Column(name = "magic_scaling_percent")
    private Double magicScalingPercent;

    @Column(name = "fire_scaling_percent")
    private Double fireScalingPercent;

    @Column(name = "lightning_scaling_percent")
    private Double lightningScalingPercent;

    @Column(name = "dark_scaling_percent")
    private Double darkScalingPercent;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "weapon_shield_attributes_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private WeaponShieldAttributesEntity weaponShieldAttributesEntity;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "attributes_requirement_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private AttributesRequirementEntity attributesRequirementEntity;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "attributes_bonus_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private AttributesBonusEntity attributesBonusEntity;

    // Article
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id", referencedColumnName = "id", insertable = false, updatable = false)
    private ArticleEntity article;

    public Byte getWeaponCategoryId() {
        return weaponCategoryId;
    }

    public WeaponShieldEntity setWeaponCategoryId(Byte weaponCategoryId) {
        this.weaponCategoryId = weaponCategoryId;
        return this;
    }

    public Byte getAttackTypeId1() {
        return attackTypeId1;
    }

    public WeaponShieldEntity setAttackTypeId1(Byte attackTypeId1) {
        this.attackTypeId1 = attackTypeId1;
        return this;
    }

    public Byte getAttackTypeId2() {
        return attackTypeId2;
    }

    public WeaponShieldEntity setAttackTypeId2(Byte attackTypeId2) {
        this.attackTypeId2 = attackTypeId2;
        return this;
    }

    public Byte getStrScalingId() {
        return strScalingId;
    }

    public WeaponShieldEntity setStrScalingId(Byte strScalingId) {
        this.strScalingId = strScalingId;
        return this;
    }

    public Byte getDexScalingId() {
        return dexScalingId;
    }

    public WeaponShieldEntity setDexScalingId(Byte dexScalingId) {
        this.dexScalingId = dexScalingId;
        return this;
    }

    public Byte getMagicScalingId() {
        return magicScalingId;
    }

    public WeaponShieldEntity setMagicScalingId(Byte magicScalingId) {
        this.magicScalingId = magicScalingId;
        return this;
    }

    public Byte getFireScalingId() {
        return fireScalingId;
    }

    public WeaponShieldEntity setFireScalingId(Byte fireScalingId) {
        this.fireScalingId = fireScalingId;
        return this;
    }

    public Byte getLightningScalingId() {
        return lightningScalingId;
    }

    public WeaponShieldEntity setLightningScalingId(Byte lightningScalingId) {
        this.lightningScalingId = lightningScalingId;
        return this;
    }

    public Byte getDarkScalingId() {
        return darkScalingId;
    }

    public WeaponShieldEntity setDarkScalingId(Byte darkScalingId) {
        this.darkScalingId = darkScalingId;
        return this;
    }

    public Double getStrScalingPercent() {
        return strScalingPercent;
    }

    public WeaponShieldEntity setStrScalingPercent(Double strScalingPercent) {
        this.strScalingPercent = strScalingPercent;
        return this;
    }

    public Double getDexScalingPercent() {
        return dexScalingPercent;
    }

    public WeaponShieldEntity setDexScalingPercent(Double dexScalingPercent) {
        this.dexScalingPercent = dexScalingPercent;
        return this;
    }

    public Double getMagicScalingPercent() {
        return magicScalingPercent;
    }

    public WeaponShieldEntity setMagicScalingPercent(Double magicScalingPercent) {
        this.magicScalingPercent = magicScalingPercent;
        return this;
    }

    public Double getFireScalingPercent() {
        return fireScalingPercent;
    }

    public WeaponShieldEntity setFireScalingPercent(Double fireScalingPercent) {
        this.fireScalingPercent = fireScalingPercent;
        return this;
    }

    public Double getLightningScalingPercent() {
        return lightningScalingPercent;
    }

    public WeaponShieldEntity setLightningScalingPercent(Double lightningScalingPercent) {
        this.lightningScalingPercent = lightningScalingPercent;
        return this;
    }

    public Double getDarkScalingPercent() {
        return darkScalingPercent;
    }

    public WeaponShieldEntity setDarkScalingPercent(Double darkScalingPercent) {
        this.darkScalingPercent = darkScalingPercent;
        return this;
    }

    public WeaponShieldAttributesEntity getWeaponShieldAttributesEntity() {
        return weaponShieldAttributesEntity;
    }

    public WeaponShieldEntity setWeaponShieldAttributesEntity(WeaponShieldAttributesEntity weaponShieldAttributesEntity) {
        this.weaponShieldAttributesEntity = weaponShieldAttributesEntity;
        return this;
    }

    public AttributesRequirementEntity getAttributesRequirementEntity() {
        return attributesRequirementEntity;
    }

    public WeaponShieldEntity setAttributesRequirementEntity(AttributesRequirementEntity attributesRequirementEntity) {
        this.attributesRequirementEntity = attributesRequirementEntity;
        return this;
    }

    public AttributesBonusEntity getAttributesBonusEntity() {
        return attributesBonusEntity;
    }

    public WeaponShieldEntity setAttributesBonusEntity(AttributesBonusEntity attributesBonusEntity) {
        this.attributesBonusEntity = attributesBonusEntity;
        return this;
    }

    public ArticleEntity getArticle() {
        return article;
    }

    public WeaponShieldEntity setArticle(ArticleEntity article) {
        this.article = article;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WeaponShieldEntity that = (WeaponShieldEntity) o;

        if (attackTypeId1 != null ? !attackTypeId1.equals(that.attackTypeId1) : that.attackTypeId1 != null)
            return false;
        if (attackTypeId2 != null ? !attackTypeId2.equals(that.attackTypeId2) : that.attackTypeId2 != null)
            return false;
        if (darkScalingId != null ? !darkScalingId.equals(that.darkScalingId) : that.darkScalingId != null)
            return false;
        if (darkScalingPercent != null ? !darkScalingPercent.equals(that.darkScalingPercent) : that.darkScalingPercent != null)
            return false;
        if (dexScalingId != null ? !dexScalingId.equals(that.dexScalingId) : that.dexScalingId != null) return false;
        if (dexScalingPercent != null ? !dexScalingPercent.equals(that.dexScalingPercent) : that.dexScalingPercent != null)
            return false;
        if (fireScalingId != null ? !fireScalingId.equals(that.fireScalingId) : that.fireScalingId != null)
            return false;
        if (fireScalingPercent != null ? !fireScalingPercent.equals(that.fireScalingPercent) : that.fireScalingPercent != null)
            return false;
        if (lightningScalingId != null ? !lightningScalingId.equals(that.lightningScalingId) : that.lightningScalingId != null)
            return false;
        if (lightningScalingPercent != null ? !lightningScalingPercent.equals(that.lightningScalingPercent) : that.lightningScalingPercent != null)
            return false;
        if (magicScalingId != null ? !magicScalingId.equals(that.magicScalingId) : that.magicScalingId != null)
            return false;
        if (magicScalingPercent != null ? !magicScalingPercent.equals(that.magicScalingPercent) : that.magicScalingPercent != null)
            return false;
        if (strScalingId != null ? !strScalingId.equals(that.strScalingId) : that.strScalingId != null) return false;
        if (strScalingPercent != null ? !strScalingPercent.equals(that.strScalingPercent) : that.strScalingPercent != null)
            return false;
        if (weaponCategoryId != null ? !weaponCategoryId.equals(that.weaponCategoryId) : that.weaponCategoryId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (weaponCategoryId != null ? weaponCategoryId.hashCode() : 0);
        result = 31 * result + (attackTypeId1 != null ? attackTypeId1.hashCode() : 0);
        result = 31 * result + (attackTypeId2 != null ? attackTypeId2.hashCode() : 0);
        result = 31 * result + (strScalingId != null ? strScalingId.hashCode() : 0);
        result = 31 * result + (dexScalingId != null ? dexScalingId.hashCode() : 0);
        result = 31 * result + (magicScalingId != null ? magicScalingId.hashCode() : 0);
        result = 31 * result + (fireScalingId != null ? fireScalingId.hashCode() : 0);
        result = 31 * result + (lightningScalingId != null ? lightningScalingId.hashCode() : 0);
        result = 31 * result + (darkScalingId != null ? darkScalingId.hashCode() : 0);
        result = 31 * result + (strScalingPercent != null ? strScalingPercent.hashCode() : 0);
        result = 31 * result + (dexScalingPercent != null ? dexScalingPercent.hashCode() : 0);
        result = 31 * result + (magicScalingPercent != null ? magicScalingPercent.hashCode() : 0);
        result = 31 * result + (fireScalingPercent != null ? fireScalingPercent.hashCode() : 0);
        result = 31 * result + (lightningScalingPercent != null ? lightningScalingPercent.hashCode() : 0);
        result = 31 * result + (darkScalingPercent != null ? darkScalingPercent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("weaponCategoryId", weaponCategoryId)
                .add("attackTypeId1", attackTypeId1)
                .add("attackTypeId2", attackTypeId2)
                .add("strScalingId", strScalingId)
                .add("dexScalingId", dexScalingId)
                .add("magicScalingId", magicScalingId)
                .add("fireScalingId", fireScalingId)
                .add("lightningScalingId", lightningScalingId)
                .add("darkScalingId", darkScalingId)
                .add("strScalingPercent", strScalingPercent)
                .add("dexScalingPercent", dexScalingPercent)
                .add("magicScalingPercent", magicScalingPercent)
                .add("fireScalingPercent", fireScalingPercent)
                .add("lightningScalingPercent", lightningScalingPercent)
                .add("darkScalingPercent", darkScalingPercent)
                .omitNullValues()
                .toString();
    }
}
