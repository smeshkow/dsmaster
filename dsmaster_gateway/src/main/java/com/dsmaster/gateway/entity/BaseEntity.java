package com.dsmaster.gateway.entity;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:51
 */
@MappedSuperclass
public class BaseEntity<T extends BaseEntity> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "addon_id")
    private Byte addonId;

    @Column(name = "name")
    private String name;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;


    public Integer getId() {
        return id;
    }

    public T setId(Integer id) {
        this.id = id;
        return (T) this;
    }

    public Byte getAddonId() {
        return addonId;
    }

    public T setAddonId(Byte addonId) {
        this.addonId = addonId;
        return (T) this;
    }

    public String getName() {
        return name;
    }

    public T setName(String name) {
        this.name = name;
        return (T) this;
    }

    public String getDescription() {
        return description;
    }

    public T setDescription(String description) {
        this.description = description;
        return (T) this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        if (addonId != null ? !addonId.equals(that.addonId) : that.addonId != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = addonId != null ? addonId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("addonId", addonId)
                .add("name", name)
                .add("description", description)
                .omitNullValues()
                .toString();
    }
}
