package com.dsmaster.gateway.entity.configuration;

import com.google.common.base.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by smeshkov on 7/16/14.
 */

@Entity
@Table(name = "configurations", schema = "ds_core")
public class ConfigurationEntity {

    @Id
    @Column(name = "key")
    private String key;

    @Column(name = "str_val")
    private String strVal;

    @Column(name = "int_val" )
    private Integer intVal;

    @Column(name = "double_val" )
    private Double doubleVal;

    @Column(name = "description")
    private String description;

    public String getKey() {
        return key;
    }

    public ConfigurationEntity setKey(String key) {
        this.key = key;
        return this;
    }

    public String getStrVal() {
        return strVal;
    }

    public ConfigurationEntity setStrVal(String strVal) {
        this.strVal = strVal;
        return this;
    }

    public Integer getIntVal() {
        return intVal;
    }

    public ConfigurationEntity setIntVal(Integer intVal) {
        this.intVal = intVal;
        return this;
    }

    public Double getDoubleVal() {
        return doubleVal;
    }

    public ConfigurationEntity setDoubleVal(Double doubleVal) {
        this.doubleVal = doubleVal;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ConfigurationEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigurationEntity that = (ConfigurationEntity) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (doubleVal != null ? !doubleVal.equals(that.doubleVal) : that.doubleVal != null) return false;
        if (intVal != null ? !intVal.equals(that.intVal) : that.intVal != null) return false;
        if (key != null ? !key.equals(that.key) : that.key != null) return false;
        if (strVal != null ? !strVal.equals(that.strVal) : that.strVal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (strVal != null ? strVal.hashCode() : 0);
        result = 31 * result + (intVal != null ? intVal.hashCode() : 0);
        result = 31 * result + (doubleVal != null ? doubleVal.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("key", key)
                .add("strVal", strVal)
                .add("intVal", intVal)
                .add("doubleVal", doubleVal)
                .add("description", description)
                .omitNullValues()
                .toString();
    }
}
