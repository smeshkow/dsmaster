package com.dsmaster.gateway.entity.profile;

import com.dsmaster.gateway.entity.TimestampEntity;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 27.06.14
 * Time: 16:26
 */
@Entity
@Table(name = "profiles", schema = "ds_core")
public class ProfileEntity implements TimestampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "email")
    private String email;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "status_id")
    private Byte statusId;

    @Column(name = "timestamp_created")
    private Long timestampCreated;

    @Column(name = "timestamp_updated")
    private Long timestampUpdated;

    public Integer getId() {
        return id;
    }

    public ProfileEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public ProfileEntity setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ProfileEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public ProfileEntity setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public Byte getStatusId() {
        return statusId;
    }

    public ProfileEntity setStatusId(Byte statusId) {
        this.statusId = statusId;
        return this;
    }

    @Override
    public Long getTimestampCreated() {
        return timestampCreated;
    }

    @Override
    public ProfileEntity setTimestampCreated(Long timestampCreated) {
        this.timestampCreated = timestampCreated;
        return this;
    }

    @Override
    public Long getTimestampUpdated() {
        return timestampUpdated;
    }

    @Override
    public ProfileEntity setTimestampUpdated(Long timestampUpdated) {
        this.timestampUpdated = timestampUpdated;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileEntity that = (ProfileEntity) o;

        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (passwordHash != null ? !passwordHash.equals(that.passwordHash) : that.passwordHash != null) return false;
        if (statusId != null ? !statusId.equals(that.statusId) : that.statusId != null)
            return false;
        if (timestampCreated != null ? !timestampCreated.equals(that.timestampCreated) : that.timestampCreated != null)
            return false;
        if (timestampUpdated != null ? !timestampUpdated.equals(that.timestampUpdated) : that.timestampUpdated != null)
            return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (passwordHash != null ? passwordHash.hashCode() : 0);
        result = 31 * result + (statusId != null ? statusId.hashCode() : 0);
        result = 31 * result + (timestampCreated != null ? timestampCreated.hashCode() : 0);
        result = 31 * result + (timestampUpdated != null ? timestampUpdated.hashCode() : 0);
        return result;
    }
}
