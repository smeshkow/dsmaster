package com.dsmaster.gateway.entity.stuff;

import com.dsmaster.gateway.entity.BaseEntity;
import com.dsmaster.gateway.entity.attributes.ArmorAttributesEntity;
import com.dsmaster.gateway.entity.attributes.AttributesBonusEntity;
import com.dsmaster.gateway.entity.attributes.AttributesRequirementEntity;
import com.dsmaster.gateway.entity.attributes.WeaponShieldAttributesEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 10:37
 */
@Entity
@Table(name = "armors", schema = "ds_core")
public class ArmorEntity extends BaseEntity<WeaponShieldEntity> {

    @Column(name = "armor_type_id")
    private Byte armorTypeId; //head, torso, legs, hands

    @Column(name = "scaling_id")
    private Byte scalingId;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "armor_attributes_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private ArmorAttributesEntity armorAttributesEntity;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "attributes_requirement_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private AttributesRequirementEntity attributesRequirementEntity;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "attributes_bonus_entity_id", referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private AttributesBonusEntity attributesBonusEntity;

    public Byte getArmorTypeId() {
        return armorTypeId;
    }

    public ArmorEntity setArmorTypeId(Byte armorTypeId) {
        this.armorTypeId = armorTypeId;
        return this;
    }

    public Byte getScalingId() {
        return scalingId;
    }

    public ArmorEntity setScalingId(Byte scalingId) {
        this.scalingId = scalingId;
        return this;
    }

    public ArmorAttributesEntity getArmorAttributesEntity() {
        return armorAttributesEntity;
    }

    public ArmorEntity setArmorAttributesEntity(ArmorAttributesEntity armorAttributesEntity) {
        this.armorAttributesEntity = armorAttributesEntity;
        return this;
    }

    public AttributesRequirementEntity getAttributesRequirementEntity() {
        return attributesRequirementEntity;
    }

    public ArmorEntity setAttributesRequirementEntity(AttributesRequirementEntity attributesRequirementEntity) {
        this.attributesRequirementEntity = attributesRequirementEntity;
        return this;
    }

    public AttributesBonusEntity getAttributesBonusEntity() {
        return attributesBonusEntity;
    }

    public ArmorEntity setAttributesBonusEntity(AttributesBonusEntity attributesBonusEntity) {
        this.attributesBonusEntity = attributesBonusEntity;
        return this;
    }

}
