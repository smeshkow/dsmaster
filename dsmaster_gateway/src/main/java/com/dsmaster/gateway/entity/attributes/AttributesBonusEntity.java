package com.dsmaster.gateway.entity.attributes;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 30.06.14
 * Time: 11:35
 */
@Entity
@Table(name = "attributes_bonus", schema = "ds_core")
public class AttributesBonusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "bonus_str")
    private Short bonusStr;

    @Column(name = "bonus_dex")
    private Short bonusDex;

    @Column(name = "bonus_int")
    private Short bonusInt;

    @Column(name = "bonus_faith")
    private Short bonusFaith;

//    @Column(name = "bonus_physical_def")
//    private Short bonusPhysicalDef; //?
//
//    @Column(name = "bonus_resistance")
//    private Short bonusResistance; //?
//    private Short attributeBonusPhysicalDef; //? У него имеется ввиду скейл брони. see ArmorEntity


    public Integer getId() {
        return id;
    }

    public AttributesBonusEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Short getBonusStr() {
        return bonusStr;
    }

    public AttributesBonusEntity setBonusStr(Short bonusStr) {
        this.bonusStr = bonusStr;
        return this;
    }

    public Short getBonusDex() {
        return bonusDex;
    }

    public AttributesBonusEntity setBonusDex(Short bonusDex) {
        this.bonusDex = bonusDex;
        return this;
    }

    public Short getBonusInt() {
        return bonusInt;
    }

    public AttributesBonusEntity setBonusInt(Short bonusInt) {
        this.bonusInt = bonusInt;
        return this;
    }

    public Short getBonusFaith() {
        return bonusFaith;
    }

    public AttributesBonusEntity setBonusFaith(Short bonusFaith) {
        this.bonusFaith = bonusFaith;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttributesBonusEntity that = (AttributesBonusEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("bonusStr", bonusStr)
                .add("bonusDex", bonusDex)
                .add("bonusInt", bonusInt)
                .add("bonusFaith", bonusFaith)
                .omitNullValues()
                .toString();
    }
}
