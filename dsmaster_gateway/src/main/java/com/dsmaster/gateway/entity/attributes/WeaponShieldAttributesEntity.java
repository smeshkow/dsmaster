package com.dsmaster.gateway.entity.attributes;

import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * User: artem_marinov
 * Date: 30.06.14
 * Time: 11:32
 */
@Entity
@Table(name = "weapons_shield_attributes", schema = "ds_core")
public class WeaponShieldAttributesEntity extends BaseAttributesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "physical_atk")
    private Short physicalAtk; //360 for ex

    @Column(name = "magic_atk")
    private Short magicAtk;

    @Column(name = "fire_atk")
    private Short fireAtk;

    @Column(name = "lightning_atk")
    private Short lightningAtk;

    @Column(name = "dark_atk")
    private Short darkAtk;

    @Column(name = "poison_atk")
    private Short poisonAtk;

    @Column(name = "bleed_atk")
    private Short bleedAtk;

//    @Column(name = "break_atk")
//    private Short breakAtk;


    @Column(name = "physical_reduction")
    private Short physicalReduction;

    @Column(name = "magic_reduction")
    private Short magicReduction;

    @Column(name = "lightning_reduction")
    private Short lightningReduction;

    @Column(name = "fire_reduction")
    private Short fireReduction;

    @Column(name = "dark_reduction")
    private Short darkReduction;

    @Column(name = "poison_reduction")
    private Short poisonReduction;

    @Column(name = "bleed_reduction")
    private Short bleedReduction;

//    @Column(name = "break_reduction")
//    private Short breakReduction;

    @Column(name = "curse_reduction")
    private Short curseReduction;

    @Column(name = "petrify_reduction")
    private Short petrifyReduction;

    @Column(name = "counter_strength")
    private Short counterStrength; //counter

    @Column(name = "stability")
    private Short stability;

    @Column(name = "poise_damage")
    private Short poiseDamage;

    @Column(name = "short_range")
    private Short shortRange; //Дальность выстрела. Актуально для луков/арбалетов.

    @Column(name = "cast_speed")
    private Short castSpeed; //актуально для магических


    public Integer getId() {
        return id;
    }

    public WeaponShieldAttributesEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Short getPhysicalAtk() {
        return physicalAtk;
    }

    public WeaponShieldAttributesEntity setPhysicalAtk(Short physicalAtk) {
        this.physicalAtk = physicalAtk;
        return this;
    }

    public Short getMagicAtk() {
        return magicAtk;
    }

    public WeaponShieldAttributesEntity setMagicAtk(Short magicAtk) {
        this.magicAtk = magicAtk;
        return this;
    }

    public Short getFireAtk() {
        return fireAtk;
    }

    public WeaponShieldAttributesEntity setFireAtk(Short fireAtk) {
        this.fireAtk = fireAtk;
        return this;
    }

    public Short getLightningAtk() {
        return lightningAtk;
    }

    public WeaponShieldAttributesEntity setLightningAtk(Short lightningAtk) {
        this.lightningAtk = lightningAtk;
        return this;
    }

    public Short getDarkAtk() {
        return darkAtk;
    }

    public WeaponShieldAttributesEntity setDarkAtk(Short darkAtk) {
        this.darkAtk = darkAtk;
        return this;
    }

    public Short getPoisonAtk() {
        return poisonAtk;
    }

    public WeaponShieldAttributesEntity setPoisonAtk(Short poisonAtk) {
        this.poisonAtk = poisonAtk;
        return this;
    }

    public Short getBleedAtk() {
        return bleedAtk;
    }

    public WeaponShieldAttributesEntity setBleedAtk(Short bleedAtk) {
        this.bleedAtk = bleedAtk;
        return this;
    }

    public Short getPhysicalReduction() {
        return physicalReduction;
    }

    public WeaponShieldAttributesEntity setPhysicalReduction(Short physicalReduction) {
        this.physicalReduction = physicalReduction;
        return this;
    }

    public Short getMagicReduction() {
        return magicReduction;
    }

    public WeaponShieldAttributesEntity setMagicReduction(Short magicReduction) {
        this.magicReduction = magicReduction;
        return this;
    }

    public Short getLightningReduction() {
        return lightningReduction;
    }

    public WeaponShieldAttributesEntity setLightningReduction(Short lightningReduction) {
        this.lightningReduction = lightningReduction;
        return this;
    }

    public Short getFireReduction() {
        return fireReduction;
    }

    public WeaponShieldAttributesEntity setFireReduction(Short fireReduction) {
        this.fireReduction = fireReduction;
        return this;
    }

    public Short getDarkReduction() {
        return darkReduction;
    }

    public WeaponShieldAttributesEntity setDarkReduction(Short darkReduction) {
        this.darkReduction = darkReduction;
        return this;
    }

    public Short getPoisonReduction() {
        return poisonReduction;
    }

    public WeaponShieldAttributesEntity setPoisonReduction(Short poisonReduction) {
        this.poisonReduction = poisonReduction;
        return this;
    }

    public Short getBleedReduction() {
        return bleedReduction;
    }

    public WeaponShieldAttributesEntity setBleedReduction(Short bleedReduction) {
        this.bleedReduction = bleedReduction;
        return this;
    }

    public Short getCurseReduction() {
        return curseReduction;
    }

    public WeaponShieldAttributesEntity setCurseReduction(Short curseReduction) {
        this.curseReduction = curseReduction;
        return this;
    }

    public Short getPetrifyReduction() {
        return petrifyReduction;
    }

    public WeaponShieldAttributesEntity setPetrifyReduction(Short petrifyReduction) {
        this.petrifyReduction = petrifyReduction;
        return this;
    }

    public Short getCounterStrength() {
        return counterStrength;
    }

    public WeaponShieldAttributesEntity setCounterStrength(Short counterStrength) {
        this.counterStrength = counterStrength;
        return this;
    }

    public Short getStability() {
        return stability;
    }

    public WeaponShieldAttributesEntity setStability(Short stability) {
        this.stability = stability;
        return this;
    }

    public Short getPoiseDamage() {
        return poiseDamage;
    }

    public WeaponShieldAttributesEntity setPoiseDamage(Short poiseDamage) {
        this.poiseDamage = poiseDamage;
        return this;
    }

    public Short getShortRange() {
        return shortRange;
    }

    public WeaponShieldAttributesEntity setShortRange(Short shortRange) {
        this.shortRange = shortRange;
        return this;
    }

    public Short getCastSpeed() {
        return castSpeed;
    }

    public WeaponShieldAttributesEntity setCastSpeed(Short castSpeed) {
        this.castSpeed = castSpeed;
        return this;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("physicalAtk", physicalAtk)
                .add("magicAtk", magicAtk)
                .add("fireAtk", fireAtk)
                .add("lightningAtk", lightningAtk)
                .add("darkAtk", darkAtk)
                .add("poisonAtk", poisonAtk)
                .add("bleedAtk", bleedAtk)
                .add("physicalReduction", physicalReduction)
                .add("magicReduction", magicReduction)
                .add("lightningReduction", lightningReduction)
                .add("fireReduction", fireReduction)
                .add("darkReduction", darkReduction)
                .add("poisonReduction", poisonReduction)
                .add("bleedReduction", bleedReduction)
                .add("curseReduction", curseReduction)
                .add("petrifyReduction", petrifyReduction)
                .add("counterStrength", counterStrength)
                .add("stability", stability)
                .add("poiseDamage", poiseDamage)
                .add("shortRange", shortRange)
                .add("castSpeed", castSpeed)
                .omitNullValues()
                .toString();
    }

}
