package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.article.ArticleEntity;
import com.dsmaster.gateway.entity.configuration.ConfigurationEntity;

import java.util.List;

/**
 * Created by smeshkov on 7/16/14.
 */
public interface ConfigurationDao extends AbstractDSDao<ConfigurationEntity, String> {

    @Override
    default Class<ConfigurationEntity> getEntityClass() {
        return ConfigurationEntity.class;
    }

    List<ConfigurationEntity> findByKey(String key);

}
