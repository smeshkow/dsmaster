package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.calculator.AttributePerformanceEntity;

import java.util.List;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 12:42
 */
public interface CalculatorDao extends SessionFactoryProvider {

    List<AttributePerformanceEntity> getAttributePerformances();

}
