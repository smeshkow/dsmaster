package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.ArticleDao;
import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.entity.article.ArticleEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by smeshkov on 8/1/14.
 */
@Repository
public class ArticleDaoImpl extends BasicDSDao implements ArticleDao {

    @Override
    public List<ArticleEntity> findByParams(String name, Collection<String> names, Collection<Byte> typeIds) {
        Criteria criteria = getSessionFactory().getCurrentSession()
                .createCriteria(getEntityClass());

        if(StringUtils.isNotEmpty(name)) {
            criteria.add(Restrictions.eq("name", name));
        } else if(CollectionUtils.isNotEmpty(names)) {
            criteria.add(Restrictions.in("name", names));
        }

        if(CollectionUtils.isNotEmpty(typeIds)) {
            criteria.add(Restrictions.in("typeId", typeIds));
        }

        return criteria.list();
    }

}
