package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.profile.ProfileEntity;
import com.dsmaster.gateway.entity.stuff.WeaponShieldEntity;

import java.util.List;

/**
 * User: artem_marinov
 * Date: 27.06.14
 * Time: 16:23
 */
public interface ProfileDao extends AbstractDSDao<ProfileEntity, Integer> {

    ProfileEntity findByExactEmail(String mail);

    List<ProfileEntity> findByEmail(String mail);

}
