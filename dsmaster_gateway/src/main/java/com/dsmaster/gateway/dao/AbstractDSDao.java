package com.dsmaster.gateway.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by smeshkov & artem_marinov on 26/06/14.
 * @param <T> - type of Entity
 * @param <I> - type of id of Entity
 */
public interface AbstractDSDao<T, I extends Serializable> extends SessionFactoryProvider {

    Class<T> getEntityClass();

    default T find(I id) {
        return (T) getSessionFactory().getCurrentSession().get(getEntityClass(), id);
    }

    default List<T> find() {
        return (List<T>) getSessionFactory().getCurrentSession().createCriteria(getEntityClass()).list();
    }

    default T persist(T entity) {
        getSessionFactory().getCurrentSession().persist(entity);
        return entity;
    }

}
