package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.dao.ConfigurationDao;
import com.dsmaster.gateway.entity.configuration.ConfigurationEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by smeshkov on 7/16/14.
 */
@Repository
public class ConfigurationDaoImpl extends BasicDSDao implements ConfigurationDao {

    @Override
    public List<ConfigurationEntity> findByKey(String key) {
        Session session = getSessionFactory().getCurrentSession();
        return session.createCriteria(getEntityClass())
                .add(Restrictions.like("key", "%" + key + "%"))
                .list();
    }
}
