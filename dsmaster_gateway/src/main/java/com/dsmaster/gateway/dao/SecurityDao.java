package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.security.UserKeyEntity;
import com.dsmaster.model.dto.security.AccessLevel;

import java.util.Collection;
import java.util.List;

/**
 * Created by smeshkov on 7/16/14.
 */
public interface SecurityDao extends AbstractDSDao<UserKeyEntity, String> {

    @Override
    default Class<UserKeyEntity> getEntityClass() {
        return UserKeyEntity.class;
    }

    List<UserKeyEntity> findByParams(String uuid, Collection<String> uuids, AccessLevel accessLevel, Collection<AccessLevel> accessLevels);

}
