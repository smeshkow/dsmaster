package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.stuff.WeaponShieldEntity;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 15:53
 */
public interface WeaponShieldDao extends AbstractDSDao<WeaponShieldEntity, Integer> {

}
