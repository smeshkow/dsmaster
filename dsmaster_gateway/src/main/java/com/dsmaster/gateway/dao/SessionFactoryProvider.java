package com.dsmaster.gateway.dao;

import org.hibernate.SessionFactory;

/**
 * User: artem_marinov
 * Date: 18.07.14
 * Time: 11:03
 */
public interface SessionFactoryProvider {

    SessionFactory getSessionFactory();

}
