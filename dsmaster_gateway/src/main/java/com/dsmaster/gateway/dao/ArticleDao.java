package com.dsmaster.gateway.dao;

import com.dsmaster.gateway.entity.article.ArticleEntity;

import java.util.Collection;
import java.util.List;

/**
 * Created by smeshkov on 8/1/14.
 */
public interface ArticleDao extends AbstractDSDao<ArticleEntity, Integer> {

    @Override
    default Class<ArticleEntity> getEntityClass() {
        return ArticleEntity.class;
    }

    List<ArticleEntity> findByParams(String name, Collection<String> names, Collection<Byte> typeIds);
}
