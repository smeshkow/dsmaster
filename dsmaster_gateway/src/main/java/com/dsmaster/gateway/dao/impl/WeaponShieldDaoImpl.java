package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.dao.WeaponShieldDao;
import com.dsmaster.gateway.entity.stuff.WeaponShieldEntity;
import org.springframework.stereotype.Repository;

import java.util.function.Function;

/**
 * User: artem_marinov
 * Date: 26.06.14
 * Time: 15:53
 */
@Repository
public class WeaponShieldDaoImpl extends BasicDSDao implements WeaponShieldDao {

    @Override
    public Class getEntityClass() {
        return WeaponShieldEntity.class;
    }

}
