package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.dao.ConfigurationDao;
import com.dsmaster.gateway.dao.SecurityDao;
import com.dsmaster.gateway.entity.configuration.ConfigurationEntity;
import com.dsmaster.gateway.entity.security.UserKeyEntity;
import com.dsmaster.model.dto.security.AccessLevel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by smeshkov on 7/16/14.
 */
@Repository
public class SecurityDaoImpl extends BasicDSDao implements SecurityDao {

    @Override
    public List<UserKeyEntity> findByParams(String uuid, Collection<String> uuids, AccessLevel accessLevel, Collection<AccessLevel> accessLevels) {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getEntityClass());
        if(StringUtils.isNotEmpty(uuid)) {
            criteria.add(Restrictions.eq("uuid", uuid));
        }
        if(CollectionUtils.isNotEmpty(uuids)) {
            criteria.add(Restrictions.in("uuid", uuids));
        }
        if(accessLevel != null) {
            criteria.add(Restrictions.eq("accessLevelId", accessLevel.getValue()));
        }
        if(CollectionUtils.isNotEmpty(accessLevels)) {
            List<Integer> accessLevelIds = accessLevels.stream().mapToInt(a -> a.getValue()).boxed().collect(Collectors.toList());
            criteria.add(Restrictions.in("accessLevelId", accessLevelIds));
        }
        return criteria.list();
    }
}
