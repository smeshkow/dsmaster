package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.dao.CalculatorDao;
import com.dsmaster.gateway.entity.calculator.AttributePerformanceEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 12:43
 */
@Repository
public class CalculatorDaoImpl extends BasicDSDao implements CalculatorDao {

    public List<AttributePerformanceEntity> getAttributePerformances() {
        return getSessionFactory().getCurrentSession().createCriteria(AttributePerformanceEntity.class).list();
    }

}
