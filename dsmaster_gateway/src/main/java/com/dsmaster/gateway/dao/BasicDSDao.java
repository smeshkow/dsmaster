package com.dsmaster.gateway.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by smeshkov on 26/06/14.
 */
public abstract class BasicDSDao implements SessionFactoryProvider {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
