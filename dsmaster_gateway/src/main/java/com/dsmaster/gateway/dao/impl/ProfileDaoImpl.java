package com.dsmaster.gateway.dao.impl;

import com.dsmaster.gateway.dao.BasicDSDao;
import com.dsmaster.gateway.dao.ProfileDao;
import com.dsmaster.gateway.entity.profile.ProfileEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: artem_marinov
 * Date: 27.06.14
 * Time: 16:24
 */
@Repository
public class ProfileDaoImpl extends BasicDSDao implements ProfileDao {

    @Override
    public Class<ProfileEntity> getEntityClass() {
        return ProfileEntity.class;
    }

    @Override
    public ProfileEntity findByExactEmail(String mail) {
        Session session = getSessionFactory().getCurrentSession();
        return (ProfileEntity) session.createCriteria(ProfileEntity.class)
                .add( Restrictions.eq("mail", mail.toLowerCase()))
                .uniqueResult();
    }

    @Override
    public List<ProfileEntity> findByEmail(String mail) {
        Session session = getSessionFactory().getCurrentSession();
        return session.createCriteria(ProfileEntity.class)
                .add( Restrictions.like("mail", "%" + mail + "%"))
                .list();
    }

}
