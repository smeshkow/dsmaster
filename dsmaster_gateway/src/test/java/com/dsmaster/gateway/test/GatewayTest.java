package com.dsmaster.gateway.test;

import com.dsmaster.api.gateway.request.StringRequest;
import com.google.common.base.Stopwatch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

/**
 * User: artem_marinov
 * Date: 25.06.14
 * Time: 16:37
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/gateway-server.xml"
})
public class GatewayTest {

    private Stopwatch sw = Stopwatch.createStarted();

    protected final Logger logger = LoggerFactory.getLogger(getClass());

//    @Autowired
//    private StuffService stuffService;

    @Test
    public void testStartGateway() {

        System.out.println(String.format("time to initialize %s ms", sw.elapsed(TimeUnit.MILLISECONDS)));

//        assertNotNull(stuffService);

        sw.reset();
        sw.start();

        int count = 50;
        for(int i=0; i< count; i++) {
            StringRequest stringRequest = new StringRequest();
            stringRequest.setValue(Long.valueOf(System.currentTimeMillis()).toString());
//            Response response = stuffService.doStuff(stringRequest);
        }

        System.out.println(String.format("time to %d inserts: %s", count, sw.elapsed(TimeUnit.MILLISECONDS)));

    }

}
