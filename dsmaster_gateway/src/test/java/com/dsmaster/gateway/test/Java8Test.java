package com.dsmaster.gateway.test;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * User: artem_marinov
 * Date: 02.07.14
 * Time: 16:12
 */
public class Java8Test {

    @Test
    @Ignore
    public void testLambda() {
        List<String> languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");
        filter(languages, (str)->str.startsWith("J"));
    }


    private void filter(List<String> names, Predicate<String> predicate) {
        names.stream().filter(  (t -> (! predicate.test(t)))  ).map(i -> Arrays.asList(i.length(), i.length())).forEach(System.out::println);
    }


}
