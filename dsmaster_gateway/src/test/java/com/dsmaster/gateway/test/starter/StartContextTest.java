package com.dsmaster.gateway.test.starter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

/**
 * User: artem_marinov
 * Date: 15.07.14
 * Time: 14:53
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/gateway-server.xml"
})
public class StartContextTest {

    @Test
    public void testStartContext() throws InterruptedException {
        System.out.println("-----> STARTED <-----");
        TimeUnit.HOURS.sleep(3);
    }

}
