package com.dsmaster.gateway.test;

import com.google.common.base.Splitter;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * User: artem_marinov
 * Date: 30.06.14
 * Time: 11:57
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/gateway-server.xml"
})
public class GrabInfoTest {

    private String weaponUrl = "http://api.gamehubbs.com/ds2/weapon?name=%s";

    private String armorUrl = "http://api.gamehubbs.com/ds2/armor?name=%s";

    private Splitter pipeSplitter = Splitter.on('|');


    @Test
    @Ignore
    public void testGrabWeapons() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/WEAPONS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabArmorHead() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/ARMOR - HEAD");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabArmorChest() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/ARMOR - CHEST");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabArmorHands() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/ARMOR - HANDS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabArmorLegs() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/ARMOR - LEGS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabRings() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/RINGS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabSpels() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/SPELS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }

    @Test
    @Ignore
    public void testGrabCovenants() throws IOException {
        Path path = Paths.get("/home/artem_marinov/Загрузки/dsm/COVENANTS");
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            reader.lines().parallel().forEach(this::_grabItem);
        }
    }




    private void _grabItem(String row) {
        String folderName = "armor/covenants";
        List<String> strings = pipeSplitter.splitToList(row);

        String imgName = strings.get(0);
        String imgUrl = strings.get(1);
        String name = strings.get(2);

        String dataUrl = String.format(armorUrl, name);
        String itemName = imgName.substring(0, imgName.length() - 4);
        try {
            FileUtils.copyURLToFile(new URL(imgUrl), new File("gamehubbs/" + folderName +"/"+imgName), 10_000, 10_000);
//            FileUtils.copyURLToFile(new URL(dataUrl), new File("gamehubbs/weapons/"+imgName), 10_000, 10_000);
        } catch (IOException e) {
            System.out.println(String.format("!!!  can't get %s ", imgUrl));
            e.printStackTrace();
        }
        System.out.println(String.format("saved:%s", itemName));
//        try {
//            Document document = Jsoup.connect(dataUrl).ignoreContentType(true).get();
//            document.outputSettings().prettyPrint(false);
//            String text = document.body().text();
//            FileUtils.write(new File("gamehubbs/" + folderName + "/"+itemName+".json"), text);
//            System.out.println(String.format("saved:%s", itemName));
//        } catch (IOException e) {
//            System.out.println(String.format("!!!  can't get %s", dataUrl));
//            e.printStackTrace();
//        }

    }

}
