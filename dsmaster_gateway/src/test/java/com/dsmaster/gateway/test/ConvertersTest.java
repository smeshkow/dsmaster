package com.dsmaster.gateway.test;

import com.dsmaster.model.util.Fns;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * User: artem_marinov
 * Date: 01.07.14
 * Time: 10:57
 */
public class ConvertersTest {

    @Test
    public void testMutateOneToOne() {
        Dto cloneTo = new Dto();
        cloneTo.setZ("ZZZ");
        Dto r4 = Fns.mutate(Entity::new, () -> cloneTo, MutateFns.entityToDtoConverter());

        assertEquals(cloneTo, r4);
        assertSame(cloneTo, r4);
    }

    @Test
    public void testConvertList() {
        List<Entity> entities = getEntities();
        List<Dto> r2 = entities.stream()
                .map(i -> Fns.mutate(() -> i, Dto::new, MutateFns.entityToDtoConverter()))
                .collect(Collectors.toList());

        assertTrue(r2.size() == entities.size());
    }

    @Test
    public void testConvertSingle1() {
        Dto r3 = Fns.mutate(Entity::new, Dto::new, MutateFns.entityToDtoConverter());
        assertNotNull(r3);
    }

    @Test
    public void testConvertSingle2() {
        Entity entity = new Entity();
        entity.setName("zaza");
        entity.setId(33L);
        Dto r3 = Fns.mutate(() -> entity, Dto::new, MutateFns.entityToDtoConverter());
        assertEquals(r3.getId().intValue(), entity.getId().intValue());
    }




    @Test
    @Ignore
    public void testConverter() {

        List<Entity> entities = getEntities();

        //do not remove pls
//        List<Dto> r = entities.stream().collect((Supplier<List<Dto>>) ArrayList::new, new BiConsumer<List<Dto>, Entity>() {
//
        //нужен одиночный мутатор
        //нужен множественный конвертр


//        entities.stream().forEach(i -> {
//            mutate(Entity::new, Dto::new, this.entityToDtoConverter());
//        });

    }

    private List<Entity> getEntities() {
        List<Entity> entities = new ArrayList<>();

        for(int i=0; i<100; i++) {
            Entity entity = new Entity();
            entity.setId(Long.valueOf(i));
            entity.setName("name - " + i * 31);
            entities.add(entity);
        }

        return entities;
    }

}

class MutateFns {



    public static BiFunction<Entity, Dto, Dto> entityToDtoConverter() {
        return (f, t ) -> {
            t.setName(f.getName());
            Long fromId = f.getId();
            int id = fromId != null ? fromId.intValue() : -1;
            t.setId(id);
            t.setConcat("it is concat for " + id);
            return t;
        };
    }

}

class Dto {

    private Integer id;

    private String name;

    private String concat;

    private String z;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConcat() {
        return concat;
    }

    public void setConcat(String concat) {
        this.concat = concat;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dto dto = (Dto) o;

        if (concat != null ? !concat.equals(dto.concat) : dto.concat != null) return false;
        if (id != null ? !id.equals(dto.id) : dto.id != null) return false;
        if (name != null ? !name.equals(dto.name) : dto.name != null) return false;
        if (z != null ? !z.equals(dto.z) : dto.z != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (concat != null ? concat.hashCode() : 0);
        result = 31 * result + (z != null ? z.hashCode() : 0);
        return result;
    }
}


class Entity {

    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity entity = (Entity) o;

        if (id != null ? !id.equals(entity.id) : entity.id != null) return false;
        if (name != null ? !name.equals(entity.name) : entity.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}