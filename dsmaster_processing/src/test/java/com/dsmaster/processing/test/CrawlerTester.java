package com.dsmaster.processing.test;

import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.processing.crawler.CrawlerState;
import com.dsmaster.processing.crawler.DsmCrawler;
import com.google.common.base.Stopwatch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

/**
 * Created by smeshkov on 06.08.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/processing-server.xml"
})
public class CrawlerTester {

    @Test
    public void testCrawler() {
        Stopwatch sw = Stopwatch.createStarted();
        ArticleType type = ArticleType.RING;
        DsmCrawler crawler = DsmCrawler.createStarted(type, true, true);
        CrawlerState state = crawler.finish();
        sw.stop();
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        System.out.println(String.format("%s data [%d], elapsed %d %s",
                type, state.getData().size(), sw.elapsed(timeUnit), timeUnit));
    }

}
