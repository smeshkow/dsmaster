package com.dsmaster.processing.test;

import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.processing.crawler.CrawlerState;
import com.dsmaster.processing.crawler.DsmCrawler;
import com.google.common.base.Stopwatch;

import java.util.concurrent.TimeUnit;

/**
 * Created by smeshkov on 7/29/14.
 */
public class CrawlerSimpleTester {

    public static void main(String[] args) {
        Stopwatch sw = Stopwatch.createStarted();
        DsmCrawler crawler = DsmCrawler.createStarted(ArticleType.MENU);
        CrawlerState state = crawler.finish();
        sw.stop();
        System.out.println(String.format("menu data [%d], elapsed %d %s",
                state.getData().size(), sw.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS));
//
//        Map<String, String> pairMap = data.stream().collect(Collectors.toMap(Pair<String, String>::getLeft, Pair<String, String>::getRight));
//        System.out.println(pairMap);
    }

}
