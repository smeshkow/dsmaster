package com.dsmaster.processing;

import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.processing.crawler.CrawlerState;
import com.dsmaster.processing.crawler.DsmCrawler;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by smeshkov on 25/06/14.
 */
public class Scheduler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void postConstruct() {

    }

    @Scheduled( cron = "0 0 3 * * *" )
    public void updateWeaponShield() {
        Stopwatch sw = Stopwatch.createStarted();
        ArticleType type = ArticleType.WEAPON_SHIELD;
        DsmCrawler crawler = DsmCrawler.createStarted(type, true, true);
        CrawlerState state = crawler.finish();
        sw.stop();
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        log.info(String.format("%s data [%d], elapsed %d %s",
                type, state.getData().size(), sw.elapsed(timeUnit), timeUnit));
    }

}
