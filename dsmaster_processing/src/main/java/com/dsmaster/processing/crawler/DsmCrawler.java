package com.dsmaster.processing.crawler;

import com.dsmaster.api.client.article.ArticleClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.article.ArticleListResponse;
import com.dsmaster.api.gateway.service.article.ArticleParamsRequest;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.model.util.ExcHandler;
import com.dsmaster.model.util.AppCtxHolder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

/**
 * Created by smeshkov on 7/10/14.
 */

public class DsmCrawler {

    private static final Logger log =
            LoggerFactory.getLogger(DsmCrawler.class);

    private static final String SITE_URL = "http://darksouls2.wiki.fextralife.com";

    private final ExecutorService runner = Executors.newSingleThreadExecutor();
    private final CrawlerState state;

    private Crawler crawler;
    private Future<CrawlerState> result;

    private DsmCrawler() {
        state = new CrawlerState();
    }

    public static DsmCrawler createStarted(ArticleType type) {
        return createStarted(type, false, false);
    }

    public static DsmCrawler createStarted(ArticleType type, boolean saveToDb, boolean fast) {
        DsmCrawler dsmCrawler = create(type, saveToDb, fast);
        return dsmCrawler.run();
    }

    public static DsmCrawler create(ArticleType type, boolean saveToDb, boolean fast) {
        DsmCrawler dsmCrawler = new DsmCrawler();
        ArticleClient client = AppCtxHolder.get().getBean(ArticleClient.class);
        switch (type) {
            case ALL:
                buildMenuCrawler(saveToDb, fast, dsmCrawler);
                buildWeaponShieldCrawler(saveToDb, fast, dsmCrawler, client);
                buildArmorCrawler(saveToDb, fast, dsmCrawler, client);
                buildRingCrawler(saveToDb, fast, dsmCrawler, client);
                // TODO add rest of crawlers
                break;
            case MENU:
                buildMenuCrawler(saveToDb, fast, dsmCrawler);
                break;
            case WEAPON_SHIELD:
                buildWeaponShieldCrawler(saveToDb, fast, dsmCrawler, client);
                break;
            case ARMOR:
                buildArmorCrawler(saveToDb, fast, dsmCrawler, client);
                break;
            case ARROW:
                // TODO
                ExcHandler.throwNotSupported(type.toString());
                break;
            case RING:
                buildRingCrawler(saveToDb, fast, dsmCrawler, client);
                break;
            case SPELL:
                // TODO
                ExcHandler.throwNotSupported(type.toString());
                break;
            case ITEM:
                // TODO
                ExcHandler.throwNotSupported(type.toString());
                break;
        }
        return dsmCrawler;
    }

    private static void addCrawlerToChain(DsmCrawler dsmCrawler, Crawler crawler) {
        if(dsmCrawler.crawler == null) {
            dsmCrawler.crawler = crawler;
        } else {
            Crawler currentCrawler = dsmCrawler.crawler;
            while (currentCrawler.getNext() != null) currentCrawler = currentCrawler.getNext();
            currentCrawler.setNext(crawler);
        }
    }

    private static void buildMenuCrawler(boolean saveToDb, boolean fast, DsmCrawler dsmCrawler) {
        addCrawlerToChain(dsmCrawler, new MenuCrawler(dsmCrawler.state, SITE_URL, "/Sitemap",
                Lists.newArrayList("#content_view .wiki_table tbody tr td"), saveToDb, fast));
    }

    private static void buildArmorCrawler(boolean saveToDb, boolean fast, DsmCrawler dsmCrawler, ArticleClient client) {
        try {
            ArticleListResponse response = client.findByParams(new ArticleParamsRequest(new Request())
                    .setName("Armor")
                    .setTypes(ImmutableSet.of(ArticleType.MENU)));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                Article article = response.getArticleList().get(0);
                addCrawlerToChain(dsmCrawler, new ArmorCrawler(dsmCrawler.state, SITE_URL, article.getLink(),
                        Lists.newArrayList("#content_view .wiki_table tbody tr td strong a"),
                        saveToDb, fast));
            } else {
                log.error("[{}] {}", response.getResponse().getStatusCode(), response.getResponse().getErrorMessage());
            }
        } catch (TException e) { ExcHandler.ex(e); }
    }

    private static void buildWeaponShieldCrawler(boolean saveToDb, boolean fast, DsmCrawler dsmCrawler, ArticleClient client) {
        try {
            ArticleListResponse response = client.findByParams(new ArticleParamsRequest(new Request())
                    .setNames(ImmutableSet.of("Weapons", "Shields"))
                    .setTypes(ImmutableSet.of(ArticleType.MENU)));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                for (Article article : response.getArticleList()) {
                    addCrawlerToChain(dsmCrawler, new WeaponShieldCrawler(dsmCrawler.state, SITE_URL, article.getLink(),
                            Lists.newArrayList("#content_view .wiki_table tbody tr td a"),
                            saveToDb, fast));
                }
            } else {
                log.error("[{}] {}", response.getResponse().getStatusCode(), response.getResponse().getErrorMessage());
            }
        } catch (TException e) { ExcHandler.ex(e); }
    }

    private static void buildRingCrawler(boolean saveToDb, boolean fast, DsmCrawler dsmCrawler, ArticleClient client) {
        try {
            ArticleListResponse response = client.findByParams(new ArticleParamsRequest(new Request())
                    .setNames(ImmutableSet.of("Rings"))
                    .setTypes(ImmutableSet.of(ArticleType.MENU)));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                Article article = response.getArticleList().get(0);
                addCrawlerToChain(dsmCrawler, new RingCrawler(dsmCrawler.state, SITE_URL, article.getLink(),
                        Lists.newArrayList("#content_view .wiki_table tbody tr td:eq(1) a"),
                        saveToDb, fast));
            } else {
                log.error("[{}] {}", response.getResponse().getStatusCode(), response.getResponse().getErrorMessage());
            }
        } catch (TException e) { ExcHandler.ex(e); }
    }

    public DsmCrawler run() {
        if(crawler == null) {
            throw new NullPointerException("Crawler is null!");
        }
        result = runner.submit(new CrawlerRunner(crawler));
        return this;
    }

    public List<Runnable> stop() {
        return state.stop();
    }

    public CrawlerState finish() {
        if(result == null) throw new NullPointerException("Crawler wasn't started! Use run() to start");
        try {
            return result.get();
        } catch (InterruptedException e) { ExcHandler.interruption(e); }
        catch (ExecutionException e) { ExcHandler.ex(e); }
        return null;
    }

    private static class CrawlerRunner implements Callable<CrawlerState> {

        private Crawler crawler;

        private CrawlerRunner(Crawler crawler) {
            this.crawler = crawler;
        }

        @Override
        public CrawlerState call() throws Exception {
            Crawler next = crawler;
            do {
                crawler.getState().runCrawler(next);
                crawler = next;
                while (crawler.getState().inProgress()) TimeUnit.MILLISECONDS.sleep(5000);
                next = crawler.getNext();
            } while (next != null);
            return crawler.getState();
        }
    }

}
