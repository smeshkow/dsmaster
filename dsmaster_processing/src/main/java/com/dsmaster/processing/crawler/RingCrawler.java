package com.dsmaster.processing.crawler;

import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.model.dto.common.Addon;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

/**
 * Created by smeshkov on 7/30/14.
 */
public class RingCrawler extends Crawler<RingCrawler> {

    private static final ArticleType TYPE = ArticleType.RING;

    public RingCrawler(CrawlerState state, String siteUrl, String urlStr, List<String> selectors, boolean isSaveToDb, boolean isFast) {
        super(state, siteUrl, urlStr, selectors, isSaveToDb, isFast);
    }

    @Override
    public CrawlerState call() throws Exception {
        return start();
    }

    @Override
    protected CrawlerState crawl(Document doc) {
        if(CollectionUtils.isNotEmpty(selectors)) {
            for (String selector : selectors) {
                Elements elements = doc.select(selector);
                for (Element a : elements) {
                    if(a.hasClass("wiki_link")) {
                        String href = a.attr("href");
                        if(StringUtils.isNotEmpty(href) && StringUtils.isNotEmpty(a.text())) {
                            String link = href.startsWith("http") ? href : siteUrl + href;
                            final Article article = new Article(Addon.BASE, a.text(), TYPE).setLink(link);
                            fetchDoc(link).ifPresent(d -> parseInfoPage(d, article));
                            if(isSaveToDb) article.setId(saveToDb(article).getId());
                            log.info("parsed -> {}", article);
                            state.addData(article);
                        }
                    }
                }
            }
        } else {
            log.error("selectors list is empty or null!");
        }
        return state;
    }

    private void parseInfoPage(Document doc, Article article) {
        Element description = null;
        Elements descriptionEls = doc.select("#content_view h1#toc0");
        if(CollectionUtils.isNotEmpty(descriptionEls)) description = descriptionEls.get(0);

        Element effect = null;
        Elements effectEls = doc.select("#content_view h2#toc1");
        if(CollectionUtils.isNotEmpty(effectEls)) effect = effectEls.get(0);

        Element location = null;
        Elements locationEls = doc.select("#content_view h2#toc2");
        if(CollectionUtils.isNotEmpty(locationEls)) location = locationEls.get(0);

        Element notes = null;
        Elements notesEls = doc.select("#content_view h2#toc3");
        if(CollectionUtils.isNotEmpty(notesEls)) notes = notesEls.get(0);

        Elements siblings = description != null ? description.siblingElements() :
                effect != null ? effect.siblingElements() :
                        location != null ? location.siblingElements() :
                                notes != null ? notes.siblingElements() : null;
        int allSiblingsCount = siblings != null ? siblings.size() : 0;

        // description
        if(description != null) {
            StringBuilder sb = new StringBuilder();
            Element nextPart = effect != null ? effect : location != null ? location : notes != null ? notes : null;
            int excludeIndex = nextPart != null ? nextPart.elementSiblingIndex() - 1 : allSiblingsCount;
            for(int i = description.elementSiblingIndex(); i < excludeIndex; i++) {
                Element sib = siblings.get(i);
                sb.append(sib.toString());
            }
            if(sb.length() > 0) article.setDescription(sb.toString());
        }

        StringBuilder notesAndEffect = new StringBuilder();

        // Effect
        if(effect != null) {
            Element nextPart = location != null ? location : notes != null ? notes : null;
            int excludeIndex = nextPart != null ? nextPart.elementSiblingIndex() - 1 : allSiblingsCount;
            for(int i = effect.elementSiblingIndex(); i < excludeIndex; i++) {
                Element sib = siblings.get(i);
                notesAndEffect.append(sib.toString());
            }
        }

        // Location
        if(location != null) {
            StringBuilder sb = new StringBuilder();
            Element nextPart = notes != null ? notes : null;
            int excludeIndex = nextPart != null ? nextPart.elementSiblingIndex() - 1 : allSiblingsCount;
            for(int i = location.elementSiblingIndex(); i < excludeIndex; i++) {
                Element sib = siblings.get(i);
                sb.append(sib.toString());
            }
            if(sb.length() > 0) article.setAcquiredFrom(sb.toString());
        }

        // Notes
        if(notes != null) {
            int excludeIndex = allSiblingsCount;
            for(int i = notes.elementSiblingIndex(); i < excludeIndex; i++) {
                Element sib = siblings.get(i);
                notesAndEffect.append(sib.toString());
            }
        }

        if(notesAndEffect.length() > 0) article.setHintsAndTips(notesAndEffect.toString());
    }
}