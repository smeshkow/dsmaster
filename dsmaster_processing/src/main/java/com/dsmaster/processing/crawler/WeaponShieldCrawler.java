package com.dsmaster.processing.crawler;

import com.dsmaster.api.client.article.ArticleClient;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.model.dto.common.Addon;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.Optional;

/**
 * Created by smeshkov on 7/30/14.
 */
public class WeaponShieldCrawler extends Crawler<WeaponShieldCrawler> {

    private static final ArticleType TYPE = ArticleType.WEAPON_SHIELD;

    public WeaponShieldCrawler(CrawlerState state, String siteUrl, String urlStr, List<String> selectors, boolean isSaveToDb, boolean isFast) {
        super(state, siteUrl, urlStr, selectors, isSaveToDb, isFast);
    }

    @Override
    public CrawlerState call() throws Exception {
        return start();
    }

    @Override
    protected CrawlerState crawl(Document doc) {
        if(CollectionUtils.isNotEmpty(selectors)) {
            for (String selector : selectors) {
                Elements elements = doc.select(selector);
                for (Element a : elements) {
                    if(!a.hasClass("wiki_link")) {
                        String href = a.attr("href");
                        if(StringUtils.isNotEmpty(href) && StringUtils.isNotEmpty(a.html())) {
                            String link = href.startsWith("http") ? href : siteUrl + href;
                            Elements img = a.getElementsByTag("img");
                            if(img != null) {
                                String name = img.attr("title").replaceAll("\\.(png|gif|jpg|jpeg)$", "");
                                if(StringUtils.isNotEmpty(name)) {
                                    final Article article = new Article(Addon.BASE, name, TYPE).setLink(link);
                                    fetchDoc(link).ifPresent(d -> parseInfoPage(d, article));
                                    if(isSaveToDb) article.setId(saveToDb(article).getId());
                                    log.info("parsed -> {}", article);
                                    state.addData(article);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            log.error("selectors list is empty or null!");
        }
        return state;
    }

    private void parseInfoPage(Document doc, Article article) {
        // description
        Elements description = doc.select("#content_view table.wiki_table tbody tr td[colspan=6]");
        if(CollectionUtils.isNotEmpty(description)) {
            Element el = description.get(0);
            if(StringUtils.isNotEmpty(el.html())) {
                article.setDescription(el.html());
            }
        }

        Elements elements = doc.select("#content_view strong");
        if(CollectionUtils.isNotEmpty(elements)) {
            for (Element element : elements) {
                if(element.text().contains("Acquired From")) {
                    for (Element sib : element.siblingElements()) {
                        if(sib.tagName().equals("ul") && StringUtils.isNotEmpty(sib.html())) {
                            article.setAcquiredFrom(sib.html());
                            break;
                        }
                    }
                } else if(element.text().contains("Hints and Tips")) {
                    for (Element sib : element.siblingElements()) {
                        if(sib.tagName().equals("ul") && StringUtils.isNotEmpty(sib.html()) && !sib.html().equals(article.getAcquiredFrom())) {
                            article.setHintsAndTips(sib.html());
                            break;
                        }
                    }
                }
            }
        }
    }
}