package com.dsmaster.processing.crawler;

import com.dsmaster.model.dto.article.Article;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by smeshkov on 7/31/14.
 */
public class CrawlerState {

    private final ExecutorService exec;
    private final List<Future> results;
    private final List<Article> data;

    public CrawlerState() {
        exec = Executors.newCachedThreadPool();
        results = new ArrayList<>();
        data = new ArrayList<>();
    }

    public CrawlerState addData(Article pair) {
        data.add(pair);
        return this;
    }

    public Future runCrawler(Crawler crawler) {
        Future submit = exec.submit(crawler);
        results.add(submit);
        return submit;
    }

    public boolean inProgress() {
        for (Future f : results) {
            if(!f.isDone() && !f.isCancelled()) {
                return true;
            }
        }
        return false;
    }

    public List<Runnable> stop() {
        List<Runnable> runnables = exec.shutdownNow();
        return runnables;
    }

    public List<Future> getResults() {
        return results;
    }

    public List<Article> getData() {
        return data;
    }

}
