package com.dsmaster.processing.crawler;

import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.dto.article.ArticleType;
import com.dsmaster.model.dto.common.Addon;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

/**
 * Created by smeshkov on 7/30/14.
 */
public class MenuCrawler extends Crawler<MenuCrawler> {

    private static final ArticleType TYPE = ArticleType.MENU;

    public MenuCrawler(CrawlerState state, String siteUrl, String urlStr, List<String> selectors, boolean isSaveToDb, boolean isFast) {
        super(state, siteUrl, urlStr, selectors, isSaveToDb, isFast);
    }

    @Override
    public CrawlerState call() throws Exception {
        return start();
    }

    @Override
    protected CrawlerState crawl(Document doc) {
        if(CollectionUtils.isNotEmpty(selectors)) {
            for (String selector : selectors) {
                Elements menuItems = doc.select(selector);
                for (Element element : menuItems) {
                    Elements aEls = element.getElementsByTag("a");
                    if(CollectionUtils.isNotEmpty(aEls)) {
                        for (Element a : aEls) {
                            String href = a.attr("href");
                            if(StringUtils.isNotEmpty(href) && StringUtils.isNotEmpty(a.text())) {
                                Article article = new Article(Addon.BASE, a.text(), TYPE).setLink(siteUrl + href);
                                if(isSaveToDb) article = saveToDb(article);
                                log.debug("parsed -> {}", article);
                                state.addData(article);
                                state.runCrawler(new MenuCrawler(state, siteUrl, href, Lists.newArrayList("#content_view h3 span a"), isSaveToDb, isFast));
                            }
                        }
                    }
                }
            }
        } else {
            log.error("selectors list is empty or null!");
        }
        return state;
    }

}