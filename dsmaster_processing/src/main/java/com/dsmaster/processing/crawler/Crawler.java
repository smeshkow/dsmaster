package com.dsmaster.processing.crawler;

import com.dsmaster.api.client.article.ArticleClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.article.ArticleRequest;
import com.dsmaster.api.gateway.service.article.ArticleResponse;
import com.dsmaster.model.dto.article.Article;
import com.dsmaster.model.util.ExcHandler;
import com.dsmaster.model.util.AppCtxHolder;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Created by smeshkov on 7/30/14.
 */
public abstract class Crawler<T extends Crawler> implements Callable<CrawlerState> {

    protected final Logger log =
            LoggerFactory.getLogger(getClass());

    private static final int TIMEOUT = 30000;

    protected final String siteUrl;
    protected final String urlStr;
    protected final List<String> selectors;
    protected final CrawlerState state;
    protected final boolean isSaveToDb;
    protected final boolean isFast;
    protected final ArticleClient client;

    protected Crawler next;

    protected Crawler(CrawlerState state, String siteUrl, String urlStr, List<String> selectors, boolean isSaveToDb, boolean isFast) {
        this.state = state;
        this.siteUrl = siteUrl;
        this.urlStr = urlStr;
        this.selectors = selectors;
        this.isSaveToDb = isSaveToDb;
        this.isFast = isFast;
        this.client = AppCtxHolder.get().getBean(ArticleClient.class);
    }

    protected CrawlerState start() throws Exception{
        if (!Thread.currentThread().isInterrupted()) {
            String link = urlStr.startsWith("http") ? urlStr : siteUrl + urlStr;
            Optional<Document> docOptional = fetchDoc(link);
            if(docOptional.isPresent()) {
                return docOptional.map(d -> crawl(d)).get();
            }
            return state;
        } else {
            log.error("was interrupted");
            throw new InterruptedException("was interrupted");
        }
    }

    protected Optional<Document> fetchDoc(String urlStr) {
        if (StringUtils.isNotEmpty(urlStr)) {
            try {
                Connection connection = Jsoup.connect(urlStr);
                connection.timeout(TIMEOUT);
                connection.followRedirects(true);
                connection.userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0");
                Connection.Response response = connection.execute();
                if (response.statusCode() == 200) {
                    return Optional.of(response.parse());
                } else {
                    log.error("Wrong status code -> {}: {}", response.statusCode(), response.statusMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
                log.error("Can't parse url \"{}\"", urlStr);
            }
        } else {
            log.error("Url is empty \"{}\"", urlStr);
        }
        return Optional.empty();
    }

    protected Article saveToDb(Article article) {
        ArticleRequest request = new ArticleRequest(new Request(), article);
        try {
            if(isFast) {
                client.saveOneway(request);
            } else {
                ArticleResponse response = client.save(request);
                if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                    article = response.getArticle();
                }
            }
        } catch (TException e) { ExcHandler.ex(e); }
        return article;
    }

    protected abstract CrawlerState crawl(Document doc);

    public Crawler getNext() {
        return next;
    }

    public T setNext(Crawler next) {
        this.next = next;
        return (T) this;
    }

    public CrawlerState getState() {
        return state;
    }


}
