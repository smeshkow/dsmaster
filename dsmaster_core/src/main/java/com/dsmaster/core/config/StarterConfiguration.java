package com.dsmaster.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by smeshkov on 7/7/14.
 */

@Configuration
public class StarterConfiguration {

    protected final Logger log =
            LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void contextInitialized() {
        // TODO init here...
        log.info("Context Initialised");
    }

    @PreDestroy
    public void contextdestroyed() {
        // TODO destroy here...
        log.info("Context Destroyed");
    }

}
