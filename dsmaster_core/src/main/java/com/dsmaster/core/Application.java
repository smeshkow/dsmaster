package com.dsmaster.core;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.system.ApplicationPidListener;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.annotation.PreDestroy;
import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by smeshkov on 05/07/14.
 */
@Configuration
public class Application {

    protected static final Logger log =
            LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            String moduleName = System.getProperty("moduleName");
            log.info("moduleName: {}", moduleName);
            Preconditions.checkState(StringUtils.isNotEmpty(moduleName));
            log.info("Starting App");
            Stopwatch sw = Stopwatch.createStarted();

            String xmlContextPath = System.getProperty("xmlContextPath");
            log.info("XML context path -> [{}]", xmlContextPath);
            if(StringUtils.isNotEmpty(xmlContextPath)) {
                SpringApplication springApplication = new SpringApplication(new Object[]{Application.class, xmlContextPath});
                File pidFile = new File(String.format("/etc/dsmaster/%s/%s.pid", moduleName, moduleName));
                springApplication.addListeners(new ApplicationPidListener(pidFile));
                log.info("PID file path: {}", pidFile.getAbsolutePath());
                //TODO тут наверное имеет смысл еще и создавать шел скрипт
                springApplication.run(args);
//                SpringApplication.run(new Object[]{Application.class, xmlContextPath}, args);
            } else {
                throw new NullPointerException("XML context path is empty!");
//                SpringApplication.run(Application.class, args);
            }

            sw.stop();
            log.info( "\n\n******************    O_o   *********************\n" +
                    "                App started in:" + sw.elapsed(TimeUnit.SECONDS) + "s \n" +
                    "**************************************************\n\n" );
//            System.exit(0);
        } catch(Throwable t) {
            log.error("Server start failed", t);
            t.printStackTrace();
            throw t;
        }
    }

    @PreDestroy
    public void preDestroy() {
        log.info("Stop application....");
    }
}