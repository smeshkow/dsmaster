#!/bin/sh
SERVICE_NAME=${moduleName}
#PID_PATH_NAME=/etc/dsmaster/gateway/gateway.pid
PID_PATH_NAME=${pidPath}
case $1 in
    start)
        echo "Starting $SERVICE_NAME ..."
        if [ ! -f $PID_PATH_NAME ]; then
            #cd  /home/artem_marinov/dsm/dsmaster/dsmaster_gateway
            #nohup /home/artem_marinov/bin/gradle2 :dsmaster_gateway:run > /var/log/dsmaster/gateway/server.log &
            ${starterCommand}
            echo "$SERVICE_NAME started ..."
        else
            echo "$SERVICE_NAME is already running ..."
        fi
    ;;
    status)
		if [ -f $PID_PATH_NAME ]; 
			then
				echo "$SERVICE_NAME running."
			else
				echo "$SERVICE_NAME not running."
		fi
	;;
    stop)
        if [ -f $PID_PATH_NAME ]; then
            PID=$(cat $PID_PATH_NAME);
            echo "$SERVICE_NAME stoping..."

			NOT_KILLED=1
		    for i in 1 2 3 4 5 6; do
		    	if ps -p $PID > /dev/null
				then
					kill $PID;
					echo "."
		        	sleep 5
				else
					NOT_KILLED=0
				fi
	    	done
            
			if [ $NOT_KILLED = 1 ]
    		then
    			echo "$SERVICE_NAME not killed yet :(. PID: $PID"
			else
			 	rm $PID_PATH_NAME
			 	echo "$SERVICE_NAME stopped..."				
			fi            
            
        else
            echo "$SERVICE_NAME is not running or unknown pid."
        fi
    ;;
    restart)
        $0 stop
        $0 start
    ;;

esac 
