include '../../request/Request.thrift'
include 'Profile.thrift'

namespace java com.dsmaster.api.gateway.service.profile

struct ProfileRequestD {
    1: Request.Request request,
    2: Profile.Profile dto
}