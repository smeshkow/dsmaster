include '../../request/Request.thrift'

namespace java com.dsmaster.api.gateway.service.profile

struct AuthRequest {
    1: Request.Request request,
    2: string email,
    3: string password
}