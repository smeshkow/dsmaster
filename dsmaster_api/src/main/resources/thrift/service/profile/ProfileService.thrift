include '../../response/Response.thrift'
include 'RegisterRqst.thrift'
include 'AuthRequest.thrift'
include 'AuthResponse.thrift'

namespace java com.dsmaster.api.gateway.service.profile

service ProfileService {
    Response.Response registerUser(1: RegisterRqst.RegisterRqst request);
    AuthResponse.AuthResponse authUser(1: AuthRequest.AuthRequest request);
}