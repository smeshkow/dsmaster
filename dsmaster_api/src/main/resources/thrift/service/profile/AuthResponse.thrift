include '../../response/Response.thrift'
include 'AuthStatus.thrift'
include 'AccessLevel.thrift'

namespace java com.dsmaster.api.gateway.service.security

struct AuthResponse {
    1: Response.Response response,
    2: AuthStatus.AuthStatus authStatus,
    3: AccessLevel.AccessLevel accessLevel
}