include '../../request/Request.thrift'
include 'WeaponShield.thrift'

namespace java com.dsmaster.api.gateway.service.stuff

struct SaveWeaponShieldRequest {
    1: Request.Request request,
    2: WeaponShield.WeaponShield weaponShield;
}