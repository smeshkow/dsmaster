include '../../response/Response.thrift'
include 'WeaponShield.thrift'

namespace java com.dsmaster.api.gateway.service.stuff

struct WeaponShieldListResponse {
    1: Response.Response response,
    2: list<WeaponShield.WeaponShield> weaponShields;
}