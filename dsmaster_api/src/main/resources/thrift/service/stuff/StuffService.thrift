include '../../request/Request.thrift'
include '../../response/Response.thrift'
include '../../request/StringRequest.thrift'
include 'SaveWeaponShieldRequest.thrift'
include 'WeaponShieldListResponse.thrift'

namespace java com.dsmaster.api.gateway.service.stuff

service StuffService {
    WeaponShieldListResponse.WeaponShieldListResponse findWeaponShields(1: Request.Request request);
    Response.Response saveWeaponShield(1: SaveWeaponShieldRequest.SaveWeaponShieldRequest request);
}