include '../../request/Request.thrift'
include 'UserKey.thrift'

namespace java com.dsmaster.api.gateway.service.security

struct UserKeyRqstLvlA {
    1: Request.Request request,
    2: UserKey.UserKey dto
}