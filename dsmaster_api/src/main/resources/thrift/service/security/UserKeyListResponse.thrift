include '../../response/Response.thrift'
include 'UserKey.thrift'

namespace java com.dsmaster.api.gateway.service.security

struct UserKeyListResponse {
    1: Response.Response response,
    2: optional list<UserKey.UserKey> dtoList
}