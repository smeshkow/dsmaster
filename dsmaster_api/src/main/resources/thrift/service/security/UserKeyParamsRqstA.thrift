include '../../request/Request.thrift'
include 'UserKey.thrift'
include 'AccessLevel.thrift'

namespace java com.dsmaster.api.gateway.service.security

struct UserKeyParamsRqstA {
    1: Request.Request request,
    2: optional string uuid,
    3: optional set<string> uuids,
    4: optional AccessLevel.AccessLevel accessLevel,
    5: optional set<AccessLevel.AccessLevel> accessLevels
}