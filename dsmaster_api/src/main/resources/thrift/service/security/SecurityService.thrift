include '../../response/Response.thrift'
include '../../request/Request.thrift'
include 'UserKeyListResponse.thrift'
include 'GetUserKeyListRqstLvlA.thrift'
include 'UserKeyRqstLvlA.thrift'
include 'UserKeyParamsRqstA.thrift'

namespace java com.dsmaster.api.gateway.service.security

service SecurityService {
    UserKeyListResponse.UserKeyListResponse getAllKeys(1: GetUserKeyListRqstLvlA.GetUserKeyListRqstLvlA request);
    UserKeyListResponse.UserKeyListResponse getAllKeysForCache(1: Request.Request request);
    Response.Response saveKey(1: UserKeyRqstLvlA.UserKeyRqstLvlA request);
    oneway void saveKeyOneway(1: UserKeyRqstLvlA.UserKeyRqstLvlA request);
    UserKeyListResponse.UserKeyListResponse findByParams(1: UserKeyParamsRqstA.UserKeyParamsRqstA request);
}