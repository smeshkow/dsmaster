include '../../response/Response.thrift'
include 'Configuration.thrift'

namespace java com.dsmaster.api.gateway.service.configuration

struct ConfigurationResponse {
    1: Response.Response response,
    2: optional Configuration.Configuration configuration
}