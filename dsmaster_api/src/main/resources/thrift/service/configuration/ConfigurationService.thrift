include '../../request/StringRequest.thrift'
include 'ConfigurationResponse.thrift'
include 'ConfigurationListResponse.thrift'

namespace java com.dsmaster.api.gateway.service.configuration

service ConfigurationService {
    ConfigurationListResponse.ConfigurationListResponse findByKey(1: StringRequest.StringRequest request);
    ConfigurationResponse.ConfigurationResponse findByExactKey(1: StringRequest.StringRequest request);
}