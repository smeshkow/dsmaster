include '../../response/Response.thrift'
include 'BuildBonuses.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

struct BuildBonusesResponse {
    1: Response.Response response,
    2: BuildBonuses.BuildBonuses buildBonuses
}