include '../../response/Response.thrift'
include 'Build.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

struct BuildResponse {
    1: Response.Response response,
    2: Build.Build build
}