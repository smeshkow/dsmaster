include 'BuildRequest.thrift'
include 'BuildResponse.thrift'
include 'ItemListRequest.thrift'
include 'ItemListResponse.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

service CalculatorService {

    BuildResponse.BuildResponse calculateBuildBonus(1: BuildRequest.BuildRequest request );

    ItemListResponse.ItemListResponse calculateItemListBonuses(1: ItemListRequest.ItemListRequest request );

}