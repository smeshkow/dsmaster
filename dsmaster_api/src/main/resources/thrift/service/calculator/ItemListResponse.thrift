include '../../response/Response.thrift'
include 'ItemList.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

struct ItemListResponse {
    1: Response.Response response,
    2: ItemList.ItemList itemList
}