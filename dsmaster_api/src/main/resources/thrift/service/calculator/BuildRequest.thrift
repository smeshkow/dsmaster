include '../../request/Request.thrift'
include 'Build.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

struct BuildRequest {
    1: Request.Request request,
    2: Build.Build build
}