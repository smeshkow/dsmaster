include '../../request/Request.thrift'
include 'ItemList.thrift'

namespace java com.dsmaster.api.gateway.service.calculator

struct ItemListRequest {
    1: Request.Request request,
    2: ItemList.ItemList itemList
}