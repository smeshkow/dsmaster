include '../../response/Response.thrift'
include 'Article.thrift'

namespace java com.dsmaster.api.gateway.service.article

struct ArticleListResponse {
    1: Response.Response response,
    2: optional list<Article.Article> articleList
}