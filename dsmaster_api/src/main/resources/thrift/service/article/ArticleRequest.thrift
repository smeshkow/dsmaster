include '../../request/Request.thrift'
include 'Article.thrift'

namespace java com.dsmaster.api.gateway.service.article

struct ArticleRequest {
    1: Request.Request request,
    2: Article.Article article
}