include '../../response/Response.thrift'
include 'Article.thrift'

namespace java com.dsmaster.api.gateway.service.article

struct ArticleResponse {
    1: Response.Response response,
    2: optional Article.Article article
}