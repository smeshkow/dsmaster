include '../../request/Request.thrift'
include 'ArticleType.thrift'

namespace java com.dsmaster.api.gateway.service.article

struct ArticleParamsRequest {
    1: Request.Request request,
    2: optional string name,
    3: optional set<string> names,
    4: optional set<ArticleType.ArticleType> types
}