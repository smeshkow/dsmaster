include '../../request/StringRequest.thrift'
include 'ArticleResponse.thrift'
include 'ArticleListResponse.thrift'
include 'ArticleRequest.thrift'
include 'ArticleParamsRequest.thrift'

namespace java com.dsmaster.api.gateway.service.article

service ArticleService {
    ArticleResponse.ArticleResponse create(1: ArticleRequest.ArticleRequest request);
    ArticleResponse.ArticleResponse update(1: ArticleRequest.ArticleRequest request);
    ArticleResponse.ArticleResponse save(1: ArticleRequest.ArticleRequest request);
    oneway void saveOneway(1: ArticleRequest.ArticleRequest request);
    ArticleListResponse.ArticleListResponse findByParams(1: ArticleParamsRequest.ArticleParamsRequest request);
}