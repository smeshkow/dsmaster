include 'BDecimal.thrift'

namespace java com.dsmaster.api.gateway.request

struct Request {
    1: string uuid,
    2: i32 fromUserId,
    3: BDecimal.BDecimal fromUserIP,
    4: i32 startFrom = 0,
    5: i32 count = 50
}