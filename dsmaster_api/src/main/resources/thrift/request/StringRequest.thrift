include 'Request.thrift'

namespace java com.dsmaster.api.gateway.request

struct StringRequest {
    1: Request.Request request,
    2: string value
}