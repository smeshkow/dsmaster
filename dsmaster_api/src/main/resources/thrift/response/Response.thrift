include 'StatusCode.thrift'

namespace java com.dsmaster.api.gateway.response

struct Response {
    1: StatusCode.StatusCode statusCode = StatusCode.StatusCode.SUCCESS,
    2: string errorMessage,
    3: string statusDetail
}