namespace java com.dsmaster.api.gateway.response

enum StatusCode {
    UNKNOWN_ERROR = 0,
	SUCCESS = 1,
	ILLEGAL_ARGUMENTS = 2,
	ILLEGAL_STATUS = 3,
	OBJECT_NOT_FOUND = 4,
	ALREADY_EXISTS = 5,
	NO_PRIVILEGES = 6
}