include 'UserKey.thrift'

namespace java com.dsmaster.api.util

struct UserKeyCache {
    1: string serverUuid,
    2: map<string, UserKey.UserKey> keyMap
}