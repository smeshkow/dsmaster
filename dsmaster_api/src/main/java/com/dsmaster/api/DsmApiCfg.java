package com.dsmaster.api;

import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.util.ExcHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

/**
 * Created by smeshkov on 7/18/14.
 */
public class DsmApiCfg {

    protected final Logger log =
            LoggerFactory.getLogger(getClass());

    private final String cfgName;
    private final ApiMode mode;
    private final String host;
    private final Integer port;

    public DsmApiCfg(String cfgName, ApiMode mode, String host, Integer port) {
        this.cfgName = cfgName;
        this.mode = mode;
        this.host = host;
        this.port = port;
    }

    public DsmApi initApi(DefaultListableBeanFactory beanFactory, String postfix, String packageStr) {
        if(StringUtils.isNotEmpty(cfgName)) {
            StringBuilder sb = new StringBuilder(packageStr);sb.append(".");sb.append(cfgName);sb.append(".");
            sb.append(Character.toUpperCase(cfgName.charAt(0)));sb.append(cfgName.substring(1));sb.append(postfix);
            String className = sb.toString();
            String beanName = cfgName + postfix;
            try {
                Class<?> clazz = Class.forName(className);
                DsmApi instance = (DsmApi) clazz.newInstance();
                instance.setHost(host);
                instance.setPort(port);
                instance.setMode(mode);
                beanFactory.registerSingleton(beanName, instance);
                beanFactory.autowireBean(instance);
                instance.start();
                log.debug("[{}] created", className);
                return instance;
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                ExcHandler.ex(e);
            }
        }
        return null;
    }



}
