package com.dsmaster.api;

import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.util.ExcHandler;

/**
 * Created by smeshkov on 7/18/14.
 */
public interface DsmApi {

    void start();
    String getHost();
    Integer getPort();
    ApiMode getMode();
    default void setHost(String host) { ExcHandler.throwNotSupported(); }
    default void setPort(Integer port) { ExcHandler.throwNotSupported(); }
    default void setMode(ApiMode mode) { ExcHandler.throwNotSupported(); }

}
