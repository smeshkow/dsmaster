package com.dsmaster.api.client.configuration;

import com.dsmaster.api.DsmApiCfg;
import com.dsmaster.api.client.DsmClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.request.StringRequest;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.configuration.ConfigurationListResponse;
import com.dsmaster.api.gateway.service.configuration.ConfigurationResponse;
import com.dsmaster.api.gateway.service.configuration.ConfigurationService;
import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.dto.configuration.Configuration;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by smeshkov on 7/18/14.
 */

public class ConfigurationClient extends DsmClient implements ConfigurationService.Iface {

    private static final String POSTFIX = "Client";

    // Configuration
    @Value("${api.cfg}")
    protected String apiCfg;
    @Value("${api.cfg.name}")
    protected String apiCfgName;
    @Value("${api.mode}")
    private String modeStr;
    @Value("${api.host.address}")
    private String host;
    @Value("${api.host.port}")
    private Integer port;

    @Autowired
    private ApplicationContext ctx;

    private ConfigurationService.Client client;
    private ApiMode mode;

    @PostConstruct
    public void init() {
        mode = ApiMode.forVal(modeStr) != null ? ApiMode.forVal(modeStr) : ApiMode.SIMPLE;
        try {
            ConfigurationListResponse response = findByKey(new StringRequest(new Request(), apiCfg));
            if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) ctx.getAutowireCapableBeanFactory();
                String packageStr = this.getClass().getPackage().getName();
                for (Configuration cfg : response.getConfigurationList()) {
                    String cfgName = cfg.getKey().replace(apiCfg + ".", "");
                    DsmApiCfg dsmApiCfg = new DsmApiCfg(cfgName, getMode(), cfg.getStrVal(), cfg.getIntVal());
                    dsmApiCfg.initApi(beanFactory, POSTFIX, packageStr.replace("." + apiCfgName, ""));
                }
            }
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void newClient() {
        client = new ConfigurationService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public ApiMode getMode() {
        return mode;
    }

    @Override
    public ConfigurationListResponse findByKey(StringRequest request) throws TException {
        return call((t) -> client.findByKey(t), request);
    }

    @Override
    public ConfigurationResponse findByExactKey(StringRequest request) throws TException {
        return call((t) -> client.findByExactKey(t), request);
    }

}
