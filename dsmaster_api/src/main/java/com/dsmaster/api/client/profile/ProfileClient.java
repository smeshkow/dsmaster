package com.dsmaster.api.client.profile;

import com.dsmaster.api.client.DsmApiClient;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.service.profile.AuthRequest;
import com.dsmaster.api.gateway.service.profile.ProfileService;
import com.dsmaster.api.gateway.service.profile.RegisterRqst;
import com.dsmaster.api.gateway.service.security.AuthResponse;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * Created by smeshkov on 7/28/14.
 */
public class ProfileClient extends DsmApiClient implements ProfileService.Iface {

    private ProfileService.Client client;

    @Override
    protected void newClient() {
        client = new ProfileService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public Response registerUser(RegisterRqst request) throws TException {
        return call((t) -> client.registerUser(t), request);
    }

    @Override
    public AuthResponse authUser(AuthRequest request) throws TException {
        return call((t) -> client.authUser(t), request);
    }
}
