package com.dsmaster.api.client;

import com.dsmaster.model.dto.common.ApiMode;
import com.google.common.base.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by smeshkov on 7/15/14.
 */
public abstract class DsmApiClient extends DsmClient {

    private ApiMode mode;
    private String host;
    private Integer port;

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public ApiMode getMode() {
        return mode;
    }

    @Override
    public void setMode(ApiMode mode) {
        this.mode = mode;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("super", super.toString())
                .add("mode", mode)
                .add("host", host)
                .add("port", port)
                .omitNullValues()
                .toString();
    }
}
