package com.dsmaster.api.client.security;

import com.dsmaster.api.client.DsmApiClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.article.*;
import com.dsmaster.api.gateway.service.security.*;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * Created by smeshkov on 8/4/14.
 */
public class SecurityClient extends DsmApiClient implements SecurityService.Iface {

    private SecurityService.Iface client;

    @Override
    protected void newClient() {
        client = new SecurityService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public UserKeyListResponse getAllKeys(GetUserKeyListRqstLvlA request) throws TException {
        return call((t) -> client.getAllKeys(t), request);
    }

    @Override
    public UserKeyListResponse getAllKeysForCache(Request request) throws TException {
        return new UserKeyListResponse(new Response(StatusCode.NO_PRIVILEGES, "", ""));
    }

    @Override
    public Response saveKey(UserKeyRqstLvlA request) throws TException {
        return call((t) -> client.saveKey(t), request);
    }

    @Override
    public void saveKeyOneway(UserKeyRqstLvlA request) throws TException {
        callOneway((t) -> client.saveKeyOneway(t), request);
    }

    @Override
    public UserKeyListResponse findByParams(UserKeyParamsRqstA request) throws TException {
        return call((t) -> client.findByParams(t), request);
    }
}
