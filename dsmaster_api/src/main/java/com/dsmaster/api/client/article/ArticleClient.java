package com.dsmaster.api.client.article;

import com.dsmaster.api.client.DsmApiClient;
import com.dsmaster.api.gateway.service.article.*;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * Created by smeshkov on 8/4/14.
 */
public class ArticleClient extends DsmApiClient implements ArticleService.Iface {

    private ArticleService.Iface client;

    @Override
    protected void newClient() {
        client = new ArticleService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public ArticleResponse create(ArticleRequest request) throws TException {
        return call((t) -> client.create(t), request);
    }

    @Override
    public ArticleResponse update(ArticleRequest request) throws TException {
        return call((t) -> client.update(t), request);
    }

    @Override
    public ArticleResponse save(ArticleRequest request) throws TException {
        return call((t) -> client.save(t), request);
    }

    @Override
    public void saveOneway(ArticleRequest request) throws TException {
        callOneway((t) -> client.saveOneway(t), request);
    }

    @Override
    public ArticleListResponse findByParams(ArticleParamsRequest request) throws TException {
        return call((t) -> client.findByParams(t), request);
    }
}
