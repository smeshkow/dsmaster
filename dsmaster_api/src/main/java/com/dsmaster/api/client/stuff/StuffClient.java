package com.dsmaster.api.client.stuff;

import com.dsmaster.api.client.DsmApiClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.request.StringRequest;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.service.stuff.SaveWeaponShieldRequest;
import com.dsmaster.api.gateway.service.stuff.StuffService;
import com.dsmaster.api.gateway.service.stuff.WeaponShieldListResponse;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * Created by smeshkov on 7/15/14.
 */
public class StuffClient extends DsmApiClient implements StuffService.Iface {

    private StuffService.Client client;

    @Override
    protected void newClient() {
        client = new StuffService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public WeaponShieldListResponse findWeaponShields(Request request) throws TException {
        return call((t) -> client.findWeaponShields(t), request);
    }

    @Override
    public Response saveWeaponShield(SaveWeaponShieldRequest request) throws TException {
        return call((t) -> client.saveWeaponShield(t), request);
    }

}
