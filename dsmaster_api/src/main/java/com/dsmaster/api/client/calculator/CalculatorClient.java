package com.dsmaster.api.client.calculator;

import com.dsmaster.api.client.DsmApiClient;
import com.dsmaster.api.gateway.service.calculator.*;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 10:01
 */
public class CalculatorClient extends DsmApiClient implements CalculatorService.Iface {

    private CalculatorService.Client client;

    @Override
    protected void newClient() {
        client = new CalculatorService.Client(new TBinaryProtocol(transport));
    }

    @Override
    public BuildResponse calculateBuildBonus(BuildRequest request) throws TException {
        return call((t) -> client.calculateBuildBonus(t), request);
    }

    @Override
    public ItemListResponse calculateItemListBonuses(ItemListRequest request) throws TException {
        return call((t) -> client.calculateItemListBonuses(t), request);
    }

}
