package com.dsmaster.api.client;

import com.dsmaster.api.DsmApi;
import com.dsmaster.model.dto.common.ApiMode;
import com.dsmaster.model.util.ExcHandler;
import com.dsmaster.model.util.ExceptionalConsumer;
import com.dsmaster.model.util.ExceptionalFunction;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * Created by smeshkov on 7/15/14.
 */
public abstract class DsmClient implements DsmApi {

    protected final Logger log =
            LoggerFactory.getLogger(getClass());

    protected TTransport transport;

    @PostConstruct
    @Override
    public void start() {
        if(StringUtils.isNotEmpty(getHost()) && getPort() != null) {
            connect();
            close();
        } else {
            String errMsg = String.format("Can't connect, host=[%s], port=[%d], mode=[%s]", getHost(), getPort(), getMode());
            log.error(errMsg);
            throw new IllegalArgumentException(errMsg);
        }
    }

    private void connect() {
        open();
        newClient();
    }
    private void open() {
        try {
            if(ApiMode.SECURE.equals(getMode())) {
                /*
                * Similar to the server, you can use the parameters to setup client parameters or
                * use the default settings. On the client side, you will need a TrustStore which
                * contains the trusted certificate along with the public key.
                * For this example it's a self-signed cert.
                */
                TSSLTransportFactory.TSSLTransportParameters params = new TSSLTransportFactory.TSSLTransportParameters();
                params.setTrustStore("../../lib/java/dsm/.truststore", "thrift", "SunX509", "JKS");
                /*
                * Get a client transport instead of a server transport. The connection is opened on
                * invocation of the factory method, no need to specifically call open()
                */
                transport = TSSLTransportFactory.getClientSocket(getHost(), getPort(), 0, params);
            } else {
                transport = new TSocket(getHost(), getPort());
                transport.open();
            }
        } catch (TException x) { ExcHandler.ex(x); }
    }

    private void close() {
        if(transport != null) transport.close();
    }

    protected abstract void newClient();

    protected <R, T> R call(ExceptionalFunction<R, T, TException> f, T s) throws TException {
        TException x = null;
        try {
            connect();
            return f.apply(s);
        } catch (TException e) {
            x = e;
        } finally {
            close();
            if(x != null) throw x;
        }
        return null;
    }

    protected <T> void callOneway(ExceptionalConsumer<T, TException> c, T s) throws TException {
        TException x = null;
        try {
            connect();
            c.accept(s);
        } catch (TException e) {
            x = e;
        } finally {
            close();
            if(x != null) throw x;
        }
    }
}
