package com.dsmaster.api.util;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by smeshkov on 04.09.14.
 */
public class PropertyPlaceholderCfg extends PropertyPlaceholderConfigurer {

    Properties mergedProperties;

    public Properties getMergedProperties() throws IOException {
        if (mergedProperties == null) {
            mergedProperties = mergeProperties();
        }
        return mergedProperties;

    }

}
