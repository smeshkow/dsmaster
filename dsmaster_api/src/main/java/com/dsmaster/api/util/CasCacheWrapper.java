package com.dsmaster.api.util;

import com.dsmaster.api.client.security.SecurityClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.StatusCode;
import com.dsmaster.api.gateway.service.security.GetUserKeyListRqstLvlA;
import com.dsmaster.api.gateway.service.security.UserKeyListResponse;
import com.dsmaster.model.dto.security.AccessLevel;
import com.dsmaster.model.dto.security.UserKey;
import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.ExcHandler;
import com.dsmaster.model.util.MemoryUtils;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by smeshkov on 02.09.14.
 */
@Deprecated
public class CasCacheWrapper {

    private static final Logger log = LoggerFactory.getLogger(CasCacheWrapper.class);
    private static final long WAIT_FOR = 5000L;

    private static CasCacheWrapper INSTANCE;

    private final AtomicReference<UserKeyCache> state;
    private final ExecutorService refresher = Executors.newSingleThreadExecutor();
    private final ExecutorService worker = Executors.newCachedThreadPool();

    private volatile boolean isReady = false; // cache was initialized at least once

    private final SecurityClient client;

    public CasCacheWrapper() {
        state = new AtomicReference<>();
        state.set(new UserKeyCache());
        client = AppCtxHolder.get().getBean(SecurityClient.class);
        INSTANCE = this;
    }

//------------------------------------------ INIT ---------------------------------------------------------------

    @PostConstruct
    public void init() {
        log.info("Initializing...");
        refresh();
    }

    @Scheduled(fixedDelay=1_800_000)
    public void rebuild() {
        refresh();
    }

//------------------------------------------ STATIC ---------------------------------------------------------------

    public static CasCacheWrapper get() {
        return INSTANCE;
    }

    // -------------------------------------------------- WRITE ------------------------------------------------------

    public void write(final UserKey store) {
        for(;;) {
            UserKeyCache currentState = getState();
            UserKeyCache newState = getState();
            newState.putToKeyMap(store.getUuid(), store);
            if(state.compareAndSet(currentState, newState)) {
                break;
            }
        }
    }

    public void writeAccess(final String uuid, final AccessLevel accessLevel) {
        for(;;) {
            UserKeyCache currentState = getState();
            UserKeyCache newState = getState();
            UserKey store = newState.getKeyMap().get(uuid);
            if(store == null) {
                log.error("Can't find UserKeyCache by UUID[{}]", uuid);
                break;
            }
            store.setAccessLevel(accessLevel);
            if(state.compareAndSet(currentState, newState)) {
                break;
            }
        }
    }

    // -------------------------------------------------- READ ------------------------------------------------------

    public UserKey read(String uuid) {
        return getState().getKeyMap().get(uuid);
    }

    public AccessLevel readAccess(String uuid) {
        UserKey store = read(uuid);
        if(store != null) { return store.getAccessLevel();
        } else { return null; }
    }

    // -------------------------------------------------- PRIVATE ------------------------------------------------------

    private void refresh() {
        refresher.execute(() -> {
            try {
                Stopwatch sw = Stopwatch.createStarted();
                while (true) {
                    log.info("Refreshing...");
                    UserKeyCache currentState = state.get();
                    UserKeyCache newState = new UserKeyCache();

                    String serverUuid = DsmGen.get().genUUID();
                    newState.setServerUuid(serverUuid);
                    log.debug("New server UUID[{}]", newState.getServerUuid());

                    UserKeyListResponse response = client.getAllKeys(new GetUserKeyListRqstLvlA(
                            new Request().setUuid(newState.getServerUuid())));
                    if(response.getResponse().getStatusCode() == StatusCode.SUCCESS) {
                        // TODO populate newState
                    }

                    if (state.compareAndSet(currentState, newState)) {
                        isReady = true; // TODO we don't need to set value to TRUE every time
                        sw.stop();
                        log.info("Refreshed...[{}]. Elapsed: {} {}",
                                MemoryUtils.getFullObjectSize(newState, MemoryUtils.Unit.BYTE),
                                sw.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
                        break;
                    }
                    log.info("One more time...");
                }
            } catch (Throwable t) { ExcHandler.ex(t); }
        });
    }

    private UserKeyCache getState() {
        if(!isReady) {
            Future<UserKeyCache> future = worker.submit(() -> {
                while (!isReady) TimeUnit.MILLISECONDS.sleep(WAIT_FOR);
                return state.get();
            });
            try { return future.get(); }
            catch (InterruptedException | ExecutionException e ) { ExcHandler.ex(e); }
        } else {
            return state.get();
        }
        return null;
    }

}
