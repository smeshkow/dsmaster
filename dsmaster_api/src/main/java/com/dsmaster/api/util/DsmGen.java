package com.dsmaster.api.util;

import com.dsmaster.model.util.AppCtxHolder;
import com.dsmaster.model.util.DsmUtils;
import com.dsmaster.model.util.ExcHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by smeshkov on 02.09.14.
 */
public class DsmGen {

    private static final Logger log = LoggerFactory.getLogger(DsmGen.class);

    public static final String DIGEST = "SHA-256";
    public static final String KEY_STORE_TYPE = "JKS";

    private final String keyStoreFile;
    private final String keyStorePwd;
    private final String salt;

    // Singleton
    private static class DsmGenHolder {
        private static final DsmGen HOLDER = new DsmGen();
    }
    public static DsmGen get() { return DsmGenHolder.HOLDER; }

    private DsmGen() {
        try {
            PropertyPlaceholderCfg ppCfg = AppCtxHolder.get().getBean(PropertyPlaceholderCfg.class);
            Properties properties = ppCfg.getMergedProperties();
            keyStoreFile = properties.getProperty("api.keystore.path");
            keyStorePwd = properties.getProperty("api.keystore.password");
            salt = properties.getProperty("api.uuid.gen.salt");
        } catch (IOException e) {
            ExcHandler.ex(e);
            throw new RuntimeException(e.getMessage());
        }
    }

    public KeyStore createKeyStore() {
        try {
            FileInputStream input = new FileInputStream(keyStoreFile);
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
            keyStore.load(input, keyStorePwd.toCharArray());
            input.close();
            return keyStore;
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            ExcHandler.ex(e);
            throw new RuntimeException(e.getMessage());
        }
    }

    public String genUUID() {
        return String.valueOf(UUID.randomUUID());
    }

    public String hashPwd(String base) {
        if(StringUtils.isEmpty(base)) {
            String errMsg = "hash base is empty!";
            log.error(errMsg);
            throw new RuntimeException(errMsg);
        }
        if(StringUtils.isEmpty(salt)) {
            String errMsg = "salt is empty!";
            log.error(errMsg);
            throw new RuntimeException(errMsg);
        }
        try {
            StringBuilder sb = new StringBuilder(base);
            sb.append(salt);
            MessageDigest md = MessageDigest.getInstance(DIGEST);
            byte[] bytes = md.digest(sb.toString().getBytes(DsmUtils.CHARSET));
            return hexEncode(bytes);
        } catch (NoSuchAlgorithmException e) {
            ExcHandler.ex(e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * The byte[] returned by MessageDigest does not have a nice
     * textual representation, so some form of encoding is usually performed.
     *
     * This implementation follows the example of David Flanagan's book
     * "Java In A Nutshell", and converts a byte array into a String
     * of hex characters.
     *
     * Another popular alternative is to use a "Base64" encoding.
     */
    private String hexEncode(byte[] aInput){
        StringBuilder result = new StringBuilder();
        char[] digits = {'0', '1', '2', '3', '4','5','6','7','8','9','a','b','c','d','e','f'};
        for (int idx = 0; idx < aInput.length; ++idx) {
            byte b = aInput[idx];
            result.append(digits[ (b&0xf0) >> 4 ]);
            result.append(digits[ b&0x0f]);
        }
        return result.toString();
    }

}
