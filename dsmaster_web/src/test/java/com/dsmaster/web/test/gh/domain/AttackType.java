
package com.dsmaster.web.test.gh.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Slug",
    "Name",
    "_id"
})
public class AttackType {

    @JsonProperty("Slug")
    private Integer slug;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("_id")
    private com.dsmaster.web.test.gh.domain._id _id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Slug")
    public Integer getSlug() {
        return slug;
    }

    @JsonProperty("Slug")
    public void setSlug(Integer slug) {
        this.slug = slug;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("_id")
    public com.dsmaster.web.test.gh.domain._id get_id() {
        return _id;
    }

    @JsonProperty("_id")
    public void set_id(com.dsmaster.web.test.gh.domain._id _id) {
        this._id = _id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("slug", slug)
                .add("name", name)
                .add("_id", _id)
                .add("additionalProperties", additionalProperties)
                .omitNullValues()
                .toString();
    }
}
