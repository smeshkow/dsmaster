package com.dsmaster.web.test;

import com.dsmaster.api.client.calculator.CalculatorClient;
import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.service.calculator.BuildRequest;
import com.dsmaster.api.gateway.service.calculator.BuildResponse;
import com.dsmaster.api.gateway.service.calculator.ItemListRequest;
import com.dsmaster.api.gateway.service.calculator.ItemListResponse;
import com.dsmaster.model.dto.profile.Build;
import com.dsmaster.model.dto.profile.ItemList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * User: artem_marinov
 * Date: 01.08.14
 * Time: 11:01
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/web-server.xml"
})
public class CalculatorTest {

    @Autowired
    private CalculatorClient calculatorClient;

    @Test
    public void testCalculateBuildBonuses() throws Exception {
        Build srcBuild = new Build();
        srcBuild.setVgr((short) 30)
                .setEndurance((short) 30)
                .setVit((short) 30)
                .setAtn((short) 30)
                .setStr((short) 20)
                .setDex((short) 30)
                .setAdp((short) 20)
                .setIntelligent((short) 40)
                .setFth((short) 50);


        Request source = new Request();
        BuildRequest request = new BuildRequest(source, srcBuild);
        BuildResponse buildResponse = calculatorClient.calculateBuildBonus(request);
        System.out.println(buildResponse);

        Build buildWithBonuses = buildResponse.getBuild();


        ItemList itemList = new ItemList();
        ItemListRequest itemListRequest = new ItemListRequest().setItemList(itemList);

        itemList.setBuild(buildWithBonuses);
        itemList.setLeft1Id(1);


        ItemListResponse itemListResponse = calculatorClient.calculateItemListBonuses(itemListRequest);
        System.out.println(itemListResponse);
    }
}
