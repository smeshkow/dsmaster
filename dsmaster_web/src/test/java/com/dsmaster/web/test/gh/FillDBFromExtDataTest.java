package com.dsmaster.web.test.gh;

import com.dsmaster.api.gateway.request.Request;
import com.dsmaster.api.gateway.response.Response;
import com.dsmaster.api.gateway.service.stuff.SaveWeaponShieldRequest;
import com.dsmaster.model.dto.stuff.Armor;
import com.dsmaster.api.client.stuff.StuffClient;
import com.dsmaster.web.test.TestEntityTransformer;
import com.dsmaster.web.test.gh.domain.BigGHRecord;
import com.dsmaster.model.dto.stuff.WeaponShield;
import com.dsmaster.model.util.Fns;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.thrift.TException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * User: artem_marinov
 * Date: 04.07.14
 * Time: 14:43
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring/web-server.xml"
})
public class FillDBFromExtDataTest {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private StuffClient stuffService;




    @Test
    public void testFillWeaponShields() {
        doWithGHRecords("gh-data/weapons", this::saveWeaponInfo);// saveWeaponInfo();
    }

   @Test
    public void testFillArmors() {
        doWithGHRecords("gh-data/armor/head", this::saveArmorsInfo);// saveWeaponInfo();
        doWithGHRecords("gh-data/armor/chest", this::saveArmorsInfo);// saveWeaponInfo();
        doWithGHRecords("gh-data/armor/hands", this::saveArmorsInfo);// saveWeaponInfo();
        doWithGHRecords("gh-data/armor/legs", this::saveArmorsInfo);// saveWeaponInfo();
    }

    private void saveWeaponInfo(BigGHRecord record) {
        WeaponShield weaponShield = Fns.mutate(() -> record, WeaponShield::new, TestEntityTransformer.recordToWeaponShieldTransformer);
        try {
            Response response = stuffService.saveWeaponShield(new SaveWeaponShieldRequest(new Request(), weaponShield));
            System.out.println("ok -" + response);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    private void saveArmorsInfo(BigGHRecord record) {
        Armor armor = Fns.mutate(() -> record, Armor::new, TestEntityTransformer.recordToArmorTransformer);

//        Object attributeBonusPhysicalDEF = record.getAttributes().getAttributeBonusPhysicalDEF();
//        if(attributeBonusPhysicalDEF != null) {
//            System.out.println("d-" + attributeBonusPhysicalDEF.toString());
//        }
//        throw new UnsupportedOperationException("unimplemented yet");
//        TODO
//        WeaponShield weaponShield = Fns.mutate(() -> record, WeaponShield::new, TestEntityTransformer.recordToWeaponShieldTransformer);
//        try {
//            Response response = stuffService.saveWeaponShield(new SaveWeaponShieldRequest(new Request(), weaponShield));
//            System.out.println("ok -" + response);
//        } catch (TException e) {
//            e.printStackTrace();

//        }

    }




    private void doWithGHRecords(String dir, Consumer<BigGHRecord> consumer) {
        Path path = Paths.get(dir);
        try(DirectoryStream<Path> filesStream = Files.newDirectoryStream(path, "*.json")) {
            Stream<Path> stream = StreamSupport.stream(filesStream.spliterator(), false)
                    .sorted((o1, o2) -> o1.getFileName().compareTo(o2.getFileName()));
            stream.map(this::getReaderSilent).map(this::convertJsonFromReaderToBigGHRecord).forEach(consumer);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private BufferedReader getReaderSilent(Path path) {
        try {
            return Files.newBufferedReader(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private BigGHRecord convertJsonFromReaderToBigGHRecord(BufferedReader reader) {
        try {
            return objectMapper.readValue(reader, BigGHRecord.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
