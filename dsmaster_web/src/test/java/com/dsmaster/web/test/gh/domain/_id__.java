
package com.dsmaster.web.test.gh.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Timestamp",
    "Machine",
    "Pid",
    "Increment",
    "CreationTime"
})
public class _id__ {

    @JsonProperty("Timestamp")
    private Integer timestamp;
    @JsonProperty("Machine")
    private Integer machine;
    @JsonProperty("Pid")
    private Integer pid;
    @JsonProperty("Increment")
    private Integer increment;
    @JsonProperty("CreationTime")
    private String creationTime;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Timestamp")
    public Integer getTimestamp() {
        return timestamp;
    }

    @JsonProperty("Timestamp")
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("Machine")
    public Integer getMachine() {
        return machine;
    }

    @JsonProperty("Machine")
    public void setMachine(Integer machine) {
        this.machine = machine;
    }

    @JsonProperty("Pid")
    public Integer getPid() {
        return pid;
    }

    @JsonProperty("Pid")
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    @JsonProperty("Increment")
    public Integer getIncrement() {
        return increment;
    }

    @JsonProperty("Increment")
    public void setIncrement(Integer increment) {
        this.increment = increment;
    }

    @JsonProperty("CreationTime")
    public String getCreationTime() {
        return creationTime;
    }

    @JsonProperty("CreationTime")
    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("timestamp", timestamp)
                .add("machine", machine)
                .add("pid", pid)
                .add("increment", increment)
                .add("creationTime", creationTime)
                .add("additionalProperties", additionalProperties)
                .omitNullValues()
                .toString();
    }
}
