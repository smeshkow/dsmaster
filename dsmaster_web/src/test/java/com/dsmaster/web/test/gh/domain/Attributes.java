
package com.dsmaster.web.test.gh.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "SoulLevel",
    "AttributePoints",
    "Souls",
    "RequiredSouls",
    "Vigor",
    "Endurance",
    "Vitality",
    "Attunement",
    "Strength",
    "Dexterity",
    "Intelligence",
    "Faith",
    "Adaptability",
    "Agility",
    "HP",
    "Stamina",
    "EquipmentLoad",
    "EquipmentLoadUsed",
    "EquipmentLoadUsedPercentage",
    "SpellAttunementSlots",
    "SpellcastingSpeed",
    "CastSpeed",
    "PhysicalATKStrength",
    "PhysicalATKDexterity",
    "MagicATK",
    "FireATK",
    "LightningATK",
    "DarkATK",
    "PoisonATK",
    "BleedATK",
    "BreakATK",
    "PhysicalDEF",
    "MagicDEF",
    "FireDEF",
    "LightningDEF",
    "DarkDEF",
    "PoisonResist",
    "BleedResist",
    "BreakResist",
    "PetrifyResist",
    "CurseResist",
    "Evasion",
    "ActionSpeed",
    "TrapDisabling",
    "LeftHandATK",
    "RightHandATK",
    "PhysicalDEFStrike",
    "PhysicalDEFSlash",
    "PhysicalDEFThrust",
    "Poise",
    "RequiredAttributeStrength",
    "RequiredAttributeDexterity",
    "RequiredAttributeIntelligence",
    "RequiredAttributeFaith",
    "AttributeBonusStrength",
    "AttributeBonusDexterity",
    "AttributeBonusIntelligence",
    "AttributeBonusFaith",
    "AttributeBonusResistance",
    "AttributeBonusPhysicalDEF",
    "Resistance",
    "SpellType",
    "SlotsUsed",
    "Slot",
    "Uses",
    "Name",
    "LoginName",
    "Class",
    "Covenant",
    "SinLevel",
    "WeaponCategory",
    "AttackType",
    "PhysicalATK",
    "CounterStrength",
    "PoiseDamage",
    "ShotRange",
    "PhysicalReduction",
    "MagicReduction",
    "LightningReduction",
    "FireReduction",
    "DarkReduction",
    "PoisonReduction",
    "BleedReduction",
    "BreakReduction",
    "CurseReduction",
    "PetrifyReduction",
    "Stability",
    "CurrentDurabilityMaximumDurability",
    "CurseDEF",
    "Weight",
    "HasWeightEffect",
    "RequiredStrength",
    "RequiredDexterity",
    "RequiredIntelligence",
    "RequiredFaith",
    "StrengthScaling",
    "DexterityScaling",
    "MagicScaling",
    "FireScaling",
    "LightningScaling",
    "DarkScaling",
    "StrengthScalingPercentage",
    "DexterityScalingPercentage",
    "MagicScalingPercentage",
    "FireScalingPercentage",
    "LightningScalingPercentage",
    "DarkScalingPercentage",
    "LeftWeapon1Attack",
    "LeftWeapon2Attack",
    "LeftWeapon3Attack",
    "RightWeapon1Attack",
    "RightWeapon2Attack",
    "RightWeapon3Attack",
    "LeftWeapon1AttackBonus",
    "LeftWeapon2AttackBonus",
    "LeftWeapon3AttackBonus",
    "RightWeapon1AttackBonus",
    "RightWeapon2AttackBonus",
    "RightWeapon3AttackBonus",
    "ArmorTier1Bonus",
    "ArmorTier2Bonus",
    "ArmorTier3Bonus",
    "ArmorTier4Bonus",
    "ArmorTier5Bonus",
    "_id"
})
public class Attributes {

    @JsonProperty("SoulLevel")
    private Object soulLevel;
    @JsonProperty("AttributePoints")
    private Object attributePoints;
    @JsonProperty("Souls")
    private Object souls;
    @JsonProperty("RequiredSouls")
    private Object requiredSouls;
    @JsonProperty("Vigor")
    private Object vigor;
    @JsonProperty("Endurance")
    private Object endurance;
    @JsonProperty("Vitality")
    private Object vitality;
    @JsonProperty("Attunement")
    private Object attunement;
    @JsonProperty("Strength")
    private Object strength;
    @JsonProperty("Dexterity")
    private Object dexterity;
    @JsonProperty("Intelligence")
    private Object intelligence;
    @JsonProperty("Faith")
    private Object faith;
    @JsonProperty("Adaptability")
    private Object adaptability;
    @JsonProperty("Agility")
    private Object agility;
    @JsonProperty("HP")
    private Object hP;
    @JsonProperty("Stamina")
    private Object stamina;
    @JsonProperty("EquipmentLoad")
    private Object equipmentLoad;
    @JsonProperty("EquipmentLoadUsed")
    private Object equipmentLoadUsed;
    @JsonProperty("EquipmentLoadUsedPercentage")
    private Object equipmentLoadUsedPercentage;
    @JsonProperty("SpellAttunementSlots")
    private Object spellAttunementSlots;
    @JsonProperty("SpellcastingSpeed")
    private Object spellcastingSpeed;
    @JsonProperty("CastSpeed")
    private Integer castSpeed;
    @JsonProperty("PhysicalATKStrength")
    private Object physicalATKStrength;
    @JsonProperty("PhysicalATKDexterity")
    private Object physicalATKDexterity;
    @JsonProperty("MagicATK")
    private Integer magicATK;
    @JsonProperty("FireATK")
    private Integer fireATK;
    @JsonProperty("LightningATK")
    private Integer lightningATK;
    @JsonProperty("DarkATK")
    private Integer darkATK;
    @JsonProperty("PoisonATK")
    private Integer poisonATK;
    @JsonProperty("BleedATK")
    private Integer bleedATK;
    @JsonProperty("BreakATK")
    private Object breakATK;
    @JsonProperty("PhysicalDEF")
    private Short physicalDEF;
    @JsonProperty("MagicDEF")
    private Short magicDEF;
    @JsonProperty("FireDEF")
    private Short fireDEF;
    @JsonProperty("LightningDEF")
    private Short lightningDEF;
    @JsonProperty("DarkDEF")
    private Short darkDEF;
    @JsonProperty("PoisonResist")
    private Short poisonResist;
    @JsonProperty("BleedResist")
    private Short bleedResist;
    @JsonProperty("BreakResist")
    private Short breakResist;
    @JsonProperty("PetrifyResist")
    private Short petrifyResist;
    @JsonProperty("CurseResist")
    private Short curseResist;
    @JsonProperty("Evasion")
    private Object evasion;
    @JsonProperty("ActionSpeed")
    private Object actionSpeed;
    @JsonProperty("TrapDisabling")
    private Object trapDisabling;
    @JsonProperty("LeftHandATK")
    private Object leftHandATK;
    @JsonProperty("RightHandATK")
    private Object rightHandATK;
    @JsonProperty("PhysicalDEFStrike")
    private Short physicalDEFStrike;
    @JsonProperty("PhysicalDEFSlash")
    private Short physicalDEFSlash;
    @JsonProperty("PhysicalDEFThrust")
    private Short physicalDEFThrust;
    @JsonProperty("Poise")
    private Short poise;
    @JsonProperty("RequiredAttributeStrength")
    private Integer requiredAttributeStrength;
    @JsonProperty("RequiredAttributeDexterity")
    private Integer requiredAttributeDexterity;
    @JsonProperty("RequiredAttributeIntelligence")
    private Integer requiredAttributeIntelligence;
    @JsonProperty("RequiredAttributeFaith")
    private Integer requiredAttributeFaith;
    @JsonProperty("AttributeBonusStrength")
    private Object attributeBonusStrength;
    @JsonProperty("AttributeBonusDexterity")
    private Object attributeBonusDexterity;
    @JsonProperty("AttributeBonusIntelligence")
    private Object attributeBonusIntelligence;
    @JsonProperty("AttributeBonusFaith")
    private Object attributeBonusFaith;
    @JsonProperty("AttributeBonusResistance")
    private Object attributeBonusResistance;
    @JsonProperty("AttributeBonusPhysicalDEF")
    private String attributeBonusPhysicalDEF;
    @JsonProperty("Resistance")
    private Object resistance;
    @JsonProperty("SpellType")
    private Object spellType;
    @JsonProperty("SlotsUsed")
    private Object slotsUsed;
    @JsonProperty("Slot")
    private Object slot;
    @JsonProperty("Uses")
    private Object uses;
    @JsonProperty("Name")
    private Object name;
    @JsonProperty("LoginName")
    private Object loginName;
    @JsonProperty("Class")
    private Object _class;
    @JsonProperty("Covenant")
    private Object covenant;
    @JsonProperty("SinLevel")
    private Object sinLevel;
    @JsonProperty("WeaponCategory")
    private Object weaponCategory;
    @JsonProperty("AttackType")
    private Object attackType;
    @JsonProperty("PhysicalATK")
    private Integer physicalATK;
    @JsonProperty("CounterStrength")
    private Integer counterStrength;
    @JsonProperty("PoiseDamage")
    private Integer poiseDamage;
    @JsonProperty("ShotRange")
    private Integer shotRange;
    @JsonProperty("PhysicalReduction")
    private Integer physicalReduction;
    @JsonProperty("MagicReduction")
    private Integer magicReduction;
    @JsonProperty("LightningReduction")
    private Integer lightningReduction;
    @JsonProperty("FireReduction")
    private Integer fireReduction;
    @JsonProperty("DarkReduction")
    private Integer darkReduction;
    @JsonProperty("PoisonReduction")
    private Integer poisonReduction;
    @JsonProperty("BleedReduction")
    private Integer bleedReduction;
    @JsonProperty("BreakReduction")
    private Object breakReduction;
    @JsonProperty("CurseReduction")
    private Integer curseReduction;
    @JsonProperty("PetrifyReduction")
    private Integer petrifyReduction;
    @JsonProperty("Stability")
    private Integer stability;
    @JsonProperty("CurrentDurabilityMaximumDurability")
    private String currentDurabilityMaximumDurability;
    @JsonProperty("CurseDEF")
    private Object curseDEF;
    @JsonProperty("Weight")
    private Double weight;
    @JsonProperty("HasWeightEffect")
    private Object hasWeightEffect;
    @JsonProperty("RequiredStrength")
    private Object requiredStrength;
    @JsonProperty("RequiredDexterity")
    private Object requiredDexterity;
    @JsonProperty("RequiredIntelligence")
    private Object requiredIntelligence;
    @JsonProperty("RequiredFaith")
    private Object requiredFaith;
    @JsonProperty("StrengthScaling")
    private String strengthScaling;
    @JsonProperty("DexterityScaling")
    private String dexterityScaling;
    @JsonProperty("MagicScaling")
    private String magicScaling;
    @JsonProperty("FireScaling")
    private String fireScaling;
    @JsonProperty("LightningScaling")
    private String lightningScaling;
    @JsonProperty("DarkScaling")
    private String darkScaling;
    @JsonProperty("StrengthScalingPercentage")
    private Double strengthScalingPercentage;
    @JsonProperty("DexterityScalingPercentage")
    private Double dexterityScalingPercentage;
    @JsonProperty("MagicScalingPercentage")
    private Double magicScalingPercentage;
    @JsonProperty("FireScalingPercentage")
    private Double fireScalingPercentage;
    @JsonProperty("LightningScalingPercentage")
    private Double lightningScalingPercentage;
    @JsonProperty("DarkScalingPercentage")
    private Double darkScalingPercentage;
    @JsonProperty("LeftWeapon1Attack")
    private Object leftWeapon1Attack;
    @JsonProperty("LeftWeapon2Attack")
    private Object leftWeapon2Attack;
    @JsonProperty("LeftWeapon3Attack")
    private Object leftWeapon3Attack;
    @JsonProperty("RightWeapon1Attack")
    private Object rightWeapon1Attack;
    @JsonProperty("RightWeapon2Attack")
    private Object rightWeapon2Attack;
    @JsonProperty("RightWeapon3Attack")
    private Object rightWeapon3Attack;
    @JsonProperty("LeftWeapon1AttackBonus")
    private Object leftWeapon1AttackBonus;
    @JsonProperty("LeftWeapon2AttackBonus")
    private Object leftWeapon2AttackBonus;
    @JsonProperty("LeftWeapon3AttackBonus")
    private Object leftWeapon3AttackBonus;
    @JsonProperty("RightWeapon1AttackBonus")
    private Object rightWeapon1AttackBonus;
    @JsonProperty("RightWeapon2AttackBonus")
    private Object rightWeapon2AttackBonus;
    @JsonProperty("RightWeapon3AttackBonus")
    private Object rightWeapon3AttackBonus;
    @JsonProperty("ArmorTier1Bonus")
    private Object armorTier1Bonus;
    @JsonProperty("ArmorTier2Bonus")
    private Object armorTier2Bonus;
    @JsonProperty("ArmorTier3Bonus")
    private Object armorTier3Bonus;
    @JsonProperty("ArmorTier4Bonus")
    private Object armorTier4Bonus;
    @JsonProperty("ArmorTier5Bonus")
    private Object armorTier5Bonus;
    @JsonProperty("_id")
    private _id__ _id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SoulLevel")
    public Object getSoulLevel() {
        return soulLevel;
    }

    @JsonProperty("SoulLevel")
    public void setSoulLevel(Object soulLevel) {
        this.soulLevel = soulLevel;
    }

    @JsonProperty("AttributePoints")
    public Object getAttributePoints() {
        return attributePoints;
    }

    @JsonProperty("AttributePoints")
    public void setAttributePoints(Object attributePoints) {
        this.attributePoints = attributePoints;
    }

    @JsonProperty("Souls")
    public Object getSouls() {
        return souls;
    }

    @JsonProperty("Souls")
    public void setSouls(Object souls) {
        this.souls = souls;
    }

    @JsonProperty("RequiredSouls")
    public Object getRequiredSouls() {
        return requiredSouls;
    }

    @JsonProperty("RequiredSouls")
    public void setRequiredSouls(Object requiredSouls) {
        this.requiredSouls = requiredSouls;
    }

    @JsonProperty("Vigor")
    public Object getVigor() {
        return vigor;
    }

    @JsonProperty("Vigor")
    public void setVigor(Object vigor) {
        this.vigor = vigor;
    }

    @JsonProperty("Endurance")
    public Object getEndurance() {
        return endurance;
    }

    @JsonProperty("Endurance")
    public void setEndurance(Object endurance) {
        this.endurance = endurance;
    }

    @JsonProperty("Vitality")
    public Object getVitality() {
        return vitality;
    }

    @JsonProperty("Vitality")
    public void setVitality(Object vitality) {
        this.vitality = vitality;
    }

    @JsonProperty("Attunement")
    public Object getAttunement() {
        return attunement;
    }

    @JsonProperty("Attunement")
    public void setAttunement(Object attunement) {
        this.attunement = attunement;
    }

    @JsonProperty("Strength")
    public Object getStrength() {
        return strength;
    }

    @JsonProperty("Strength")
    public void setStrength(Object strength) {
        this.strength = strength;
    }

    @JsonProperty("Dexterity")
    public Object getDexterity() {
        return dexterity;
    }

    @JsonProperty("Dexterity")
    public void setDexterity(Object dexterity) {
        this.dexterity = dexterity;
    }

    @JsonProperty("Intelligence")
    public Object getIntelligence() {
        return intelligence;
    }

    @JsonProperty("Intelligence")
    public void setIntelligence(Object intelligence) {
        this.intelligence = intelligence;
    }

    @JsonProperty("Faith")
    public Object getFaith() {
        return faith;
    }

    @JsonProperty("Faith")
    public void setFaith(Object faith) {
        this.faith = faith;
    }

    @JsonProperty("Adaptability")
    public Object getAdaptability() {
        return adaptability;
    }

    @JsonProperty("Adaptability")
    public void setAdaptability(Object adaptability) {
        this.adaptability = adaptability;
    }

    @JsonProperty("Agility")
    public Object getAgility() {
        return agility;
    }

    @JsonProperty("Agility")
    public void setAgility(Object agility) {
        this.agility = agility;
    }

    @JsonProperty("HP")
    public Object getHP() {
        return hP;
    }

    @JsonProperty("HP")
    public void setHP(Object hP) {
        this.hP = hP;
    }

    @JsonProperty("Stamina")
    public Object getStamina() {
        return stamina;
    }

    @JsonProperty("Stamina")
    public void setStamina(Object stamina) {
        this.stamina = stamina;
    }

    @JsonProperty("EquipmentLoad")
    public Object getEquipmentLoad() {
        return equipmentLoad;
    }

    @JsonProperty("EquipmentLoad")
    public void setEquipmentLoad(Object equipmentLoad) {
        this.equipmentLoad = equipmentLoad;
    }

    @JsonProperty("EquipmentLoadUsed")
    public Object getEquipmentLoadUsed() {
        return equipmentLoadUsed;
    }

    @JsonProperty("EquipmentLoadUsed")
    public void setEquipmentLoadUsed(Object equipmentLoadUsed) {
        this.equipmentLoadUsed = equipmentLoadUsed;
    }

    @JsonProperty("EquipmentLoadUsedPercentage")
    public Object getEquipmentLoadUsedPercentage() {
        return equipmentLoadUsedPercentage;
    }

    @JsonProperty("EquipmentLoadUsedPercentage")
    public void setEquipmentLoadUsedPercentage(Object equipmentLoadUsedPercentage) {
        this.equipmentLoadUsedPercentage = equipmentLoadUsedPercentage;
    }

    @JsonProperty("SpellAttunementSlots")
    public Object getSpellAttunementSlots() {
        return spellAttunementSlots;
    }

    @JsonProperty("SpellAttunementSlots")
    public void setSpellAttunementSlots(Object spellAttunementSlots) {
        this.spellAttunementSlots = spellAttunementSlots;
    }

    @JsonProperty("SpellcastingSpeed")
    public Object getSpellcastingSpeed() {
        return spellcastingSpeed;
    }

    @JsonProperty("SpellcastingSpeed")
    public void setSpellcastingSpeed(Object spellcastingSpeed) {
        this.spellcastingSpeed = spellcastingSpeed;
    }

    @JsonProperty("CastSpeed")
    public Integer getCastSpeed() {
        return castSpeed;
    }

    @JsonProperty("CastSpeed")
    public void setCastSpeed(Integer castSpeed) {
        this.castSpeed = castSpeed;
    }

    @JsonProperty("PhysicalATKStrength")
    public Object getPhysicalATKStrength() {
        return physicalATKStrength;
    }

    @JsonProperty("PhysicalATKStrength")
    public void setPhysicalATKStrength(Object physicalATKStrength) {
        this.physicalATKStrength = physicalATKStrength;
    }

    @JsonProperty("PhysicalATKDexterity")
    public Object getPhysicalATKDexterity() {
        return physicalATKDexterity;
    }

    @JsonProperty("PhysicalATKDexterity")
    public void setPhysicalATKDexterity(Object physicalATKDexterity) {
        this.physicalATKDexterity = physicalATKDexterity;
    }

    @JsonProperty("MagicATK")
    public Integer getMagicATK() {
        return magicATK;
    }

    @JsonProperty("MagicATK")
    public void setMagicATK(Integer magicATK) {
        this.magicATK = magicATK;
    }

    @JsonProperty("FireATK")
    public Integer getFireATK() {
        return fireATK;
    }

    @JsonProperty("FireATK")
    public void setFireATK(Integer fireATK) {
        this.fireATK = fireATK;
    }

    @JsonProperty("LightningATK")
    public Integer getLightningATK() {
        return lightningATK;
    }

    @JsonProperty("LightningATK")
    public void setLightningATK(Integer lightningATK) {
        this.lightningATK = lightningATK;
    }

    @JsonProperty("DarkATK")
    public Integer getDarkATK() {
        return darkATK;
    }

    @JsonProperty("DarkATK")
    public void setDarkATK(Integer darkATK) {
        this.darkATK = darkATK;
    }

    @JsonProperty("PoisonATK")
    public Integer getPoisonATK() {
        return poisonATK;
    }

    @JsonProperty("PoisonATK")
    public void setPoisonATK(Integer poisonATK) {
        this.poisonATK = poisonATK;
    }

    @JsonProperty("BleedATK")
    public Integer getBleedATK() {
        return bleedATK;
    }

    @JsonProperty("BleedATK")
    public void setBleedATK(Integer bleedATK) {
        this.bleedATK = bleedATK;
    }

    @JsonProperty("BreakATK")
    public Object getBreakATK() {
        return breakATK;
    }

    @JsonProperty("BreakATK")
    public void setBreakATK(Object breakATK) {
        this.breakATK = breakATK;
    }

    @JsonProperty("PhysicalDEF")
    public Short getPhysicalDEF() {
        return physicalDEF;
    }

    @JsonProperty("PhysicalDEF")
    public void setPhysicalDEF(Short physicalDEF) {
        this.physicalDEF = physicalDEF;
    }

    @JsonProperty("MagicDEF")
    public Short getMagicDEF() {
        return magicDEF;
    }

    @JsonProperty("MagicDEF")
    public void setMagicDEF(Short magicDEF) {
        this.magicDEF = magicDEF;
    }

    @JsonProperty("FireDEF")
    public Short getFireDEF() {
        return fireDEF;
    }

    @JsonProperty("FireDEF")
    public void setFireDEF(Short fireDEF) {
        this.fireDEF = fireDEF;
    }

    @JsonProperty("LightningDEF")
    public Short getLightningDEF() {
        return lightningDEF;
    }

    @JsonProperty("LightningDEF")
    public void setLightningDEF(Short lightningDEF) {
        this.lightningDEF = lightningDEF;
    }

    @JsonProperty("DarkDEF")
    public Short getDarkDEF() {
        return darkDEF;
    }

    @JsonProperty("DarkDEF")
    public void setDarkDEF(Short darkDEF) {
        this.darkDEF = darkDEF;
    }

    @JsonProperty("PoisonResist")
    public Short getPoisonResist() {
        return poisonResist;
    }

    @JsonProperty("PoisonResist")
    public void setPoisonResist(Short poisonResist) {
        this.poisonResist = poisonResist;
    }

    @JsonProperty("BleedResist")
    public Short getBleedResist() {
        return bleedResist;
    }

    @JsonProperty("BleedResist")
    public void setBleedResist(Short bleedResist) {
        this.bleedResist = bleedResist;
    }

    @JsonProperty("BreakResist")
    public Short getBreakResist() {
        return breakResist;
    }

    @JsonProperty("BreakResist")
    public void setBreakResist(Short breakResist) {
        this.breakResist = breakResist;
    }

    @JsonProperty("PetrifyResist")
    public Short getPetrifyResist() {
        return petrifyResist;
    }

    @JsonProperty("PetrifyResist")
    public void setPetrifyResist(Short petrifyResist) {
        this.petrifyResist = petrifyResist;
    }

    @JsonProperty("CurseResist")
    public Short getCurseResist() {
        return curseResist;
    }

    @JsonProperty("CurseResist")
    public void setCurseResist(Short curseResist) {
        this.curseResist = curseResist;
    }

    @JsonProperty("Evasion")
    public Object getEvasion() {
        return evasion;
    }

    @JsonProperty("Evasion")
    public void setEvasion(Object evasion) {
        this.evasion = evasion;
    }

    @JsonProperty("ActionSpeed")
    public Object getActionSpeed() {
        return actionSpeed;
    }

    @JsonProperty("ActionSpeed")
    public void setActionSpeed(Object actionSpeed) {
        this.actionSpeed = actionSpeed;
    }

    @JsonProperty("TrapDisabling")
    public Object getTrapDisabling() {
        return trapDisabling;
    }

    @JsonProperty("TrapDisabling")
    public void setTrapDisabling(Object trapDisabling) {
        this.trapDisabling = trapDisabling;
    }

    @JsonProperty("LeftHandATK")
    public Object getLeftHandATK() {
        return leftHandATK;
    }

    @JsonProperty("LeftHandATK")
    public void setLeftHandATK(Object leftHandATK) {
        this.leftHandATK = leftHandATK;
    }

    @JsonProperty("RightHandATK")
    public Object getRightHandATK() {
        return rightHandATK;
    }

    @JsonProperty("RightHandATK")
    public void setRightHandATK(Object rightHandATK) {
        this.rightHandATK = rightHandATK;
    }

    @JsonProperty("PhysicalDEFStrike")
    public Short getPhysicalDEFStrike() {
        return physicalDEFStrike;
    }

    @JsonProperty("PhysicalDEFStrike")
    public void setPhysicalDEFStrike(Short physicalDEFStrike) {
        this.physicalDEFStrike = physicalDEFStrike;
    }

    @JsonProperty("PhysicalDEFSlash")
    public Short getPhysicalDEFSlash() {
        return physicalDEFSlash;
    }

    @JsonProperty("PhysicalDEFSlash")
    public void setPhysicalDEFSlash(Short physicalDEFSlash) {
        this.physicalDEFSlash = physicalDEFSlash;
    }

    @JsonProperty("PhysicalDEFThrust")
    public Short getPhysicalDEFThrust() {
        return physicalDEFThrust;
    }

    @JsonProperty("PhysicalDEFThrust")
    public void setPhysicalDEFThrust(Short physicalDEFThrust) {
        this.physicalDEFThrust = physicalDEFThrust;
    }

    @JsonProperty("Poise")
    public Short getPoise() {
        return poise;
    }

    @JsonProperty("Poise")
    public void setPoise(Short poise) {
        this.poise = poise;
    }

    @JsonProperty("RequiredAttributeStrength")
    public Integer getRequiredAttributeStrength() {
        return requiredAttributeStrength;
    }

    @JsonProperty("RequiredAttributeStrength")
    public void setRequiredAttributeStrength(Integer requiredAttributeStrength) {
        this.requiredAttributeStrength = requiredAttributeStrength;
    }

    @JsonProperty("RequiredAttributeDexterity")
    public Integer getRequiredAttributeDexterity() {
        return requiredAttributeDexterity;
    }

    @JsonProperty("RequiredAttributeDexterity")
    public void setRequiredAttributeDexterity(Integer requiredAttributeDexterity) {
        this.requiredAttributeDexterity = requiredAttributeDexterity;
    }

    @JsonProperty("RequiredAttributeIntelligence")
    public Integer getRequiredAttributeIntelligence() {
        return requiredAttributeIntelligence;
    }

    @JsonProperty("RequiredAttributeIntelligence")
    public void setRequiredAttributeIntelligence(Integer requiredAttributeIntelligence) {
        this.requiredAttributeIntelligence = requiredAttributeIntelligence;
    }

    @JsonProperty("RequiredAttributeFaith")
    public Integer getRequiredAttributeFaith() {
        return requiredAttributeFaith;
    }

    @JsonProperty("RequiredAttributeFaith")
    public void setRequiredAttributeFaith(Integer requiredAttributeFaith) {
        this.requiredAttributeFaith = requiredAttributeFaith;
    }

    @JsonProperty("AttributeBonusStrength")
    public Object getAttributeBonusStrength() {
        return attributeBonusStrength;
    }

    @JsonProperty("AttributeBonusStrength")
    public void setAttributeBonusStrength(Object attributeBonusStrength) {
        this.attributeBonusStrength = attributeBonusStrength;
    }

    @JsonProperty("AttributeBonusDexterity")
    public Object getAttributeBonusDexterity() {
        return attributeBonusDexterity;
    }

    @JsonProperty("AttributeBonusDexterity")
    public void setAttributeBonusDexterity(Object attributeBonusDexterity) {
        this.attributeBonusDexterity = attributeBonusDexterity;
    }

    @JsonProperty("AttributeBonusIntelligence")
    public Object getAttributeBonusIntelligence() {
        return attributeBonusIntelligence;
    }

    @JsonProperty("AttributeBonusIntelligence")
    public void setAttributeBonusIntelligence(Object attributeBonusIntelligence) {
        this.attributeBonusIntelligence = attributeBonusIntelligence;
    }

    @JsonProperty("AttributeBonusFaith")
    public Object getAttributeBonusFaith() {
        return attributeBonusFaith;
    }

    @JsonProperty("AttributeBonusFaith")
    public void setAttributeBonusFaith(Object attributeBonusFaith) {
        this.attributeBonusFaith = attributeBonusFaith;
    }

    @JsonProperty("AttributeBonusResistance")
    public Object getAttributeBonusResistance() {
        return attributeBonusResistance;
    }

    @JsonProperty("AttributeBonusResistance")
    public void setAttributeBonusResistance(Object attributeBonusResistance) {
        this.attributeBonusResistance = attributeBonusResistance;
    }

    @JsonProperty("AttributeBonusPhysicalDEF")
    public String getAttributeBonusPhysicalDEF() {
        return attributeBonusPhysicalDEF;
    }

    @JsonProperty("AttributeBonusPhysicalDEF")
    public void setAttributeBonusPhysicalDEF(String attributeBonusPhysicalDEF) {
        this.attributeBonusPhysicalDEF = attributeBonusPhysicalDEF;
    }

    @JsonProperty("Resistance")
    public Object getResistance() {
        return resistance;
    }

    @JsonProperty("Resistance")
    public void setResistance(Object resistance) {
        this.resistance = resistance;
    }

    @JsonProperty("SpellType")
    public Object getSpellType() {
        return spellType;
    }

    @JsonProperty("SpellType")
    public void setSpellType(Object spellType) {
        this.spellType = spellType;
    }

    @JsonProperty("SlotsUsed")
    public Object getSlotsUsed() {
        return slotsUsed;
    }

    @JsonProperty("SlotsUsed")
    public void setSlotsUsed(Object slotsUsed) {
        this.slotsUsed = slotsUsed;
    }

    @JsonProperty("Slot")
    public Object getSlot() {
        return slot;
    }

    @JsonProperty("Slot")
    public void setSlot(Object slot) {
        this.slot = slot;
    }

    @JsonProperty("Uses")
    public Object getUses() {
        return uses;
    }

    @JsonProperty("Uses")
    public void setUses(Object uses) {
        this.uses = uses;
    }

    @JsonProperty("Name")
    public Object getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(Object name) {
        this.name = name;
    }

    @JsonProperty("LoginName")
    public Object getLoginName() {
        return loginName;
    }

    @JsonProperty("LoginName")
    public void setLoginName(Object loginName) {
        this.loginName = loginName;
    }

    @JsonProperty("Class")
    public Object getClass_() {
        return _class;
    }

    @JsonProperty("Class")
    public void setClass_(Object _class) {
        this._class = _class;
    }

    @JsonProperty("Covenant")
    public Object getCovenant() {
        return covenant;
    }

    @JsonProperty("Covenant")
    public void setCovenant(Object covenant) {
        this.covenant = covenant;
    }

    @JsonProperty("SinLevel")
    public Object getSinLevel() {
        return sinLevel;
    }

    @JsonProperty("SinLevel")
    public void setSinLevel(Object sinLevel) {
        this.sinLevel = sinLevel;
    }

    @JsonProperty("WeaponCategory")
    public Object getWeaponCategory() {
        return weaponCategory;
    }

    @JsonProperty("WeaponCategory")
    public void setWeaponCategory(Object weaponCategory) {
        this.weaponCategory = weaponCategory;
    }

    @JsonProperty("AttackType")
    public Object getAttackType() {
        return attackType;
    }

    @JsonProperty("AttackType")
    public void setAttackType(Object attackType) {
        this.attackType = attackType;
    }

    @JsonProperty("PhysicalATK")
    public Integer getPhysicalATK() {
        return physicalATK;
    }

    @JsonProperty("PhysicalATK")
    public void setPhysicalATK(Integer physicalATK) {
        this.physicalATK = physicalATK;
    }

    @JsonProperty("CounterStrength")
    public Integer getCounterStrength() {
        return counterStrength;
    }

    @JsonProperty("CounterStrength")
    public void setCounterStrength(Integer counterStrength) {
        this.counterStrength = counterStrength;
    }

    @JsonProperty("PoiseDamage")
    public Integer getPoiseDamage() {
        return poiseDamage;
    }

    @JsonProperty("PoiseDamage")
    public void setPoiseDamage(Integer poiseDamage) {
        this.poiseDamage = poiseDamage;
    }

    @JsonProperty("ShotRange")
    public Integer getShotRange() {
        return shotRange;
    }

    @JsonProperty("ShotRange")
    public void setShotRange(Integer shotRange) {
        this.shotRange = shotRange;
    }

    @JsonProperty("PhysicalReduction")
    public Integer getPhysicalReduction() {
        return physicalReduction;
    }

    @JsonProperty("PhysicalReduction")
    public void setPhysicalReduction(Integer physicalReduction) {
        this.physicalReduction = physicalReduction;
    }

    @JsonProperty("MagicReduction")
    public Integer getMagicReduction() {
        return magicReduction;
    }

    @JsonProperty("MagicReduction")
    public void setMagicReduction(Integer magicReduction) {
        this.magicReduction = magicReduction;
    }

    @JsonProperty("LightningReduction")
    public Integer getLightningReduction() {
        return lightningReduction;
    }

    @JsonProperty("LightningReduction")
    public void setLightningReduction(Integer lightningReduction) {
        this.lightningReduction = lightningReduction;
    }

    @JsonProperty("FireReduction")
    public Integer getFireReduction() {
        return fireReduction;
    }

    @JsonProperty("FireReduction")
    public void setFireReduction(Integer fireReduction) {
        this.fireReduction = fireReduction;
    }

    @JsonProperty("DarkReduction")
    public Integer getDarkReduction() {
        return darkReduction;
    }

    @JsonProperty("DarkReduction")
    public void setDarkReduction(Integer darkReduction) {
        this.darkReduction = darkReduction;
    }

    @JsonProperty("PoisonReduction")
    public Integer getPoisonReduction() {
        return poisonReduction;
    }

    @JsonProperty("PoisonReduction")
    public void setPoisonReduction(Integer poisonReduction) {
        this.poisonReduction = poisonReduction;
    }

    @JsonProperty("BleedReduction")
    public Integer getBleedReduction() {
        return bleedReduction;
    }

    @JsonProperty("BleedReduction")
    public void setBleedReduction(Integer bleedReduction) {
        this.bleedReduction = bleedReduction;
    }

    @JsonProperty("BreakReduction")
    public Object getBreakReduction() {
        return breakReduction;
    }

    @JsonProperty("BreakReduction")
    public void setBreakReduction(Object breakReduction) {
        this.breakReduction = breakReduction;
    }

    @JsonProperty("CurseReduction")
    public Integer getCurseReduction() {
        return curseReduction;
    }

    @JsonProperty("CurseReduction")
    public void setCurseReduction(Integer curseReduction) {
        this.curseReduction = curseReduction;
    }

    @JsonProperty("PetrifyReduction")
    public Integer getPetrifyReduction() {
        return petrifyReduction;
    }

    @JsonProperty("PetrifyReduction")
    public void setPetrifyReduction(Integer petrifyReduction) {
        this.petrifyReduction = petrifyReduction;
    }

    @JsonProperty("Stability")
    public Integer getStability() {
        return stability;
    }

    @JsonProperty("Stability")
    public void setStability(Integer stability) {
        this.stability = stability;
    }

    @JsonProperty("CurrentDurabilityMaximumDurability")
    public String getCurrentDurabilityMaximumDurability() {
        return currentDurabilityMaximumDurability;
    }

    @JsonProperty("CurrentDurabilityMaximumDurability")
    public void setCurrentDurabilityMaximumDurability(String currentDurabilityMaximumDurability) {
        this.currentDurabilityMaximumDurability = currentDurabilityMaximumDurability;
    }

    @JsonProperty("CurseDEF")
    public Object getCurseDEF() {
        return curseDEF;
    }

    @JsonProperty("CurseDEF")
    public void setCurseDEF(Object curseDEF) {
        this.curseDEF = curseDEF;
    }

    @JsonProperty("Weight")
    public Double getWeight() {
        return weight;
    }

    @JsonProperty("Weight")
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @JsonProperty("HasWeightEffect")
    public Object getHasWeightEffect() {
        return hasWeightEffect;
    }

    @JsonProperty("HasWeightEffect")
    public void setHasWeightEffect(Object hasWeightEffect) {
        this.hasWeightEffect = hasWeightEffect;
    }

    @JsonProperty("RequiredStrength")
    public Object getRequiredStrength() {
        return requiredStrength;
    }

    @JsonProperty("RequiredStrength")
    public void setRequiredStrength(Object requiredStrength) {
        this.requiredStrength = requiredStrength;
    }

    @JsonProperty("RequiredDexterity")
    public Object getRequiredDexterity() {
        return requiredDexterity;
    }

    @JsonProperty("RequiredDexterity")
    public void setRequiredDexterity(Object requiredDexterity) {
        this.requiredDexterity = requiredDexterity;
    }

    @JsonProperty("RequiredIntelligence")
    public Object getRequiredIntelligence() {
        return requiredIntelligence;
    }

    @JsonProperty("RequiredIntelligence")
    public void setRequiredIntelligence(Object requiredIntelligence) {
        this.requiredIntelligence = requiredIntelligence;
    }

    @JsonProperty("RequiredFaith")
    public Object getRequiredFaith() {
        return requiredFaith;
    }

    @JsonProperty("RequiredFaith")
    public void setRequiredFaith(Object requiredFaith) {
        this.requiredFaith = requiredFaith;
    }

    @JsonProperty("StrengthScaling")
    public String getStrengthScaling() {
        return strengthScaling;
    }

    @JsonProperty("StrengthScaling")
    public void setStrengthScaling(String strengthScaling) {
        this.strengthScaling = strengthScaling;
    }

    @JsonProperty("DexterityScaling")
    public String getDexterityScaling() {
        return dexterityScaling;
    }

    @JsonProperty("DexterityScaling")
    public void setDexterityScaling(String dexterityScaling) {
        this.dexterityScaling = dexterityScaling;
    }

    @JsonProperty("MagicScaling")
    public String getMagicScaling() {
        return magicScaling;
    }

    @JsonProperty("MagicScaling")
    public void setMagicScaling(String magicScaling) {
        this.magicScaling = magicScaling;
    }

    @JsonProperty("FireScaling")
    public String getFireScaling() {
        return fireScaling;
    }

    @JsonProperty("FireScaling")
    public void setFireScaling(String fireScaling) {
        this.fireScaling = fireScaling;
    }

    @JsonProperty("LightningScaling")
    public String getLightningScaling() {
        return lightningScaling;
    }

    @JsonProperty("LightningScaling")
    public void setLightningScaling(String lightningScaling) {
        this.lightningScaling = lightningScaling;
    }

    @JsonProperty("DarkScaling")
    public String getDarkScaling() {
        return darkScaling;
    }

    @JsonProperty("DarkScaling")
    public void setDarkScaling(String darkScaling) {
        this.darkScaling = darkScaling;
    }

    @JsonProperty("StrengthScalingPercentage")
    public Double getStrengthScalingPercentage() {
        return strengthScalingPercentage;
    }

    @JsonProperty("StrengthScalingPercentage")
    public void setStrengthScalingPercentage(Double strengthScalingPercentage) {
        this.strengthScalingPercentage = strengthScalingPercentage;
    }

    @JsonProperty("DexterityScalingPercentage")
    public Double getDexterityScalingPercentage() {
        return dexterityScalingPercentage;
    }

    @JsonProperty("DexterityScalingPercentage")
    public void setDexterityScalingPercentage(Double dexterityScalingPercentage) {
        this.dexterityScalingPercentage = dexterityScalingPercentage;
    }

    @JsonProperty("MagicScalingPercentage")
    public Double getMagicScalingPercentage() {
        return magicScalingPercentage;
    }

    @JsonProperty("MagicScalingPercentage")
    public void setMagicScalingPercentage(Double magicScalingPercentage) {
        this.magicScalingPercentage = magicScalingPercentage;
    }

    @JsonProperty("FireScalingPercentage")
    public Double getFireScalingPercentage() {
        return fireScalingPercentage;
    }

    @JsonProperty("FireScalingPercentage")
    public void setFireScalingPercentage(Double fireScalingPercentage) {
        this.fireScalingPercentage = fireScalingPercentage;
    }

    @JsonProperty("LightningScalingPercentage")
    public Double getLightningScalingPercentage() {
        return lightningScalingPercentage;
    }

    @JsonProperty("LightningScalingPercentage")
    public void setLightningScalingPercentage(Double lightningScalingPercentage) {
        this.lightningScalingPercentage = lightningScalingPercentage;
    }

    @JsonProperty("DarkScalingPercentage")
    public Double getDarkScalingPercentage() {
        return darkScalingPercentage;
    }

    @JsonProperty("DarkScalingPercentage")
    public void setDarkScalingPercentage(Double darkScalingPercentage) {
        this.darkScalingPercentage = darkScalingPercentage;
    }

    @JsonProperty("LeftWeapon1Attack")
    public Object getLeftWeapon1Attack() {
        return leftWeapon1Attack;
    }

    @JsonProperty("LeftWeapon1Attack")
    public void setLeftWeapon1Attack(Object leftWeapon1Attack) {
        this.leftWeapon1Attack = leftWeapon1Attack;
    }

    @JsonProperty("LeftWeapon2Attack")
    public Object getLeftWeapon2Attack() {
        return leftWeapon2Attack;
    }

    @JsonProperty("LeftWeapon2Attack")
    public void setLeftWeapon2Attack(Object leftWeapon2Attack) {
        this.leftWeapon2Attack = leftWeapon2Attack;
    }

    @JsonProperty("LeftWeapon3Attack")
    public Object getLeftWeapon3Attack() {
        return leftWeapon3Attack;
    }

    @JsonProperty("LeftWeapon3Attack")
    public void setLeftWeapon3Attack(Object leftWeapon3Attack) {
        this.leftWeapon3Attack = leftWeapon3Attack;
    }

    @JsonProperty("RightWeapon1Attack")
    public Object getRightWeapon1Attack() {
        return rightWeapon1Attack;
    }

    @JsonProperty("RightWeapon1Attack")
    public void setRightWeapon1Attack(Object rightWeapon1Attack) {
        this.rightWeapon1Attack = rightWeapon1Attack;
    }

    @JsonProperty("RightWeapon2Attack")
    public Object getRightWeapon2Attack() {
        return rightWeapon2Attack;
    }

    @JsonProperty("RightWeapon2Attack")
    public void setRightWeapon2Attack(Object rightWeapon2Attack) {
        this.rightWeapon2Attack = rightWeapon2Attack;
    }

    @JsonProperty("RightWeapon3Attack")
    public Object getRightWeapon3Attack() {
        return rightWeapon3Attack;
    }

    @JsonProperty("RightWeapon3Attack")
    public void setRightWeapon3Attack(Object rightWeapon3Attack) {
        this.rightWeapon3Attack = rightWeapon3Attack;
    }

    @JsonProperty("LeftWeapon1AttackBonus")
    public Object getLeftWeapon1AttackBonus() {
        return leftWeapon1AttackBonus;
    }

    @JsonProperty("LeftWeapon1AttackBonus")
    public void setLeftWeapon1AttackBonus(Object leftWeapon1AttackBonus) {
        this.leftWeapon1AttackBonus = leftWeapon1AttackBonus;
    }

    @JsonProperty("LeftWeapon2AttackBonus")
    public Object getLeftWeapon2AttackBonus() {
        return leftWeapon2AttackBonus;
    }

    @JsonProperty("LeftWeapon2AttackBonus")
    public void setLeftWeapon2AttackBonus(Object leftWeapon2AttackBonus) {
        this.leftWeapon2AttackBonus = leftWeapon2AttackBonus;
    }

    @JsonProperty("LeftWeapon3AttackBonus")
    public Object getLeftWeapon3AttackBonus() {
        return leftWeapon3AttackBonus;
    }

    @JsonProperty("LeftWeapon3AttackBonus")
    public void setLeftWeapon3AttackBonus(Object leftWeapon3AttackBonus) {
        this.leftWeapon3AttackBonus = leftWeapon3AttackBonus;
    }

    @JsonProperty("RightWeapon1AttackBonus")
    public Object getRightWeapon1AttackBonus() {
        return rightWeapon1AttackBonus;
    }

    @JsonProperty("RightWeapon1AttackBonus")
    public void setRightWeapon1AttackBonus(Object rightWeapon1AttackBonus) {
        this.rightWeapon1AttackBonus = rightWeapon1AttackBonus;
    }

    @JsonProperty("RightWeapon2AttackBonus")
    public Object getRightWeapon2AttackBonus() {
        return rightWeapon2AttackBonus;
    }

    @JsonProperty("RightWeapon2AttackBonus")
    public void setRightWeapon2AttackBonus(Object rightWeapon2AttackBonus) {
        this.rightWeapon2AttackBonus = rightWeapon2AttackBonus;
    }

    @JsonProperty("RightWeapon3AttackBonus")
    public Object getRightWeapon3AttackBonus() {
        return rightWeapon3AttackBonus;
    }

    @JsonProperty("RightWeapon3AttackBonus")
    public void setRightWeapon3AttackBonus(Object rightWeapon3AttackBonus) {
        this.rightWeapon3AttackBonus = rightWeapon3AttackBonus;
    }

    @JsonProperty("ArmorTier1Bonus")
    public Object getArmorTier1Bonus() {
        return armorTier1Bonus;
    }

    @JsonProperty("ArmorTier1Bonus")
    public void setArmorTier1Bonus(Object armorTier1Bonus) {
        this.armorTier1Bonus = armorTier1Bonus;
    }

    @JsonProperty("ArmorTier2Bonus")
    public Object getArmorTier2Bonus() {
        return armorTier2Bonus;
    }

    @JsonProperty("ArmorTier2Bonus")
    public void setArmorTier2Bonus(Object armorTier2Bonus) {
        this.armorTier2Bonus = armorTier2Bonus;
    }

    @JsonProperty("ArmorTier3Bonus")
    public Object getArmorTier3Bonus() {
        return armorTier3Bonus;
    }

    @JsonProperty("ArmorTier3Bonus")
    public void setArmorTier3Bonus(Object armorTier3Bonus) {
        this.armorTier3Bonus = armorTier3Bonus;
    }

    @JsonProperty("ArmorTier4Bonus")
    public Object getArmorTier4Bonus() {
        return armorTier4Bonus;
    }

    @JsonProperty("ArmorTier4Bonus")
    public void setArmorTier4Bonus(Object armorTier4Bonus) {
        this.armorTier4Bonus = armorTier4Bonus;
    }

    @JsonProperty("ArmorTier5Bonus")
    public Object getArmorTier5Bonus() {
        return armorTier5Bonus;
    }

    @JsonProperty("ArmorTier5Bonus")
    public void setArmorTier5Bonus(Object armorTier5Bonus) {
        this.armorTier5Bonus = armorTier5Bonus;
    }

    @JsonProperty("_id")
    public _id__ get_id() {
        return _id;
    }

    @JsonProperty("_id")
    public void set_id(_id__ _id) {
        this._id = _id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("soulLevel", soulLevel)
                .add("attributePoints", attributePoints)
                .add("souls", souls)
                .add("requiredSouls", requiredSouls)
                .add("vigor", vigor)
                .add("endurance", endurance)
                .add("vitality", vitality)
                .add("attunement", attunement)
                .add("strength", strength)
                .add("dexterity", dexterity)
                .add("intelligence", intelligence)
                .add("faith", faith)
                .add("adaptability", adaptability)
                .add("agility", agility)
                .add("hP", hP)
                .add("stamina", stamina)
                .add("equipmentLoad", equipmentLoad)
                .add("equipmentLoadUsed", equipmentLoadUsed)
                .add("equipmentLoadUsedPercentage", equipmentLoadUsedPercentage)
                .add("spellAttunementSlots", spellAttunementSlots)
                .add("spellcastingSpeed", spellcastingSpeed)
                .add("castSpeed", castSpeed)
                .add("physicalATKStrength", physicalATKStrength)
                .add("physicalATKDexterity", physicalATKDexterity)
                .add("magicATK", magicATK)
                .add("fireATK", fireATK)
                .add("lightningATK", lightningATK)
                .add("darkATK", darkATK)
                .add("poisonATK", poisonATK)
                .add("bleedATK", bleedATK)
                .add("breakATK", breakATK)
                .add("physicalDEF", physicalDEF)
                .add("magicDEF", magicDEF)
                .add("fireDEF", fireDEF)
                .add("lightningDEF", lightningDEF)
                .add("darkDEF", darkDEF)
                .add("poisonResist", poisonResist)
                .add("bleedResist", bleedResist)
                .add("breakResist", breakResist)
                .add("petrifyResist", petrifyResist)
                .add("curseResist", curseResist)
                .add("evasion", evasion)
                .add("actionSpeed", actionSpeed)
                .add("trapDisabling", trapDisabling)
                .add("leftHandATK", leftHandATK)
                .add("rightHandATK", rightHandATK)
                .add("physicalDEFStrike", physicalDEFStrike)
                .add("physicalDEFSlash", physicalDEFSlash)
                .add("physicalDEFThrust", physicalDEFThrust)
                .add("poise", poise)
                .add("requiredAttributeStrength", requiredAttributeStrength)
                .add("requiredAttributeDexterity", requiredAttributeDexterity)
                .add("requiredAttributeIntelligence", requiredAttributeIntelligence)
                .add("requiredAttributeFaith", requiredAttributeFaith)
                .add("attributeBonusStrength", attributeBonusStrength)
                .add("attributeBonusDexterity", attributeBonusDexterity)
                .add("attributeBonusIntelligence", attributeBonusIntelligence)
                .add("attributeBonusFaith", attributeBonusFaith)
                .add("attributeBonusResistance", attributeBonusResistance)
                .add("attributeBonusPhysicalDEF", attributeBonusPhysicalDEF)
                .add("resistance", resistance)
                .add("spellType", spellType)
                .add("slotsUsed", slotsUsed)
                .add("slot", slot)
                .add("uses", uses)
                .add("name", name)
                .add("loginName", loginName)
                .add("_class", _class)
                .add("covenant", covenant)
                .add("sinLevel", sinLevel)
                .add("weaponCategory", weaponCategory)
                .add("attackType", attackType)
                .add("physicalATK", physicalATK)
                .add("counterStrength", counterStrength)
                .add("poiseDamage", poiseDamage)
                .add("shotRange", shotRange)
                .add("physicalReduction", physicalReduction)
                .add("magicReduction", magicReduction)
                .add("lightningReduction", lightningReduction)
                .add("fireReduction", fireReduction)
                .add("darkReduction", darkReduction)
                .add("poisonReduction", poisonReduction)
                .add("bleedReduction", bleedReduction)
                .add("breakReduction", breakReduction)
                .add("curseReduction", curseReduction)
                .add("petrifyReduction", petrifyReduction)
                .add("stability", stability)
                .add("currentDurabilityMaximumDurability", currentDurabilityMaximumDurability)
                .add("curseDEF", curseDEF)
                .add("weight", weight)
                .add("hasWeightEffect", hasWeightEffect)
                .add("requiredStrength", requiredStrength)
                .add("requiredDexterity", requiredDexterity)
                .add("requiredIntelligence", requiredIntelligence)
                .add("requiredFaith", requiredFaith)
                .add("strengthScaling", strengthScaling)
                .add("dexterityScaling", dexterityScaling)
                .add("magicScaling", magicScaling)
                .add("fireScaling", fireScaling)
                .add("lightningScaling", lightningScaling)
                .add("darkScaling", darkScaling)
                .add("strengthScalingPercentage", strengthScalingPercentage)
                .add("dexterityScalingPercentage", dexterityScalingPercentage)
                .add("magicScalingPercentage", magicScalingPercentage)
                .add("fireScalingPercentage", fireScalingPercentage)
                .add("lightningScalingPercentage", lightningScalingPercentage)
                .add("darkScalingPercentage", darkScalingPercentage)
                .add("leftWeapon1Attack", leftWeapon1Attack)
                .add("leftWeapon2Attack", leftWeapon2Attack)
                .add("leftWeapon3Attack", leftWeapon3Attack)
                .add("rightWeapon1Attack", rightWeapon1Attack)
                .add("rightWeapon2Attack", rightWeapon2Attack)
                .add("rightWeapon3Attack", rightWeapon3Attack)
                .add("leftWeapon1AttackBonus", leftWeapon1AttackBonus)
                .add("leftWeapon2AttackBonus", leftWeapon2AttackBonus)
                .add("leftWeapon3AttackBonus", leftWeapon3AttackBonus)
                .add("rightWeapon1AttackBonus", rightWeapon1AttackBonus)
                .add("rightWeapon2AttackBonus", rightWeapon2AttackBonus)
                .add("rightWeapon3AttackBonus", rightWeapon3AttackBonus)
                .add("armorTier1Bonus", armorTier1Bonus)
                .add("armorTier2Bonus", armorTier2Bonus)
                .add("armorTier3Bonus", armorTier3Bonus)
                .add("armorTier4Bonus", armorTier4Bonus)
                .add("armorTier5Bonus", armorTier5Bonus)
                .add("_id", _id)
                .add("additionalProperties", additionalProperties)
                .omitNullValues()
                .toString();
    }
}
