package com.dsmaster.web.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: artem_marinov
 * Date: 29.07.14
 * Time: 12:09
 */
public class LoggerTester {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testLogger() throws Exception {
        System.out.println("sout");
        logger.error("LOGGER");
    }

}
