
package com.dsmaster.web.test.gh.domain;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Slug",
    "Name",
    "ShortName",
    "ThumbUrl",
    "StartingClass",
    "DsVersion",
    "IsFinished",
    "AttackType",
    "WeaponCategory",
    "Attributes",
    "Upgrades",
    "_id"
})
public class BigGHRecord {

    @JsonProperty("Slug")
    private Integer slug;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("ShortName")
    private String shortName;
    @JsonProperty("ThumbUrl")
    private Object thumbUrl;
    @JsonProperty("StartingClass")
    private Object startingClass;
    @JsonProperty("DsVersion")
    private Object dsVersion;
    @JsonProperty("IsFinished")
    private Boolean isFinished;
    @JsonProperty("AttackType")
    private AttackType attackType;
    @JsonProperty("WeaponCategory")
    private WeaponCategory weaponCategory;
    @JsonProperty("Attributes")
    private Attributes attributes;
    @JsonProperty("Upgrades")
    private Object upgrades;
    @JsonProperty("_id")
    private _id___ _id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Slug")
    public Integer getSlug() {
        return slug;
    }

    @JsonProperty("Slug")
    public void setSlug(Integer slug) {
        this.slug = slug;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("ShortName")
    public String getShortName() {
        return shortName;
    }

    @JsonProperty("ShortName")
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonProperty("ThumbUrl")
    public Object getThumbUrl() {
        return thumbUrl;
    }

    @JsonProperty("ThumbUrl")
    public void setThumbUrl(Object thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    @JsonProperty("StartingClass")
    public Object getStartingClass() {
        return startingClass;
    }

    @JsonProperty("StartingClass")
    public void setStartingClass(Object startingClass) {
        this.startingClass = startingClass;
    }

    @JsonProperty("DsVersion")
    public Object getDsVersion() {
        return dsVersion;
    }

    @JsonProperty("DsVersion")
    public void setDsVersion(Object dsVersion) {
        this.dsVersion = dsVersion;
    }

    @JsonProperty("IsFinished")
    public Boolean getIsFinished() {
        return isFinished;
    }

    @JsonProperty("IsFinished")
    public void setIsFinished(Boolean isFinished) {
        this.isFinished = isFinished;
    }

    @JsonProperty("AttackType")
    public AttackType getAttackType() {
        return attackType;
    }

    @JsonProperty("AttackType")
    public void setAttackType(AttackType attackType) {
        this.attackType = attackType;
    }

    @JsonProperty("WeaponCategory")
    public WeaponCategory getWeaponCategory() {
        return weaponCategory;
    }

    @JsonProperty("WeaponCategory")
    public void setWeaponCategory(WeaponCategory weaponCategory) {
        this.weaponCategory = weaponCategory;
    }

    @JsonProperty("Attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("Attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("Upgrades")
    public Object getUpgrades() {
        return upgrades;
    }

    @JsonProperty("Upgrades")
    public void setUpgrades(Object upgrades) {
        this.upgrades = upgrades;
    }

    @JsonProperty("_id")
    public _id___ get_id() {
        return _id;
    }

    @JsonProperty("_id")
    public void set_id(_id___ _id) {
        this._id = _id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("slug", slug)
                .add("name", name)
                .add("shortName", shortName)
                .add("thumbUrl", thumbUrl)
                .add("startingClass", startingClass)
                .add("dsVersion", dsVersion)
                .add("isFinished", isFinished)
                .add("attackType", attackType)
                .add("weaponCategory", weaponCategory)
                .add("attributes", attributes)
                .add("upgrades", upgrades)
                .add("_id", _id)
                .add("additionalProperties", additionalProperties)
                .omitNullValues()
                .toString();
    }
}
