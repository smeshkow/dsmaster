package com.dsmaster.web.test;

import com.dsmaster.model.dto.stuff.*;
import com.dsmaster.model.dto.stuff.attributes.*;
import com.dsmaster.web.test.gh.domain.Attributes;
import com.dsmaster.web.test.gh.domain.BigGHRecord;
import com.dsmaster.model.dto.common.Addon;
import com.dsmaster.model.util.Fns;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * User: artem_marinov
 * Date: 15.07.14
 * Time: 11:33
 */
public class TestEntityTransformer {

    private static final Function<String, Scaling> toScaling = (str) -> (str == null || str.equals("-")) ? null : Scaling.valueOf(str.trim());

    public static final BiFunction<Attributes, AttributesBonus, AttributesBonus> attributesBonusTransformer = (f,t) -> {
//        Fns.setShortIfNotNull(t::setBonusStr, f::getAttributeBonusStrength);
//        Fns.setShortIfNotNull(t::setBonusDex, f::getAttributeBonusDexterity);
//        Fns.setShortIfNotNull(t::setBonusPhysicalDef, f::getAttributeBonusPhysicalDEF);
//        Fns.setShortIfNotNull(t::setBonusInt, f::getAttributeBonusIntelligence);
//        Fns.setShortIfNotNull(t::setBonusFaith, f::getAttributeBonusFaith);
        Fns.doIfNotNull(System.out::println, f::getAttributeBonusStrength);
        Fns.doIfNotNull(System.out::println, f::getAttributeBonusDexterity);
//        Fns.doIfNotNull(System.out::println, f::getAttributeBonusPhysicalDEF);
        Fns.doIfNotNull(System.out::println, f::getAttributeBonusIntelligence);
        Fns.doIfNotNull(System.out::println, f::getAttributeBonusFaith);
//        Fns.doIfNotNull(System.out::println, f::getAttributeBonusResistance);
        return t;
    };

    public static final BiFunction<Attributes, AttributesRequirement, AttributesRequirement> attributesRequirementTransformer = (f,t) -> {
        Fns.setShortIfNotNull(t::setRequiredStr, f::getRequiredAttributeStrength, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setRequiredDex, f::getRequiredAttributeDexterity, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setRequiredInt, f::getRequiredAttributeIntelligence, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setRequiredFaith, f::getRequiredAttributeFaith, i -> i.shortValue());
        return t;
    };



    public static final BiFunction<Attributes, WeaponShieldAttributes, WeaponShieldAttributes> weaponShieldAttributesTransformer = (f,t) -> {

        AttackAttribute attackAttribute = new AttackAttribute();
        t.setAttackAttribute(attackAttribute);

        Fns.setShortIfNotNull(attackAttribute::setPhysicalAtk, f::getPhysicalATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setMagicAtk, f::getMagicATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setFireAtk, f::getFireATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setLightningAtk, f::getLightningATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setDarkAtk, f::getDarkATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setPoisonAtk, f::getPoisonATK, i -> i.shortValue());
        Fns.setShortIfNotNull(attackAttribute::setBleedAtk, f::getBleedATK, i -> i.shortValue());

        Fns.setShortIfNotNull(t::setPhysicalReduction, f::getPhysicalReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setMagicReduction, f::getMagicReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setFireReduction, f::getFireReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setLightningReduction, f::getLightningReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setDarkReduction, f::getDarkReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setPoisonReduction, f::getPoisonReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setBleedReduction, f::getBleedReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setCurseReduction, f::getCurseReduction, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setPetrifyReduction, f::getPetrifyReduction, i -> i.shortValue());

        Fns.setShortIfNotNull(t::setCounterStrength, f::getCounterStrength, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setStability, f::getStability, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setPoiseDamage, f::getPoiseDamage, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setStability, f::getStability, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setShortRange, f::getShotRange, i -> i.shortValue());
        Fns.setShortIfNotNull(t::setCastSpeed, f::getCastSpeed, i -> i.shortValue());

        Fns.setShortIfNotNull(t::setDurability, f::getCurrentDurabilityMaximumDurability, i -> Short.valueOf(i));
        Fns.setDoubleIfNotNull(t::setWeight, f::getWeight);

        return t;
    };

    public static final BiFunction<Attributes, ArmorAttributes, ArmorAttributes> armorAttributesTransformer = (f,t) -> {
        Fns.setShortIfNotNull(t::setPhysicalDef, f::getPhysicalDEF);
        Fns.setShortIfNotNull(t::setMagicDef, f::getMagicDEF);
        Fns.setShortIfNotNull(t::setFireDef, f::getFireDEF);
        Fns.setShortIfNotNull(t::setLightningDef, f::getLightningDEF);
        Fns.setShortIfNotNull(t::setDarkDef, f::getDarkDEF);

        Fns.setShortIfNotNull(t::setPoisonResist, f::getPoisonResist);
        Fns.setShortIfNotNull(t::setBleedResist, f::getBleedResist);
        Fns.setShortIfNotNull(t::setPetrifyResist, f::getPetrifyResist);
        Fns.setShortIfNotNull(t::setCurseResist, f::getCurseResist);

        Fns.setShortIfNotNull(t::setPhysicalDefStrike, f::getPhysicalDEFStrike);
        Fns.setShortIfNotNull(t::setPhysicalDefSlash, f::getPhysicalDEFSlash);
        Fns.setShortIfNotNull(t::setPhysicalDefThrust, f::getPhysicalDEFThrust);

        Fns.setShortIfNotNull(t::setPoise, f::getPoise);

        Fns.setShortIfNotNull(t::setDurability, f::getCurrentDurabilityMaximumDurability, i -> Short.valueOf(i));
        Fns.setDoubleIfNotNull(t::setWeight, f::getWeight);

        return t;
    };


    public static final BiFunction<BigGHRecord, WeaponShield, WeaponShield> recordToWeaponShieldTransformer = (f, t) -> {

        WeaponCategory weaponCategory = null;
        if(f.getWeaponCategory() != null) {
            String weaponCategoryName = f.getWeaponCategory().getName();
            if(f.getWeaponCategory().getAdditionalProperties().size() > 0) {
                System.out.println(f.getWeaponCategory().getAdditionalProperties());
            }
            weaponCategoryName = weaponCategoryName
                    .toUpperCase()
                    .replace(' ', '_');
            weaponCategoryName = StringUtils.replace(weaponCategoryName, "(", "");
            weaponCategoryName = StringUtils.replace(weaponCategoryName, ")", "");
            weaponCategory = WeaponCategory.valueOf(weaponCategoryName);
        }
        if(weaponCategory == null) {
            System.out.println("!! weapon categoty is null " + f);
        }

        AttackType attackType1 = null;
        AttackType attackType2 = null;

        if(f.getAttackType() != null) {
            String attackTypeName = f.getAttackType().getName();
            if(f.getAttackType().getAdditionalProperties().size() > 0) {
                System.out.println(f.getAttackType().getAdditionalProperties());
            }
            Iterable<String> attackTypeNames = Splitter.on('/').split(attackTypeName.toUpperCase().replace(' ', '_'));
            List<String> attackTypeNamesList = Lists.newArrayList(attackTypeNames);
            if(attackTypeNamesList.size() == 0) {
                System.out.println("attackTypeNamesList == 0 " + f);
            }
            attackType1 = AttackType.valueOf(attackTypeNamesList.get(0));
            if(attackTypeNamesList.size() > 1) {
                attackType2 = AttackType.valueOf(attackTypeNamesList.get(1));
            }
        }



//        if(attackType1 == null || attackType2 == null) {
//            System.out.println("some attack type is null + "+f);
//        }
        Attributes attributes = f.getAttributes();
        WeaponShieldAttributes weaponShieldAttributes = Fns.mutate(() -> attributes, WeaponShieldAttributes::new, weaponShieldAttributesTransformer);
        t.setAddon(Addon.BASE)
                .setName(f.getName())
                .setDescription(null)
                .setWeaponCategory(weaponCategory)
                .setAttackType1(attackType1)
                .setAttackType2(attackType2)
                .setStrScaling(toScaling.apply(attributes.getStrengthScaling()))
                .setDexScaling(toScaling.apply(attributes.getDexterityScaling()))
                .setMagicScaling(toScaling.apply(attributes.getMagicScaling()))
                .setFireScaling(toScaling.apply(attributes.getFireScaling()))
                .setLightningScaling(toScaling.apply(attributes.getLightningScaling()))
                .setDarkScaling(toScaling.apply(attributes.getDarkScaling()))
                .setWeaponShieldAttributes(weaponShieldAttributes)
        ;

        Fns.setDoubleIfNotNull(t::setStrScalingPercent, attributes::getStrengthScalingPercentage);
        Fns.setDoubleIfNotNull(t::setDexScalingPercent, attributes::getDexterityScalingPercentage);
        Fns.setDoubleIfNotNull(t::setMagicScalingPercent, attributes::getMagicScalingPercentage);
        Fns.setDoubleIfNotNull(t::setFireScalingPercent, attributes::getFireScalingPercentage);
        Fns.setDoubleIfNotNull(t::setLightningScalingPercent, attributes::getLightningScalingPercentage);
        Fns.setDoubleIfNotNull(t::setDarkScalingPercent, attributes::getDarkScalingPercentage);

        AttributesRequirement attributesRequirement = Fns.mutate(() -> attributes, AttributesRequirement::new, attributesRequirementTransformer);
        t.setAttributesRequirement(attributesRequirement);
        AttributesBonus attributesBonus = Fns.mutate(() -> attributes, AttributesBonus::new, attributesBonusTransformer);
        t.setAttributesBonus(attributesBonus);

        return t;
    };

    public static final BiFunction<BigGHRecord, Armor, Armor> recordToArmorTransformer = (f, t) -> {
        Attributes attributes = f.getAttributes();
        t.setAddon(Addon.BASE)
                .setName(f.getName())
                .setDescription(null)
                .setScaling(toScaling.apply(attributes.getAttributeBonusPhysicalDEF()))
                ;

        String slotName = (String) ((Map) f.getAdditionalProperties().get("ArmorCategory")).get("Name");
        switch (slotName) {
            case "Legs":
                t.setArmorType(ArmorType.LEGS);
                break;
            case "Arms":
                t.setArmorType(ArmorType.HANDS);
                break;
            case "Chest":
                t.setArmorType(ArmorType.CHEST);
                break;
            case "Head":
                t.setArmorType(ArmorType.HEAD);
                break;

        }

        ArmorAttributes armorAttributes = Fns.mutate(() -> attributes, ArmorAttributes::new, armorAttributesTransformer);
        t.setArmorAttributes(armorAttributes);
        AttributesRequirement attributesRequirement = Fns.mutate(() -> attributes, AttributesRequirement::new, attributesRequirementTransformer);
        t.setAttributesRequirement(attributesRequirement);
        AttributesBonus attributesBonus = Fns.mutate(() -> attributes, AttributesBonus::new, attributesBonusTransformer);
        t.setAttributesBonus(attributesBonus);
        return t;
    };

}
